\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{Cloud@CNAF}

\author{D Michelotto, F Cappanini, E Fattibene, D Salomoni and P Veronesi}

\address{INFN CNAF, Viale Berti Pichat 6/2, 40126 Bologna, Italy}

\ead{diego.michelotto@cnaf.infn.it}

\begin{abstract}

  Cloud@CNAF is a project aimed at deploying a Cloud Infrastructure,
  based on open source solutions to serve the different CNAF use
  cases. The project is the result of the collaboration of a
  transverse group of people from all CNAF departments: network,
  storage, farming, national services, distributed systems. The
  Cloud@CNAF IaaS (Infrastructure as a Service) is based on OpenStack,
  Havana version, a free and open-source cloud-computing software
  platform. The present infrastructure is used by many projects such
  as EEE, !CHAOS, Middleware Developers group. This paper presents
  the activity carried out at CNAF to set up the infrastructure, its
  deployment and the related training events devoted to the users. A
  perspective on the evolution of the infrastructure is also
  presented.
\end{abstract}

\section{Introduction}

The main goal of Cloud@CNAF project is to provide a Cloud
Infrastructure for CNAF users and for projects taking place at CNAF:

\begin{itemize}
\item Provisioning VM for CNAF departments
  \begin{itemize}
  \item Backend for provisioning of VMs to other services/processes
    like Jenkins, LBaaS
  \item Provisioning of VM for the User Support service (VirtualBox-like)
  \end{itemize}
\item Provisioning of VM for CNAF staff members
\item VM for experiments hosted at CNAF
  \begin{itemize}
  \item DIRAC-OpenStack integration
  \item access to VM for pilot jobs - CMS
  \item Long Term Data Preservation
  \end{itemize}
\item Tutorials and Hands-on
\end{itemize}

The infrastructure made available is based on OpenStack~\cite{ref1}, an
open source product that can be deployed on open source platforms and
has strong support from the industry. The version deployed in
production is Havana~\cite{ref2}.

\section{Infrastructure}
The present infrastructure is composed of:
\begin{itemize}
\item one Controller Node providing the core services - Keystone,
  Glance (LVM), Heat, Horizon, Ceilometer, MySQL, QPID, hosted on a
  physical machine with:
  \begin{itemize}
  \item 2x8 HT (32) Intel(R) Xeon(R) CPU E5-2450 @ 2.10GHz, 64 GB
    RAM
  \end{itemize}
\item one Network Node providing Neutron configured to use 100 VLANs
  with Open vSwitch\cite{ref2.1} virtual switch, hosted on a physical
  machine with:
  \begin{itemize}
  \item 2x8 HT (32) Intel(R) Xeon(R) CPU E5-2450 @ 2.10GHz, 64 GB
    RAM
  \end{itemize}
\item the infrastructure is completed by four Compute nodes (for a
  total of 64 CPU and 256 GB of RAM) that provide Nova services based
  on KVM/QEMU hypervisors.
\end{itemize}
In this environment GPFS\cite{ref2.2} is used as distributed file
system for the backend infrastructure storage. The GPFS cluster is
composed of three GPFS servers that expose four LUNs exported from a
Dell PowerVault StorageSystem, providing 16TB of storage space for
Nova backend.

A cloud User Interface node is also present to guarantee the access to
the infrastructure using the powerful OpenStack APIs for an advanced
use of OpenStack.

In early 2015 more than 50 users have been registered in the
infrastructure, providing for them 50 tenants with 48 instances that
use about 90 VCPUs and 180GB of RAM.

\section{Use cases}
The active use cases working at CNAF can be separated in two sets, the
first represents the internal CNAF use cases, and the second collects
the external projects where CNAF is involved in.
\subsection{CNAF Internal use case}
This set of use cases collect internal work groups and single user's
projects.
\begin{itemize}
\item The \textbf{Middleware Developers} team is composed by
  people active on the development of new software like EMI
  Middleware~\cite{ref3} for the Grid distributed computing resources,
  VOMS~\cite{ref3.1}, StoRM~\cite{ref3.2} and
  Argus~\cite{ref3.3}. Middleware developers are involved in two kinds
  of projects:
\begin{itemize}
\item {\it Middleware-project}: the instances of this project are
  coordinated by an external Jenkins~\cite{ref4} master. The instances
  are used for building the software and deploying it (Installation
  and Upgrade). Moreover, some instances based on CoreOS~\cite{ref4.1}
  are implementing Docker~\cite{ref4.2} nodes to speed up test
  deployment. (See Fig.~\ref{fig1}).
  \begin{figure}[h]
    \begin{center}
      \includegraphics[width=130mm]{jenkins.png}
    \end{center}
    \caption{\label{fig1}The Jenkins build node architecture (Left
      hand side) and the Jenkins docker deployment test architecture
      (Right hand side) are shown.}
  \end{figure}
\item {\it Storm Distributed Testbed}: this project is used for study
  and test the deployment in high availability for StoRM
  infrastructure. (See Fig.~\ref{fig2}).
  \begin{figure}[h]
    \begin{center}
      \includegraphics[width=140mm]{storm.png}
    \end{center}
    \caption{\label{fig2}The StoRM distributed architecture.}
  \end{figure}
\end{itemize}

\item \textbf{CNAF staff.} Every employee at CNAF can rely on a
  personal project that can be used for internal needs. Each project
  has limited resources: 10 instances, 10 VCPU, 25GB RAM, 5 Floating
  IPs and 10 security groups. At the time of writing there are about
  30 active users having personal projects.
\end{itemize}

\subsection{External Projects}
CNAF participates to several external project, in particular two of
them make a massively use of the Cloud@CNAF infrastructure.

\begin{itemize}
\item The \textbf{Extreme Energy Event} (EEE)~\cite{ref5} experiment
  is devoted to the search of high energy cosmic rays through a
  network of telescopes installed in about fifty high-grade schools
  distributed throughout the Italian territory.

  The EEE activity at CNAF started in 2014 with the data collection
  during the EEE pilot run. The activity involved 21 schools (plus two
  INFN telescopes) in a coordinated data acquisition. During the pilot
  run all the schools were connected/authenticated at CNAF in order to
  transfer the amount of data acquired from the school's
  telescopes. To this scope, in our cloud infrastructure a node has
  been dedicated to play the role of frontend in order to receive all
  the data (with a total required bandwidth of 300 kB/s) to collect
  the expected 10 TB per year using BTSynk~\cite{ref5.1}. All the
  information collected by the experiment are considered in custody
  and for this reason the migration of the data to a tape system has
  been implemented.

\item The \textbf{!CHAOS}~\cite{ref6} activity was originally
  developed within the context of High Energy Physics (HEP) as a
  candidate of Distributed Control Systems (DCS) and Data Acquisition
  (DAQ) for the SuperB experiment. In 2014 it evolved into the project
  {\it !CHAOS: a cloud of controls} supported by MIUR and developed by
  INFN through its four sites: Laboratori Nazionali di Frascati (LNF),
  Laboratori Nazionali del Sud (LNS), Padova Section and CNAF Centre.

  On top of OpenStack, the backend services are automatically provided
  as Platform as a Service (PaaS) components. The !CHAOS frontend
  services (CDS and MDS), instead, exploit the virtual instances of
  PaaS components and can run on single or multiple nodes. The
  frontend services communicate in a bidirectional way with the remote
  !CHAOS clients, such as the Control Unit (CU) and the User Interface
  (UI). Since the remote clients cannot be identified by a public IP
  address, a VPN service has been deployed within the Cloud@CNAF
  infrastructure to allow network traffic flow to and from the
  frontend services.
\end{itemize}

\section{Training}

In 2014 at CNAF four OpenStack training sessions have been organized
by the Software Developer and Distributed System group. The first
training course covered basic information about OpenStack, such as its
architecture and components.

The other three were dedicated to hands on OpenStack from a user point
of view. During these training, many arguments were treated, in
particular was explained how to use the OpenStack dashboard and how to
use its command line interface. More than 30 users participated to
these training courses. In addition, a seminar on Heat component has
been organized.

\section{Future Work}

In 2015 the Cloud@CNAF project will be improved in order to obtain a
highly available infrastructure. Next steps foresee the deployment of
a new infrastructure with redundant components (Controller and
Network) and an increasing number of hypervisor resources.

For stability reason the Neutron services will be based on Linux
Bridge instead of Open vSwitch. At the same time, the compute nodes
will be increased from 4 to 13 nodes aiming to make available a total
computing power of about 200 CPUs and 800 GB of RAM.

The GPFS filesystem will be used as backend storage for all OpenStacke
services, Nova, Cinder and Glance. Data persistence and the messaging
service, AMQP~\cite{ref6.1}, will be guaranteed by a three nodes
cluster hosting MySQL Percona~\cite{ref6.2} and RabbitMQ~\cite{ref6.3}
software tools.


\section*{References}
\begin{thebibliography}{9}
\bibitem{ref1} OpenStack, {\it http://www.OpenStack.org/}
\bibitem{ref2} OpenStack Havana, {\it https://www.OpenStack.org/software/havana/}
\bibitem{ref2.1} Open vSwitch, {\it http://openvswitch.org/}
\bibitem{ref2.2} GPFS aka Spectrum Scale, {\it http://www-03.ibm.com/systems/uk/storage/spectrum/scale/}
\bibitem{ref3} EMI, {\it http://www.eu-emi.eu}
\bibitem{ref3.1} VOMS, {\it http://italiangrid.github.io/voms/}
\bibitem{ref3.2} StoRM, {\it http://italiangrid.github.io/storm/}
\bibitem{ref3.3} Argus, {\it http://argus-authz.github.io/}
\bibitem{ref4} Jenkins, {\it https://jenkins-ci.org/}
\bibitem{ref4.1} CoreOS, {\it https://coreos.com/}
\bibitem{ref4.2} Docker, {\it https://www.docker.com/}
\bibitem{ref5} E Fattibene et al., CNAF activities in the !CHAOS project, this report
\bibitem{ref5.1} BitTorrent Sync, {\it https://www.getsync.com/intl/it/}
\bibitem{ref6} E Fattibene et al., The EEE Project activity at CNAF, this report
\bibitem{ref6.1} AMQP, {\it https://www.amqp.org/}
\bibitem{ref6.2} Percona, {\it https://www.percona.com/}
\bibitem{ref6.3} RabbitMQ, {\it https://www.rabbitmq.com/}
\end{thebibliography}

\end{document}

