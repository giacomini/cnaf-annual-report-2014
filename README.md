L'Annual Report 2014 del CNAF contiene la descrizione delle attività
del Centro nell'anno 2014, sotto forma di brevi articoli raccolti in
un unico documento.

Questa repository ha lo scopo di supportare la preparazione del
report, mantenendo sia i singoli articoli che la struttura generale
del documento.

Gli articoli devono essere scritti in inglese seguendo il template definito
dall'Institute of Physics (IOP) per le Conference Series. I template e le
istruzioni per gli autori sono disponibili all'indirizzo
<http://conferenceseries.iop.org/content/authors>. Le linee guida e i
template sono anche disponibili nella cartella istruzioni.

I template sono disponibili sia per LaTeX che per Microsoft Word, ma
è fortemente raccomandato che i contributi siano scritti in LaTeX. Questo
permette una maggiore qualità del prodotto finale e una maggiore
automazione nella gestione della produzione del report.

Il documento finale verrà ottenuto integrando direttamente i pdf, che
devono quindi essere "camera-ready". Tuttavia è preferibile caricare
sulla repository anche i sorgenti, inviandoli agli editor; se il sorgente
è fatto di più file, questi devono essere contenuti in un tarball o in uno zip.
