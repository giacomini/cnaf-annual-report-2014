
\documentclass[a4]{jpconf}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{xspace}

\begin{document}
\title{Projecting the CDF computing model to the long-term future}

\author{Silvia Amerio$^a$, Michele Pezzi$^b$}

\address{$^a$Dept. of Physics, University of Padova, via Marzolo 8, Padova (Italy)}
\address{$^b$INFN-CNAF, viale Pichat 6/2, Bologna (Italy)}

\ead{silvia.amerio@pd.infn.it}

\begin{abstract}
CNAF has played a key role in CDF computing model and is now contributing to the long term future preservation of CDF data and 
analysis capabilities. In this report, after a brief introduction on CDF most recent physics results, we will describe the 
status of the long term future data preservation project which is being implemented at CNAF.     
\end{abstract}

\section{The CDF experiment and recent results}
The CDF experiment is a high-energy physics experiment which took data  between 1986 and 2011.
 It detected and studied collisions of protons and anti-protons accelerated up to an 
energy of 2 TeV by the Tevatron accelerator complex, located at Fermilab (Batavia, US).
The discovery of top quark in 1995~\cite{TopDiscovery}, the observation of $B^0_S-\overline{B^0_S}$ oscillations~\cite{B0sOscill} and 
of single top~\cite{SingleTop}, are only few of the fundamental results obtained by the experiment. 
 
%\section{Recent CDF results}
CDF ended its data taking in 2011. More than 70 papers have been published since then, and many analysis are still
 ongoing on its 10$fb^{-1}$ data sample.
In the top sector, a measurement of the single top quark production cross section on the full data sample was 
published in 2014~\cite{Aaltonen:2014mza}. CDF and D0 measurements 
were combined and resulted in the first observation of single-top-quark production in the $s$-channel~\cite{CDF:2014uma}.
In the B sector, recent results include measurements of direct and indirect CP violating asymmetries 
in $D_0$ decays~\cite{Aaltonen:2014vra}~\cite{Aaltonen:2014efa}. Higgs sector is also under investigation, with limits on 
Higgs spin and parity, searches for fermiophobic Higgs and limits on charged Higgs boson
 production~\cite{Aaltonen:2015mka}~\cite{Aaltonen:2014hua}~\cite{Aaltonen:2015tsa}.

%  Measurements of top mass and top cross sections with increasing precision.

%ZZ cross section measurement
%Searches: Higgs

\section{CDF Computing Model for the long term future}
CDF computing architecture is evolving towards a model which will allow data access and analysis in the long term future.
At Tevatron a data preservation project has been implemented in 2014. The project is divided into the following areas:
\begin{itemize}
\item data preservation: all CDF data have been migrated to the most recent tape technology (T10Kd);
\item data access: CDF moved to a new data handling system, \textit{SAMWeb}~\cite{Lyon:2012vd}, based on http protocol for 
communication between the database and the experiment framework. This new system is easier to maintain 
and will be used by Intesity Frontier experiments at FNAL, so long term future support is guaranteed;
\item analysis software: a version of CDF software based on SL6 operating system has been released in 2014 and will be the 
legacy software release for the long term future; 
\item data analysis: a new submission system, \textit{jobsub}~\cite{Box:2014faa} has been developed to allow Intensity Frontier 
experiments to submit their jobs to the Open Science Grid. CDF moved to this new system to submit data analysis jobs to 
FNAL computing resources. As for SAMWeb, this system will be supported for many years to come.
\item documentation: all CDF internal notes have been migrated to Inspire and CDF webpages updated.   
\end{itemize}


\section{CDF computing at CNAF: the long term future data preservation project}
CNAF has been one of the major contributors to CDF computing outside Fermilab in the past and it now maintains a leading role in 
the data preservation effort. CDF has dedicated resources at CNAF: 8000 HS06 of computing power, 400 TB of 
disk and a set of machines for data access and analysis services. In 2014, CNAF contributed to CDF computing with 
the implementation of a long term future preservation project for CDF data. The project is 
being implemented in collaboration with CDF experiment and within 
the DPHEP collaboration. This is the first project funded by INFN on long term 
data preservation and will serve as a prototype for other experiments hosting their data at CNAF and other
INFN sites.  
The project is divided into two main areas: bit (data) preservation and analysis framework development. 

\subsection{Bit Preservation}
During 2014, 4 PB of CDF data (raw data and ntuples) were copied from FNAL using a dedicated link (see fig.~\ref{fig_network}).  
A mechanism able to copy the data at 5 Gb/s rate and store it in CNAF tape library 
has been setup in the second half of 2013 and used throughout 2014 to copy the data from FNAL. 
A dedicated 10Gb/s link and a reserved network allowed to manage and monitor CDF data movement 
independently from CNAF Tier 1 network resources and to have a secure high 
speed “channel” always available for data transfers, with no sharing of any resource.
The storage layout consists of a pool of disks managed by GPFS, a tape library 
infrastructure for the archive back-end and an integration system to transfer 
data from disk to tape and vice versa. The CNAF Tier 1 storage solution is 
GEMSS~\cite{Ricci:2012xg}, an integration of GPFS, TSM and StoRM. A single 10 Gb/s GridFtp Server, 
connected directly through the Storage Area Network (SAN) to the CDF GPFS 
file system disks and to the CNAF Tier 1 network switch 10 Gb backbone is used for the data copy.
 This allowed a plain method for transferring data from Fermilab to CNAF through a single 
point. In fig.~\ref{fig:FnalCNAFCopy} shows the data transfer rate during 2014. 

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\linewidth]{figures/NetworkLayout.png}
\caption{Layout of the FNAL-CNAF copy network.}
\label{fig_network}
\end{figure}


\begin{figure}[!h]
\centering
\includegraphics[width=0.75\linewidth]{figures/DataTransferRate2014.png}
\caption{Data transfer rate from Fermilab to CNAF during 2014.}
\label{fig:FnalCNAFCopy}
\end{figure}

\subsection{Analysis framework}
In order to access and process the data in the long term future it is essential to have an infrastructure that allows 
to use the experiment software. The goal is to make the software and data available and functional for many years in the future,
beyond CDF collaboration.

In the current CDF analysis framework at CNAF, the user submits a job from  his/her user area. 
The job submission system contacts the data access machine (SAM station) to access the data. Data is sent to worker nodes together
 with a tarball containing the analysis code. If needed, e.g. for MC production, detector and run conditions 
are retrieved from a dedicated database. All these services are already installed at CNAF and for the long-term future 
we plan to replicate this system as much as possible, upgrading the services to the latest versions.
We expect in the long-term future data will be accessed and processed  very rarely.  For this it is 
necessary to study a solution which is robust but at the same time allows to minimize the needed resources 
(number of physical machines within the framework).  In the analysis framework at CNAF the only real 
machine, except the storage system, is the database, which is currently located at FNAL. All other services are instantiated
 on virtual machines. The virtual machines will be handled by WNoDeS~\cite{bib_wnodes}, an INFN-developed framework that makes
 possible to dynamically allocate virtual resources out of a common resource pool, in order to instantiate the 
virtual machines when a job is started.

For the data access, the data access machine has been upgraded to SAMWeb. CNAF tape system is transparent for SAMWeb: upon 
a request to access a file, the file is recalled from tape on a disk cache before being sent 
to the final destination (worker node or user area). As already stated in the previous section, in the long term future 
we foresee intermittent access to CDF data, so the necessary disk cache will be allocated on-demand using CNAF resources 
in opportunistic mode.

Future analysis on CDF data will use a software legacy release based on SL6 
distributed through CVMFS. At CNAF as a first step squid proxy servers have been setup to access the CVMFS server 
located at FNAL. In a second phase the FNAL CVMFS server will be replicated at CNAF.

As far as job submission is concerned, the system currently installed for CDF at CNAF is based on glideinWMS~\cite{Amerio:2012ij} 
to exploit computing resources at CNAF and additional LCG resources at different Tier-2 sites 
in Italy and other European countries. In 2014 the current system has been maintained, upgrading it to
 use the legacy release. In 2015 we plan to move to the new job submission system developed at FNAL, jobsub.

\subsection{Conclusions}
During CDF RunII (2001-2011) operations CNAF has been one of the major computing centers for the experiment. 
A portal to access CNAF Tier-1 and other LCG resources is hosted at CNAF, together with dedicated 
data processing and storage resources. Now, three years after the end of data taking, CDF has entered the data preservation 
phase and CNAF is contributing with the implementation of a long term future data preservation 
project: complete copy of all CDF data is now available at CNAF, and the setup of a long term future analysis framework started at the 
end of 2014 and will be completed in 2015.

%\subsection{Appendices}

\section*{References}

\bibliographystyle{iopart-num}
\bibliography{cdfbib}

\end{document}


