\documentclass[a4paper]{jpconf}
\usepackage{hyperref}

\newcommand{\tmtextit}[1]{{\itshape{#1}}}
\newcommand{\tmtt}[1]{\texttt{#1}}

\begin{document}

\title{Adapting a custom accounting system to APEL}

\author{Andrea Chierici}
\ead{andrea.chierici@cnaf.infn.it}

\author{Stefano Dal Pra}
\ead{stefano.dalpra@cnaf.infn.it}

\author{Giuseppe Misurelli}
\ead{giuseppe.misurelli@cnaf.infn.it}

\author{Saverio Virgilio}
\ead{saverio.virgilio@cnaf.infn.it}

\section{Introduction}
Starting from October 2013, we upgraded the lower layer of the
accounting system of the Tier--1 with a solution designed and
implemented from scratch, due to an incident occurred with the
previous system, the Distributed Grid Accounting System (DGAS). Now,
accounting data for Grid and local jobs are stored in a \tmtt{acct}
PostgreSQL database whose content and schema is managed by the farming
staff\cite{DGAS}.

Data propagation to the {\url{accounting.egi.eu}} portal is performed
as before, through the DGAS--HLR hierarchy: the MySQL database in the
first--level HLR is updated with the newest accounting records. This
step, formerly performed by specific DGAS components, has been
replaced by a custom component whose main task is to fetch new records
from the {\tmtt{acct}} database and to insert them into the
{\tmtt{hlr}} database hosted in the site HLR.  These data were then
transmitted to the second--level HLR, hosted and managed by staff at
INFN--Torino. This system acted as a central collector for all the
accounting data from all the sites in the italian Regional Operation
Center (ROC) and was also responsible of their delivering to the
{\url{accounting.egi.eu}} portal of WLCG accounting, managed by
APEL\cite{APEL}.

On September 2014 the second--level HLR of the Italian ROC had to be
dismissed and after a short discussion, \ the IGI consortium decided
that Italian sites had to replace their DGAS infrastructure with the
one provided by APEL: the main difference is that each APEL instance
directly delivers usage records straight to the
{\url{accounting.egi.eu}} portal.

\section{Incompatibility issues}

A few issues prevented us from directly adopting the APEL accounting
model and forced us to study a different solution:

\begin{description}
\item[multi--site batch system] Three Grid sites are supported by the
  ``Tier--1'' computing centre, and they all rely on a single instance
  of the Platform/LSF batch system deployed on the farm: APEL however
  cannot fairly handle this case, as it assumes that each Grid site
  relies on its own batch system.
  
\item[LSF log parsing] Even in the most recent APEL release available,
  the component responsible of parsing the raw accounting records
  logged by LSF, proved to be bugged, failing to recognize a number of
  valid accounting records.
  
\item[Wrong WallClockTime] LSF logs the time each job spends in
  {\tmtt{RUN}} status in the {\tmtt{runtime}} field. APEL however does
  not collect that value and sets {\tmtt{WallDuration}} as
  ``{\tmtt{endtime - starttime}}''. This is correct if the job is not
  suspended during its lifetime and if it is dispatched only once,
  which is not always guaranteed. Furthermore, if a job is killed
  before being started, the {\tmtt{starttime}} field of its log record
  remains at its default value, which is zero. This fools the APEL
  parser to account for a job with a {\tmtt{WallDuration}} equal to
  \tmtextit{epoch}, i.e. as if it started in 01-01-1970.
  
\item[multi--core] The number of used cores is not considered in the
  computation of {\tmtt{WallDuration}} for multi--core jobs, which
  then turns out to have an efficiency $E = \frac{CPUtime}{WCtime} >
  1$.  This is not consistent with the case of single--core jobs and
  should instead be computed as $E = \frac{CPUtime}{cores \times
    WCtime}$.
  
\item[HPC] INFN--T1 hosts a small HPC cluster, managed by LSF 9.1,
  running many--core, multi--core or GPU based applications. The
  accounting of the activity of this cluster is done using the same
  custom accounting system described in \cite{DGAS}. This enables us
  to quickly adapt our accounting requirements even on new and
  different use-cases, such as that of GPU computing.
  
\item[Custom needs] A generic accounting system should aim at
  collecting and storing only those accounting information that make
  sense on the widest set of batch systems: still, many useful
  specific data are logged by the LSF batch system, and these are
  valuable to the farm staff (such as the queue name, the submission
  or execution host, the job exit status, the submission request
  parameters, etc.). Thus, the overall information gathered by our
  local accounting is a superset of those currently required and
  requested to provide accounting information. A single accounting
  system (with all the data of interest stored in it) is simpler to
  maintain and eases the task of extracting a specific subset of info
  to fulfil a particular request.

\end{description}

\section{Switching the data propagation model}
After an incident happened at the 2$^{nd}$ level HLR and its
subsequent dismissal, the way to propagate the accounting data from
the Tier--1 to the EGI portal had to be changed.  The data delivered
from our local {\tmtt{acct}} database had to be adapted to directly
transmit accounting records to the EGI--APEL database using the
{\tmtt{ssmsend}} utility coming from the APEL distribution.

\subsection{The APEL patching attempt}
At first, an attempt to adapt APEL software to solve the
aforementioned issues was made. Three APEL python programs were
patched to provide multi--site awareness and multi--core
compliance. Furthermore, the low level parsing of the raw LSF log
files was adapted to use a reliable open source library, from the
{\tmtt{python-lsf-tools}} collection. The former log parsing approach
used the {\tmtt{lsf.py}} low level module from the APEL distribution,
that is based on a regular expression which simply is too fragile and
fails to correctly parse the records on a certain number of corner
cases. The {\tmtt{accounting.py}} library from
{\tmtt{python-lsf-tools}} proved indeed to be more reliable.

After verifying the updates were correct, a ticket (\#109485) had been submitted to {\url{https://ggus.eu}} to propose the APEL team the adoption of the patches and to get support during the process of the
adaptation of the accounting data delivery.

\subsection{The working approach}
Even if this approach proved to produce correct data, while waiting
for the feedback about the proposed patches, we followed a different
strategy: the data to be delivered using the {\tmtt{ssmsend}} APEL
utility from the {\tmtt{apel-ssm}} package are produced by a custom
script which extracts them from the {\tmtt{acct}} database and then
dumps them into a set of files equivalent to those produced by the
original APEL system. Each file contains one--thousand usage records
and is deleted just after its delivery.  The final outcome of the
implemented solution has a significant difference with respect to what
would be done using current APEL approach: the {\tmtt{Site}} field
doesn't come any more from a configuration file, it hasn't static
values hard coded in it but is defined instead as a map of the
submission host. This enables one single host to deliver all the
accounting records produced by a Batch System instance, despite the
number of CEs used or the logical Grid Sites relying on it.

\subsection{Faust}
Short after dismissing the old 2$^{nd}$ level HLR, the former DGAS
staff introduced \tmtextit{Faust}, a new accounting repository for the
Italian ROC, offering a web--service based API interface intended for
extracting graphical reports for the client. This repository has to be
fed with the same data delivered to APEL, so the transmission process
is repeated twice, simply providing a different configuration file to
the {\tmtt{ssmsend}} utility.

\subsection{Validation}
The validation process for the new system was followed through the
aforementioned ggus ticket, which provides a pretty detailed report on
the progress of this activity.

\section{Conclusions}
Our implementation proved to be reliable enough to be used in every
day production: we have now a more robust accounting system that is
used every day by a great number of people flawlessly.

\section*{References}

\begin{thebibliography}{9}
\bibitem{DGAS} S. Dal Pra, ``Accounting Data Recovery. A Case Report
  from INFN-T1'' Nota interna, Commissione Calcolo e Reti dell'INFN,
  {\tt CCR-48/2014/P}
\bibitem{APEL} APEL, \url{https://wiki.egi.eu/wiki/APEL}
\end{thebibliography}

\end{document}
