\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{The INFN Tier-1: networking}

\author{S Zani, D De Girolamo and L Chiarelli}

\section{Introduction}
The CNAF Network department manages the wide area and local area
connections of CNAF, is responsible for the security of the centre and
also contributes to the management of the local CNAF services (e.g.,
DNS, mailing, Windows domain, etc.) and some of the main INFN national
ICT services.

\section{Wide Area Network}

Inside the CNAF datacentre the main PoP of GARR network is hosted, one
of the first nodes of the recent GARR-X evolution based on a fully
managed dark fibre infrastructure.

As shown in Figure~\ref{wan}, CNAF is connected to the WAN via
GARR/GEANT essentially with two physical links:

\begin{itemize}
\item A general IP with a 10~Gb/s connection via GARR and GEANT
\item A link to WLCG destinations, which has been upgraded to 40~Gb/s,
  shared between the LHC-OPN Network for Tier0-Tier1 and Tier1-Tier1
  traffic and LHC-ONE network mainly for Tier2 and Tier3 traffic.
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{wan.png}
\end{center}
\caption{\label{wan}WAN connection schema}
\end{figure}

\subsection{Routing changes in 2014}

During 2014 the traffic between the Tier1 centres hosted at CNAF, KIT
and IN2P3 has been moved from LHC-OPN to LHC-ONE in order to optimize
the network resources available within the GEANT network and between
the GEANT network and the national NRENs. In Italy the peering between
the GARR network and the GEANT network is made of $2 x 100$~Gb/s
links; moving part of some of the inter-Tier1 traffic on that link
frees bandwidth on the LHC-OPN uplink for Tier0-Tier1 transfers and
the rest of Tier1-Tier1 traffic.

\subsection{Possible Capacity Evolution}

The WLCG link can be upgraded any time to 60~Gb/s ($6x10$~Gb/s) and
GARR has in its roadmap a 100~Gb/s link between the GARR POP at CNAF
and GEANT (the fibers are 100~Gb-ready but an upgrade of the GARR
optical devices is needed).

An upgrade of the general IP link to 20~Gb/s is planned for 2015.

\section{Local Area Network}

The Tier1 LAN is essentially a star topology network based on a fully
redundant switch router (Cisco Nexus 7018), used both as a core switch
and an access router for LHC-OPN and LHC-ONE networks. In addition,
more than 100 aggregation (``Top Of the Rack'') switches are
installed, with Gigabit Ethernet interfaces for the Worker Nodes of
the farm and 10~Gb Ethernet interfaces used as uplinks to the core
switch. Disk servers and GridFTP servers are directly connected to the
core switch at 10~Gb/s.

General Internet access, local connections to the offices and INFN
national services provided by CNAF are managed by another network
infrastructure based on a Cisco 7606 Router, a Cisco Catalyst 6509 and
an Extreme Networks Black Diamond 8810. CNAF owns an IPv4 B class
(131.154.0.0/16) and a couple of C classes for specific purposes: half
of the B class is used for Tier1 resources and the other half is used
for all the other services, thus providing sufficient IP
addresses. The private address classes are used for IPMI and other
internal services.

Additionally, two /48 IPv6 prefixes are assigned to CNAF
(2001:760:4204::/48 for CNAF General and 2001:760:4205::/48 for CNAF
WLCG). Recently we have started the IPv6 implementation on the LAN.

\section{Network monitoring and security}

In addition to the perfSONAR-PS and the perfSONAR-MDM
infrastructures~\cite{perfsonar} required by WLCG, the monitoring
system is based on several tools organized in the ``Net-board'', a
monitoring dashboard realized at CNAF (see Figure~\ref{netboard}). The
Net-board integrates MRTG~\cite{mrtg}, NetFlow Analyser~\cite{netflow}
and Nagios~\cite{nagios}, with some scripts and web applications to
give a complete view of the network usage, which allows to promptly
identify possible problems. The alarm system is based on Nagios.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{netboard.png}
\end{center}
\caption{\label{netboard}A screenshot of the monitoring Net-board}
\end{figure}

The network security policies are mainly implemented as hardware-based
ACLs on the access router and on the core switches (with a dedicated
ASIC on the devices).

The network group, in coordination with GARR-CERT and EGI-CSIRT, also
takes care of security incidents at CNAF, both for compromised systems
or credentials and discovered vulnerabilities of software and Grid
middleware, cooperating with all involved parties to identify and
apply the appropriate solutions.

\section{Software Defined Networks}

Software Defined Networks (SDN) provide a level of abstraction on top
of the physical network, allowing to delegate some decisions to
application software.

The CNAF Network department has been investigating SDN technology
since 2013~\cite{chiarelli}, with the purpose to evaluate its use in a number of
present and foreseable scenarios typical of the centre, including:
physical network abstraction, integration with virtualization stacks
like OpenStack, centralization and automation of network
configuration, network configuration on-demand, seamless integration
of geographically-separated sites.

The experimentation is performed on a dedicated infrastructure,
NetLab@CNAF, shown in Figure~\ref{netlab}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{netlab.png}
\end{center}
\caption{\label{netlab}The NetLab@CNAF}
\end{figure}

During 2014 the investigation has concerned mainly two products:

\begin{itemize}
\item Programmable Flow Control by NEC~\cite{pfc}
\item OpenDaylight~\cite{opendaylight}
\end{itemize}

Both are based on the OpenFlow standard protocol~\cite{openflow} and
provide convenient interfaces for the definition of virtual routers
and switches for the establishment of Virtual Tenant Networks,
especially useful when assigning cloud resources to different users,
which should appear separated network-wise.

\section*{References}
\begin{thebibliography}{9}
\bibitem{perfsonar} perfSONAR, \verb"http://psps.perfsonar.net/"
\bibitem{mrtg} MRTG -- Multi Router Traffic Grapher, \verb"http://it.wikipedia.org/wiki/Multi_Router_Traffic_Grapher"
\bibitem{netflow} NetFlow, \verb"http://en.wikipedia.org/wiki/NetFlow"
\bibitem{nagios} NAGIOS, \verb"http://www.nagios.org"
\bibitem{chiarelli} L. Chiarelli, \em{Software Defined Networks:
  Studio del modello e implementazione di una rete basata sullo
  standard OpenFlow}, Master Thesis
\bibitem{openflow} OpenFlow, \verb"https://www.opennetworking.org/"
\bibitem{pfc} NEC Programmable Flow Control, \verb"http://www.necam.com/SDN/"
\bibitem{opendaylight} OpenDaylight, \verb"https://www.opendaylight.org/"
\end{thebibliography}

\end{document}
