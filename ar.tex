\documentclass[twoside]{book}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian, english]{babel}
\usepackage[a4paper, width=455.24408pt, height=654.41338pt, top=1.58in, centering]{geometry} % match the jpconf style
\usepackage{pdfpages}
%\usepackage{lipsum}
\usepackage{emptypage} % so that empty pages don't have headings and footers
\usepackage{fancyhdr}
\usepackage{tocloft}
\usepackage{titlesec}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{footnote}           % to manage footnotes inside a tabular
\usepackage[hidelinks]{hyperref} % this package should come last; toc entries are hypertext links

\renewcommand{\cftpartfont}{\hfil\Large\bfseries}
\renewcommand{\cftpartafterpnum}{\hfil}
\cftpagenumbersoff{part}

\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}}

\begin{document}

%\title{INFN-CNAF\\
%Annual Report 2014}
%\date{}
%\author{}
%\maketitle

\includepdf[pages=1, pagecommand={\thispagestyle{empty}}]{papers/cover.pdf}

\newpage
\thispagestyle{empty}
~
\vfill

\subsubsection*{INFN-CNAF Annual Report 2014}

\textit{www.cnaf.infn.it/annual-report}\\
ISSN 2283-5490 (online)

\subsubsection*{Editors}

Luca dell'Agnello\\
Francesco Giacomini\\
Lucia Morganti

\subsubsection*{Cover Design}

Francesca Cuicchio

\subsubsection*{Address}

INFN CNAF\\
Viale Berti Pichat, 6/2\\
I-40127 Bologna\\
Tel. +39 051 2095 475, Fax +39 051 2095 477\\
\textit{www.cnaf.infn.it}


\cleardoublepage % force a right-side page

\frontmatter
\pagestyle{fancy}
\fancyhf{} % clear headers and footers
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhf[HLE,HRO]{\thepage}
\fancyhf[HLO,HRE]{\leftmark}

\tableofcontents

\cleardoublepage % force a right-side page

\mainmatter

\phantomsection
\addcontentsline{toc}{chapter}{Introduction}
\markboth{\MakeUppercase{Introduction}}{\MakeUppercase{Introduction}}
\chapter*{Introduction}
\thispagestyle{plain}
%\lipsum

During 2014 CNAF continued to pursue its main mission, namely
supporting the scientific computing of the INFN activities, supporting
the World-Wide LHC Computing Grid (WLCG) e-infrastructure, addressing
the developments towards new distributed systems paradigms
(e.g. Cloud) and developing software modules targeted to INFN
experiments. A significant part of the activity has also been devoted
to attracting external funds (basically proposing or participating to
new H2020 projects) and to transferring knowledge to the industrial
and public administration worlds.

The Tier-1 Data Center ran smoothly during the year, apart from an
incident at one of our chillers that forced a one-day unexpected
shutdown of the center, yet staying well inside the availability and
reliability metrics requested to an LHC Tier-1 class data center. The
year 2014 confirmed the previous years' trend of a steady increase in
the number of particle and astroparticle physics experiments using the
CNAF facilities; four new experiments (NA62, PANDA, CTA, Darkside and
CUORE) in fact decided to rely on our center for their data storage
and processing. Nevertheless the utilization ratio of the computing
resources by the LHC experiments compared to other experiments
remained at 75\%.

All experimental groups present at CNAF are assisted by the User
Support team, which is skilled both in proposing computing models for
experiments and in the management of the daily operations. The team
represents a well-defined interface between the users and the internal
organization, from the operators of the data center to the Grid
middleware developers.

During 2014 the computing power available at the Tier-1 reached
136000~HS06, the fast storage exceeded the capacity of 15~PB and the
long-term storage the capacity of 24~PB. Some important objectives
were achieved during the year: a thorough investigation into low-power
CPUs; the introduction of the concept of "dynamic partitioning" in the
CNAF batch system, to provide an acceptable resource-sharing mechanism
also for jobs requiring a multicore environment; significant
improvements to the monitoring and reporting system of the main
operational parameters of the center. Important achievements were also
accomplished in the domain of services of national utility for INFN,
such as a highly-available DNS architecture, the introduction of an
official INFN document management system, a Dropbox-like service and a
disaster recovery system for the INFN Information System.

The Software Development and Distributed Systems (SDDS) group was
involved in significant projects based on the OpenStack open source
software to create private and public clouds. The Cloud@CNAF
initiative has been the first example of this, a pilot Cloud opened to
INFN internal users. !CHAOS and Open City Platform (OCP) are the other
two projects present at CNAF and based on OpenStack, in this case
funded by MIUR. The LHCb and KM3NeT experiments have profited of the
skills available in the group to develop key parts of their data
acquisition and trigger system software. The group is also involved in
the management of the Grid-WLCG infrastructure and in the maintenance
of some critical Grid middleware services, namely VOMS, StoRM and
Argus.

Within the European H2020 framework program, CNAF participated to the
2014 calls related to the areas of development more suited for our
center, especially on distributed systems (Grid and Cloud) and on
infrastructures for our communities, but also on the development of
new IT technology. Four projects were submitted: INDIGO Data Cloud
(EINFRA-1-2014), on the development of a data and computing platform
targeting scientific communities and provisioned over hybrid (private
and public) e-infrastructures; ASTERICS (INFRADEV-4-2014-2015), to
implement and operate cross-cutting services for a cluster of ESFRI
projects focused on astronomy and astroparticle physics; ExaNeSt
(FETHPC-1-2015), with the aim to develop and prototype solutions for
some of the crucial problems on the way towards the production of
exascale-level supercomputers; and EGI-Engage (EINFRA-1-2014), to
engage the Research Community towards an Open Science Commons. At the
time of writing this annual report, we know that all four projects
have been approved. The resources granted by these projects will allow
CNAF to continue its development activities in the various fields of
interest for the coming years.

\vspace{1cm}

\begin{flushright}
  \parbox{0.7\textwidth}{
    \begin{center}
      \textit{Gaetano Maron}
      \\\textit{CNAF Director}
    \end{center}
  }
\end{flushright}

\cleardoublepage

\phantomsection
\addcontentsline{toc}{chapter}{Giulia Vita Finzi (1956--2015)}
\markboth{}{}
\chapter*{Giulia Vita Finzi (1956--2015)}
\thispagestyle{plain}

\begin{wrapfigure}{l}{0.3\textwidth}
  \vspace{-10pt}
  \centering
  \includegraphics[width=0.3\textwidth]{papers/giulia.jpg}
  \vspace{-20pt}
\end{wrapfigure}

After a short illness, our friend and colleague Giulia Vita Finzi
passed away on March $23^{th}$, 2015. We have lost a very special
colleague, always enthusiastically present in all initiatives that
have made CNAF an important centre, at a national and international
level.

Giulia joined CNAF as a scientific secretary. Later she became
involved in the evolution of the networking infrastructure and in the
founding of the ICT national services, historically an important asset
for the whole INFN: the first INFN web site, the news system, the IXI
network. Then came the experience with the CNTC (Commission for New
Computing Technologies) and the participation to the development of
the Grid, with a specific focus on the Information System and the
management of X.509 certificates. Her last committment was the
responsibility of the national service that guarantees proper
technical support for hardware and software acquisitions. Her
collaboration has been constant, constructive and precious.

For many of us she was also a dear friend, present in happy moments as
well as in sad or difficult situations. And she always had a word of
optimism and encouragement. Her great energy was contageous.

Working at CNAF has never been easy, but Giulia knew how to make it a
little bit easier.

% include a part entry in the toc
\newcommand{\ip}[1]{%includepart
  \cleardoublepage
  \thispagestyle{empty}
  \phantomsection
%  \addtocontents{toc}{\string\begin{NoHyper}}
  \addcontentsline{toc}{part}{#1}
%  \addtocontents{toc}{\string\end{NoHyper}}
  \addtocontents{toc}{\protect\mbox{}\protect\hrulefill\par}
  {\null\hfill\LARGE\textbf{#1}}
  \cleardoublepage
}

% for each paper:
% * add a directive to include the paper in the toc
%   (\phantomsection makes \addcontentsline work well with hyperref)
% * include the first page with a plain page style
%   (=> no heading and footer)
% * set the left marker of the heading
% * include the rest of the paper with the fancy page style
% example usage: \ia{The Trigger and Data Acquisition system of the KM3NeT-Italy detector}{tridas}


\newcommand{\ia}[2]{%includearticle
  \phantomsection
  \addcontentsline{toc}{chapter}{#1}
  \includepdf[pages=1, pagecommand={\thispagestyle{plain}}]{papers/#2}
  \markboth{#1}{}
  \includepdf[pages=2-, pagecommand={\thispagestyle{fancy}}]{papers/#2}
}

\ip{Scientific Exploitation of CNAF ICT Resources}

\ia{The User Support unit at CNAF}{user_support}
\ia{ALICE -- A Large Ion Collider Experiment}{alice}
\ia{AMS data processing and analysis at CNAF}{ams}
\ia{ATLAS activities}{atlas}
\ia{Pierre Auger Observatory Data Simulation and Analysis at CNAF}{auger}
\ia{The Belle II Experiment at the INFN CNAF Tier1}{belle2}
\ia{The Borexino experiment at the INFN CNAF Tier1}{borexino}
\ia{The CMS Experiment at the INFN CNAF Tier1}{cms}
\ia{The COKA Project}{coka}
\ia{The Cherenkov Telescope Array}{cta}
\ia{CUORE experiment}{cuore}
\ia{The EEE Project activity at CNAF}{eee}
\ia{The GERDA experiment}{gerda}
\ia{The Fermi-LAT experiment at the INFN CNAF Tier 1}{glast}
\ia{LHCb Computing at CNAF}{lhcb}
\ia{NA62 computing at CNAF}{na62}
\ia{OPERA Experiment}{opera}
\ia{Advanced Virgo Computing at INFN CNAF}{virgo}
\ia{XENON computing activities}{xenon}

% to keep together the next part title with its chapters in the toc
%\addtocontents{toc}{\newpage}

\ip{The INFN-Tier1 Center and National ICT Services}

\ia{The INFN-Tier1: a general overview}{t1}
\ia{Low-power CPU investigation}{low_power}
\ia{Adapting a custom accounting system to APEL}{accounting}
\ia{Dynamic partitioning for multi-core and high-memory provisioning with LSF}{dynamic_farm}
\ia{Towards a common monitoring dashboard for the Tier-1}{monitoring}
\ia{Projecting the CDF computing model to the long-term future}{cdf}
\ia{The INFN Tier-1: networking}{t1_network}
\ia{National ICT infrastructures and services}{servizi_nazionali}

\ip{Software Services and Distributed Systems}

\ia{CNAF activities in the !CHAOS project}{chaos}
\ia{The Trigger and Data Acquisition system of the KM3NeT-Italy detector}{km3tridas}
\ia{The 40 MHz trigger-less DAQ for the LHCb upgrade}{lhcb_eb}
\ia{WNoDeS: The error-correcting virtualization framework}{wnodes}
\ia{Cloud@CNAF}{cloud_cnaf}
\ia{Middleware support, maintenance and development}{mw_devel}
\ia{A novel software quality model}{sw_quality}
\ia{An assessment of software metrics tools}{metrics}
\ia{Provisioning IaaS for the Open City Platform project}{ocp}
\ia{Porting the Filtered Back-projection algorithm on low-power Systems-On-Chip}{ct_socs}

\ip{Knowledge Transfer}

\ia{External Projects and Technology Transfer}{tt}

\ip{Additional Information}

\cleardoublepage % force a right-side page

\phantomsection
\addcontentsline{toc}{chapter}{Organization}
\markboth{\MakeUppercase{Organization}}{\MakeUppercase{Organization}}
\chapter*{Organization}
\thispagestyle{plain}

\vspace*{1cm}

\subsection*{Director}

\begin{tabular}{ l }
Gaetano Maron
\end{tabular}

\subsection*{Scientific Advisory Panel}

\begin{tabular}{ l l p{7cm} }
\textit{Chairperson} & Michael Ernst         & \textit{\small Brookhaven National Laboratory, USA} \\
                     & Gian Paolo Carlino    & \textit{\small INFN -- Sezione di Napoli, Italy} \\
                     & Patrick Fuhrmann      & \textit{\small Deutsches Elektronen-Synchrotron, Germany} \\
                     & Josè Hernandez        & \textit{\small Centro de Investigaciones Energéticas, Medioambientales y Tecnológicas, Spain} \\
                     & Donatella Lucchesi    & \textit{\small Università di Padova, Italy} \\
                     & Vincenzo Vagnoni      & \textit{\small INFN -- Sezione di Bologna, Italy} \\
                     & Pierre-Etienne Macchi & \textit{\small IN2P3/CNRS, France}

\end{tabular}

% open local environment where the format of section and subsection
% is modified
{

% see titlesec documentation
%\titleformat{ command }[ shape ]{ format }{ label }{ sep }{ before-code }[ after-code ]
\titleformat{\section}{\large\bfseries\center}{}{}{}[\titlerule]
\titleformat{\subsection}{}{}{}{{\bfseries Head:} }

%\titlespacing*{ command }{ left }{ before-sep }{ after-sep }[ right-sep ]
%\titlespacing*{\section}{0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}

% NB use the * versions of section and subsection otherwise they end up in the toc

\section*{User Support}

\subsection*{D. Cesini}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  M. Tenti     & L. Morganti & S. A. Tupputi & S. Taneja
\\A. Falabella

\end{tabular}

\section*{Tier1}

\subsection*{L. dell'Agnello}

\begin{savenotes}               % to manage correctly footnotes inside tabular
  \begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

    \textbf{Farming}                & \textbf{Storage}         & \textbf{Networking} & \textbf{Infrastructure}
    \\[.1cm]\underline{A. Chierici} & \underline{V. Sapunenko} & \underline{S. Zani} & \underline{M. Onofri}
    \\S. Dal Pra                    & A. Cavalli               & L. Chiarelli\footnote{GARR employee relocated at CNAF} & M. Donatelli
    \\G. Misurelli                  & D. Gregori               & D. De Girolamo      & A. Mazza
    \\A. Simonetto                  & M. Pezzi                 & F. Rosso
    \\S. Virgilio                   & A. Prosperini
    \\                              & P. Ricci

  \end{tabular}
\end{savenotes}

\section*{R\&D Service}

\subsection*{D. Salomoni}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  D. Andreotti   & M. Bencivenni & A. Ceccanti & V. Ciaschini
\\G. Dalla Torre & E. Fattibene  & M. Favaro   & F. Giacomini
\\M. Manzali     & D. Michelotto & A. Paolini  & E. Ronchieri
\\P. Veronesi    & E. Vianello   & G. Zizzi

\end{tabular}

\section*{National ICT Services}

\subsection*{R. Veraldi}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

S. Antonelli

\end{tabular}

\section*{Technology Transfer}

\subsection*{M. C. Vistoli}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  A. Ferraro & B. Martelli

\end{tabular}

\section*{Hardware and Software Support}

\subsection*{G. Vita Finzi}

\section*{Information System}

\subsection*{G. Guizzunti}

\begin{tabular}{ p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} p{0.23\textwidth} }

  S. Bovina & M. Canaparo & E. Capannini & F. Capannini
\\S. Cattabriga & C. Galli & S. Longo & C. Simoni

\end{tabular}

\section*{Director Office}

\subsection*{A. Marchesi}

\section*{Expenditure Centralization Office\protect\footnote{The office is under the INFN Director General.}}

\subsection*{M. Pischedda}

} % close local environment


\cleardoublepage % force a right-side page

\phantomsection
\addcontentsline{toc}{chapter}{Seminars}
\markboth{\MakeUppercase{Seminars}}{\MakeUppercase{Seminars}}
\chapter*{Seminars}
\thispagestyle{plain}

\begin{longtable}{ l p{12cm} }

  Jan. $9^{th}$ & Filippo Mantovani \\
  & \textbf{Supercomputing based on Mobile Processors} \\[.5cm] % leave .5 cm of vertical space

  Jan. $15^{th}$ & Diego Michelotto, Marco Bencivenni \\
  & \textbf{Un Portale Web per Comunità Scientifiche} \\[.5cm]

  Mar. $4^{th}$ & Paolo Veronesi, Giuseppe Misurelli \\
  & \textbf{Gestione del ciclo di vita dei server con Foreman e Puppet} \\[.5cm]

  Mar. $14^{th}$ & Pedro Andrade \\
  & \textbf{Exploiting open source tools to realize a new monitoring infrastructure at CERN} \\[.5cm]

  Apr. $4^{th}$ & Davide Salomoni \\
  & \textbf{Report da ISGC 2014} \\[.5cm]

  Jul. $16^{th}$ & Matteo Favaro \\
  & \textbf{Ceph, architettura e utilizzo} \\[.5cm]

  Jul. $23^{th}$ & Fabio Capannini \\
  & \textbf{Orchestration in OpenStack with Heat} \\[.5cm]

  Sep. $19^{th}$ & Andrea Petrucci \\
  & \textbf{Experience with Infiniband for CMS Event Building} \\[.5cm]

  Oct. $3^{rd}$ & Alberto Di Meglio \\
  & \textbf{CERN openlab, un modello di collaborazione tra ricerca e industria} \\[.5cm]

  Oct. $7^{th}$ & Elisabetta Ronchieri \\
  & \textbf{Report dalla conferenza RPSD 2014} \\[.5cm]

  Oct. $7^{th}$ & Salvatore Tupputi \\
  & \textbf{Report dalla conferenza GPU in HEP} \\[.5cm]

  Oct. $20^{th}$ & Tim Mattson \\
  & \textbf{Programming Extreme Scale Computers} \\[.5cm]

  Nov. $18^{th}$, $20^{th}$ & Francesco Giacomini\\
  & \textbf{Modern C++ -- From Pointers to Values} \\[.5cm]

  Dec. $2^{nd}$ & Davide Salomoni \\
  & \textbf{Report da SuperComputing 2014} \\[.5cm]

  Dec. $2^{nd}$ & Alessandro Paolini, Enrico Fattibene \\
  & \textbf{Report da OpenStack Summit 2014} \\[.5cm]

  Dec. $4^{th}$ & Rene Meusel \\
  & \textbf{Introduction to the CernVM-File System} \\[.5cm]

  Dec. $9^{th}$ & Matteo Panella \\
  & \textbf{Casi d'uso di infrastrutture OpenStack: file system on-demand e database su container} \\[.5cm]

  Dec. $19^{th}$ & Lorenzo Dini \\
  & \textbf{Building Software at Google Scale} \\[.5cm]

  Dec. $22^{nd}$ & Leonardo Testi \\
  & \textbf{ALMA: alla scoperta dell'Universo freddo} \\[.5cm]

\end{longtable}

\end{document}
