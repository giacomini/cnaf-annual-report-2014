\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{The Trigger and Data Acquisition system of the KM3NeT-Italy detector}

\author{M. Manzali $^2$ $^3$, T. Chiarusi $^1$, M. Favaro $^1$ $^2$ $^3$, F. Giacomini $^2$, C. Pellegrino $^1$ $^4$ on behalf of the KM3NeT-Italy Collaboration}

\address{$^1$ INFN Bologna, Bologna, Italy}
\address{$^2$ INFN CNAF, Bologna, Italy}
\address{$^3$ Universit\`{a} degli Studi di Ferrara, Ferrara, Italy}
\address{$^4$ Universit\`{a} degli Studi di Bologna, Ferrara, Italy}

\ead{matteo.manzali@cnaf.infn.it}

\begin{abstract}
KM3NeT-Italy is an INFN project that will develop a submarine cubic-kilometre neutrino telescope in the Ionian Sea (Italy) in front of the south-east coast of Portopalo di Capo Passero, Sicily. It will use thousands of PMTs to measure the Cherenkov light emitted by high-energy muons, whose signal-to-noise ratio is quite disfavoured. This forces the use of an on-line Trigger and Data Acquisition System (TriDAS) in order to reject as much background as possible. In March 2013 a prototype detector, hosting 32 PMTs, has been deployed in the abyssal site of Capo Passero and successfully operated. The existing TriDAS software, used for the prototype, needs a deep revision in order to meet the requirements of the final detector: the adoption of new tools for software development and modern design solutions will bring several improvements and simplifications during this upgrade.
\end{abstract}

\section{Introduction}
KM3NeT-Italy is an INFN project supported with Italian PON\footnote{The research leading to these results has received funding from the Programma Operativo Nazionale  "Ricerca \& Competitivit\`a" 2007-2013.} fundings for building the first part of the Italian node of the KM3NeT neutrino telescope \cite{Neutrino} \cite{km3net}. The detector will be placed in the Ionian Sea (Italy) at 3500 m of depth. The KM3NeT-Italy detector will be made of ~700 optical modules (OMs), each one containing a 10" PMT and the readout electronics. The OMs are organized in vertical structures called towers. Each tower is composed of 14 horizontal bars with 6 OMs each. The detection principle is based on the measurement of the Cherenkov light from high-energy neutrino induced charged particles produced within a fiducial volume around the telescope \cite{muon}. The "all data to shore" approach is assumed to reduce the complexity of the submarine detector, demanding for an on-line trigger integrated in the data acquisition system running in the shore station, called TriDAS \cite{phase2}. Due to the large optical background in the sea from \textsuperscript{40}K decays and bioluminescence, the throughput from the sea can range up to 30 Gbps. This puts strong constraints on the performances of the TriDAS processes and the related network infrastructure.

\section{The evolution of the TriDAS software}
TriDAS, designed for the prototype tower deployed by KM3NeT-Italy and running at the offshore station of Portopalo, has been checked with a long period of data acquisition, started more than two years ago. After an important upgrade of the dedicated computing infrastructure and a revision of the existing software (Fig. \ref{fig:Schema}), TriDAS will also be used for the data acquisition of the first block of the KM3NeT-Italy telescope, that is composed of 8 towers. In order to meet the new requirements, the revision of TriDAS has involved the adoption of modern software design solutions and high-level libraries and the update to the new raw data format.

\begin{figure}
\centering
\includegraphics[width=0.50\linewidth]{schema}
\caption{Scheme of TriDAS components and their interactions with external services.}
\label{fig:Schema}
\end{figure}

\subsection{Floor Control Module Server (FCMServer)}
The FCMServer represents the interface of TriDAS with the data from the off-shore detector. It performs the read-out of data coming from a number (up to 4) of floors through a dedicated ASIC \cite{ASIC}. After having performed a consistency check, it sends the data to the connected HitManager.

\subsection{Hit Manager (HM)}
The HMs are the first step of aggregation of the detector. Each HM receives data from a set of floors called sector: the sector's dimension can be dynamically decided before each run, but it is seen that the best fit is 1/2 or 1 tower. The main task of HMs is to split data into slices of a fixed time duration (about 200 ms) called SectorTimeSlices and to send them to a specific TriggerCPU, a dedicated object for the last step of aggregation and online analysis.

\subsection{TriggerCPU (TCPU)}
The TCPUs are responsabile for the last step of data aggregation and online analysis. Each TCPU receives from Hit Managers all the SectorTimeSlices related to a specific time interval and creates the TelescopeTimeSlice, rappresenting the state-of-the-art of the whole detector in a specific time interval. Once that a TelescopeTimeSlice is ready, the TCPU applies first and second level triggers in order to find interesting events and to remove backgroud noise. Finally the TelescopeTimeSlice with related events is sent to the Event Manager and stored.

\subsection{Event Manager (EM)}
The EM is the software component of the TriDAS deputed to the storage of triggered data. A single EM process collects triggered data from the whole TCPU set and performs data writing on local storage. Data are written in strict temporal order in so-called PostTrigger files (PT files), which maximum size is fixed by means of configuration parameter. Each PT file also contains all the run setup parameters and the detector geometry.

\subsection{TriDAS SuperVisor (TSV)}
The TSV is  the process responsible to decide wich SectorTimeSlices have to be sent to wich TCPU. When a TCPU can handle a TelescopeTimeSlice, it sends a token to the TSV: this last one chooses a SectorTimeSlice ID and communicates to all the HMs that ID and which TCPU has sent the token. HMs are constantly waiting for comunications from the TSV and  when a SectorTimeSlice ID is received they sent the requested SectorTimeSlice to the related TPCU. Finally the TSV has a fault tollerant mechanism that permit to resend a SectorTimeSlice ID to a different TPCU if the previous send has failed.

\subsection{Tridas Controller (TSC)}
The TSC is the software interface that permits to control the entire TriDAS enviroment. Its purpose is organize and control the launch of each sotware in order to allow a correct acquisition and real time analysis of the data coming from the sub-marine detector.
The TSC is modeled on top of a state machine that represent a stable situation of the system. Each state indicates a well know situation of the Tridas. With this model the TSC is able to understand and keep managed the state of the indipendent software that form the TriDAS. Moreover it can recover some faulty situations with some retry mechanisms.

\subsection{GUI}
The TriDAS enviroment, as described so far, is a closed system with a unique control access point located on the TSC. The TriDAS System Controller, in fact, permits to a only one “actor” at once to comunicate and query the TriDAS. But we want a collective and collaborative system in order to allow multiple reasearchers to view how the acquisition is performing and the health of the TriDAS. In order to achieve this functionality it has been made a GUI that sets itself as broker between the TSC and the rest of the world. It has the capability to allow only one “TSC controller” at once (human or not). It mirrors the TSC information so it can serve to multiple client in a faster way. On top of this the GUI is developed as single-page application (SPA) that is one of the most modern implementation of online applications.

\subsection{Tools and Libraries}
New tools for software development and modern design solutions have been adopted in order to con- tinuously improve software quality at decreasing cost \cite{ISSS}.

\begin{figure}
\centering
\includegraphics[width=0.50\linewidth]{test}
\caption{The photo shows a crate containing the electronics for two complete floors at the Bologna test bench.}
\label{fig:Test}
\end{figure}

\section{Tests at the KM3NeT Bologna Computing Infrastructure}
The TriDAS implementation is currently being tested with a setup realized in the KM3NeT DAQ laboratory at the INFN-Sezione di Bologna. The various TriDAS processes run on a dedicated computing farm interconnected according to the same network layout that will be used in production. The data stream incoming to the TriDAS is obtained with both an experimental setup, with the real electronics for two complete floors attached to two FCMServers (Fig. \ref{fig:Test}), and a set of FCMServer simulators which can run on the farm, allowing to simulate the data flow from all the towers.

\section{Conclusion}
The improved TriDAS for the 8 towers is currently under an intense test session. The aim is validating the system scalability and stability with long duration runs and with varying the the incoming throughput via the FCMServer simulators. At the same time, the trigger framework is optimized for enhancing the performances of the TCPU processes. Finally, new trigger algorithms for different kind of physics searches (e.g. multi-messangers alerts, high energy neutrino induced showers) are under development and tests. 

\section*{References}
\begin{thebibliography}{10}

\bibitem{Neutrino}
T. Chiarusi, M. Spurio, "High-Energy Astrophysics with Neutrino Telescopes", Eur. Phys. J. C65, 649 (2010)

\bibitem{km3net}
KM3NeT web site: http://www.km3net.org/home.php

\bibitem{muon}
S. Aiello et al., "Measurement of the atmospheric muon depth intensity relation with the NEMO Phase-2 tower", Astroparticle Physics, Vol. 66, Pag. 1-7 (2015)

\bibitem{phase2}
C. Pellegrino, F. Simeone, T. Chiarusi, "The Trigger and Data Acquisition for the NEMO-Phase 2 tower", AIP Conference Proceedings 1630, 158 (2014)

\bibitem{ASIC}
A. Lonardo et al., "NaNet: a configurable NIC bridging the gap between HPC and real-time HEP GPU computing" JINST 10 C04011 (2015)

\bibitem{ISSS}
F. Giacomini et al., "An integrated infrastructure in support of software development", Journal of Physics: Conference Series 513 (2014) 062018.

\end{thebibliography}

\end{document}


