\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{hyperref}

\begin{document}
\title{Advanced Virgo Computing at INFN CNAF}

\author{P Astone$^1$, A Colla$^1$, G Debreczeni$^2$ and G Gemme$^3$}

\address{$^1$ INFN, Sezione di Roma, I-00185 Roma, Italy}
\address{$^2$ Wigner RCP, RMKI, H-1121 Budapest, Hungary}
\address{$^3$ INFN, Sezione di Genova, I-16146  Genova, Italy}
\address{Virgo experiment \url{http://www.virgo.infn.it/}}

\ead{gianluca.gemme@ge.infn.it}

\begin{abstract}
A brief description of the Virgo experiment is given, and few highlights of the activity in 2014 are presented. The computing model of the experiment is outlined and particular emphasis is given to the crucial role of the resources provided by CNAF in the framework of the overall strategy. 
\end{abstract}

\section{Introduction}
Virgo is gravitational wave detector based on a kilometer-scale Michelson interferometer, which took scientific data from  May 2007 to September 2011, in four scientific runs (VSR1, VSR2, VSR3, VSR4).

Advanced Virgo is being realized trough the upgrade of the original Virgo interferometer, built by the Centre National de la Recherche Scientifique (CNRS, France) and by the Istituto Nazionale di Fisica Nucleare (INFN, Italy). The realization of Advanced Virgo is also based on the fundamental contribution from the National Institute for Nuclear Physics and High Energy Physics (Nikhef, Netherlands) and on the support coming from the Hungarian Academy of Sciences and from the Polish National Science Centre. A detailed description the main features of the upgrade and of the status of the construction can be found in \cite{AdVpaper2015}

Advanced Virgo will be part of the world network of second generation interferometers for gravitational wave detection, which includes the two LIGO detectors in USA \cite{aLIGOpaper2015}, the GEO detector in Germany \cite{GEO600paper2014} and the KAGRA detector in Japan \cite{Kagrapaper}. Farther in the future the IndIGO detector in India is also expected to join the network \cite{IndIGOpaper}. 

With a sensitivity 10 times better than first generation detectors, Advanced Virgo will be able to monitor a volume of Universe one thousand times greater, observing several tens of binary neutron star coalescences within a range of several hundred millions of light years, and possibly detecting binary black hole events billion of light years away \cite{observing_scenario}. Continuous signals emitted by distorted rotating neutron stars, as well as the stochastic background of cosmological or astrophysical origin will also be accessible with unprecedented sensitivity.                        

In parallel with the transition from first to second  generation experiments we improved the computing framework, which needed to be upgraded in order to fulfill the new and most ambitious scientific requirements.
 
The computing model (CM) for Advanced Virgo \cite{CM2013} was designed taking advantage of the experience gained so far with the data taking and analysis from the first engineering runs to the latest Science runs VSR1-VSR4 (the last run ended in September 2011). It also takes into account the technological progresses of these years, from the original Virgo plan, which is dated back to the year 2002 \cite{VIR-PLA-DIR-7000-122}. The fundamental focus of the CM is to collect the requirements of the science data analysis groups and to find optimal solutions to fulfill them. The CM also reflects needs and constraints raised from the LIGO/Virgo agreement \cite{LV_MOU}, which we have been facing during the last years, and were finally  addressed in an organized way in this CM.

\section{Activity in 2014}
During 2014 the on-site activity was dominated by the works related to the new hardware installation and integration. The most relevant project milestones completed in 2014 were the locking of the Input Mode Cleaner in June, and the start of its commissioning, and the suspension, in December, of the Beam Splitter payload, which represents the first complete system (Suspension+Payload+Mirror) integrated and suspended (see Fig. \ref{BS}). Moreover the large cryolinks were installed in the end buildings. The West End cryolink was successfully tested in December.
\begin{figure}[ht]
\includegraphics[width=22pc]{BS.png}\hspace{2pc}%
\begin{minipage}[b]{14pc}\caption{\label{BS}Large Beam Splitter integrated in December 2014.}
\end{minipage}
\end{figure} 

In the meantime the analysis of the data collected in the science runs that took place in previous years (in particular VSR2, 2009-2010 and VSR4, 2011) has been proceeding. This data analysis activity produced over the last few years a steady flux of $\approx 10$ papers/year. The collaboration papers published in 2014 are listed in the References:\cite{paper2014_01} -- \cite{paper2014_14}.

%Among the various, interesting results reported in the published papers, we cite one bea 
%
%\subsection{How high are pulsar "mountains"?}
%There are just over 350 pulsars (see the Australia Telescope National Facility catalog) spinning fast enough for their gravitational wave emission to be in the sensitive frequency band of the current detectors (∼20 to 2000 Hz). We have searched for a total of 195 of these pulsars using data from the LIGO, Virgo and GEO600 science runs, with the most up-to-date results for 179 of them coming from the most recent LIGO S6 and Virgo VSR2 and VSR4 runs. From these searches we were not able to detect evidence for gravitational radiation from any of the pulsars. But, we have produced the most sensitive upper limits yet, and for seven pulsars we are starting to probe an interesting regime within a factor of five of the spin-down limit. For the Crab pulsar and Vela pulsar we have surpassed the spin-down limit. From this we can say that, respectively, less than ∼1\% and ∼10\% of their spin-down energy loss is due to gravitational radiation. We can also say that there are no "mountains" on the Crab pulsar greater than ∼1  meter, and none on Vela greater than ∼10 meters. Among the other pulsars, we found eight more within a factor of ten of the spin-down limit. From the gravitational wave observations alone we can limit the "mountain" size for some of these to less than ∼1 mm, although the spin-down limit is more stringent for those pulsars \cite{paper2014_12}.
%\begin{figure}[ht]
%\includegraphics[width=22pc]{fig1.png}\hspace{2pc}%
%\begin{minipage}[b]{14pc}\caption{\label{fig1}The stars in this plot show the upper limits on the gravitational wave strain amplitude for 195 pulsars using data from the LIGO S3, S4, S5 and S6, and Virgo VSR2 and VSR4 runs. The triangles show the spin-down limits for a selection of these stars. The curves give estimates of the expected sensitivity of the runs and also future runs with Advanced LIGO and Advanced Virgo.}
%\end{minipage}
%\end{figure} 
%
%%\subsection{Do cosmic strings exist?}
%%Cosmic strings can give rise to a large variety of interesting astrophysical phenomena. Among them, powerful bursts of gravitational waves (GWs) produced by cusps are a promising observational signature. We searched for GWs from cosmic string cusps in data collected by the LIGO and Virgo gravitational wave detectors between 2005 and 2010, with over 625 days of live time. We find no evidence of GW signals from cosmic strings. From this result, we derive new constraints on cosmic string parameters, which complement and improve existing limits from previous searches for a stochastic background of GWs from cosmic microwave background measurements and pulsar timing data. In particular, if the size of loops is given by the gravitational backreaction scale, we place upper limits on the string tension Gμ below 10−8 in some regions of the cosmic string parameter space \cite{paper2014_13}.
%\begin{figure}[ht]
%\includegraphics[width=22pc]{fig2.png}\hspace{2pc}%
%\begin{minipage}[b]{14pc}\caption{\label{fig2}This plot presents existing constraints on cosmic string parameters: the string tension $G_\mu$, the loop size parameter $\epsilon$ and the probability $p$ that two string segments interact when they meet. Our analysis is able to reject the regions filled in red. For comparison, other constraints derived from searches of a GW background from cosmic strings (pulsar / CMB / LIGO stochastic) are given fixing $p$ at $10^{-3}$.}
%\end{minipage}
%\end{figure}
%
%\subsection{Scanning the Skies for Cosmic Explosions}
%During the LIGO and Virgo joint science runs in 2009-2010, gravitational wave (GW) data from three interferometer detectors were analyzed within minutes to select GW candidate events and infer their apparent sky positions. Target coordinates were transmitted to several telescopes for follow-up observations aimed at the detection of an associated optical transient. Images were obtained for eight such GW candidates. No optical transient was identified with a convincing association with any of these candidates, and none of the GW triggers showed strong evidence for being astrophysical in nature. Despite this null result, this search was the first of its kind, and lessons learned provide an important benchmark for future operations. This will be extremely valuable when Advanced LIGO and Advanced Virgo reach their design sensitivities by the end of this decade, when astrophysical searches triggered by gravitational-wave events are expected to be a regular occurrence \cite{paper2014_14}.
%
%\begin{figure}[ht]
%\begin{minipage}{18pc}
%\includegraphics[width=18pc]{fig3.png}
%\caption{\label{fig3}The gravitational wave skymap for event G19377 which occurred on 16th September 2010. This event was later revealed to be a blind injection. The skymap shows the probability that each location is the true source location based upon data purely from the gravitational-wave detectors. The locations of the areas observed by telescopes for this gravitational-wave event are indicated.}
%\end{minipage}\hspace{2pc}%
%\begin{minipage}{18pc}
%\includegraphics[width=18pc]{fig4.png}
%\caption{\label{fig4}A timeline for event G19377 which shows when each telescope observed the requested regions as indicated in the previous figure. Time zero is the time of the gravitational-wave event candidate. Model light curves for several sources (at an assumed distance of 30 Mpc) are shown for comparison.}
%\end{minipage}
%\end{figure}

\section{The Advanced Virgo Computing Model}
The overall computing strategy for Advanced Virgo is described in detail in the Advanced Virgo computing model \cite{CM2013}.
Virgo (and Advanced Virgo) has a hierarchical model for data production and distribution: different kinds of data are
produced by the detector and firstly stored at the EGO site in Cascina. There is no permanent data storage in
Cascina, just a buffer of 6 months of data mainly used for detector characterization.

The external comuputing centers (CCs) receive a copy of the data and provide storage resources for permanent data archiving.
They must guarantee fast data access and computing resources for off-line analyses. Finally, they must provide
the network links to the other Advanced Virgo computing resources.

For this goal a robust data distribution and access framework (based on file and metadata catalogs) is a
crucial element. For data distribution we will use the well-tested framework developed by the EGO IT
department, while for file catalogs the LDR DataFindServer has been installed (or under installation) in the CCs.
The collaboration manages also smaller CCs used to run part of some analyses, simulations or for software
developments and tests.

During science runs the Cascina facility is dedicated to data production and to different detector characterization and commissioning analysis, which have the need to run "on-line" (with a very short latency, from
seconds to minutes, to give rapid information on the quality of the data) or "in-time" (with a higher latency,
even hours, but which again produce information on the quality of the data within a well defined time scale).
The detector characterization activity gives support to both commissioning and science analysis.
Science analyses are carried out offline at the external CCs, with the only exception of the low-latency
searches. Some analysis, due to the fact that we analyze data jointly with aLIGO for many searches, are carried
on in LSC CCs.

We report here a few considerations on the Advanced Virgo CM, as these have an important impact on the work we
will need to do in 2015 and 2016 to be prepared for the first run of the detector, and thus are reflected in the computing requests for the next years. First of all, to face the huge computational demands of gravitational wave searches in the forthcoming years, there will be the need to gather the resources of many CCs into a homogeneous distributed environment
(like Grids and/or Clouds) and to adapt the science pipelines to run under such distributed environment. This
work has been started and is now in an advanced state. After a short evaluation period we found
that the Pegasus workflow scheduler perfectly suits for this purpose i.e. is able to provide a uniform
distributed job submission framework and its scheduling and accounting system is compatible
with that of LIGO.

Another very important need for the advanced detector era is to provide a Grid-enabled, aLIGO-compatible Condor cluster for Advanced Virgo. While Pegasus can be used for distributed job submission system, due to the strong
demand on sharing computing resources and for more efficient workflow development, this request
is still very important.

Another important task, which we started to face, is the possibility to run search pipelines in
GPU clusters. Due to the very high increase of computing resources which in turn has serious
financial consequences, pipeline developers must be enforced to investigate the possibility of
re-implementing the most compute intensive part of their algorithms to GPUs. This will either
result in a much more consolidated computing budget, or, with similar budget, would
open the possibility to examine qualitatively new physics using the same amount of computing
resources.

Most gravitational wave searches require the use of a network of detectors (at least Advanced Virgo and aLIGO). As a consequence, these search pipelines must be able to run either in Advanced Virgo or aLIGO CCs. It is therefore important to
develop pipelines adaptable to different environments or interfaces which hide the different technologies to the
users.

Thus the most important issues of the Advanced Virgo computing model may be summarized as follows:
\begin{itemize}
\item
guarantee adequate storage and computing resources at Cascina,  for commissioning, detector characterization and low-latency searches;
\item
guarantee fast communications between Virgo applications at Cascina and aLIGO CCs/other detectors for low-latency searches;
\item
guarantee reliable storage and computing resources for off-line analyses in the Advanced Virgo CCs (CNAF and CCIN2P3);
\item
push towards the use of geographically distributed resources (Grid/Cloud), in external CCs;
\item
push towards a homogeneous model for data distribution, bookkeeping and access.
\end{itemize}

Figure \ref{SCHEMAS} gives a big picture of the data workflow for what concerns scientific data analysis and detector characterization activities for Advanced Virgo. Possible additional CCs have also been indicated, as a resource to perform intensive data analysis computation on the most important scientific data channels (which amounts to a really negligible storage need/year). 
\begin{figure}[ht]
\includegraphics[width=22pc]{SCHEMA.png}\hspace{2pc}%
\begin{minipage}[b]{14pc}
\caption{\label{SCHEMAS}Data workflow for data analysis (DA) and detector characterization (DetChar) activities in Advanced Virgo. CC2 indicates the CNAF, CC1 indicates CCIN2P3.}
\end{minipage}
\end{figure} 

\section{The role of CNAF}
The computing usage and needs at CNAF is described in an internal document, annually updated \cite{VCN2014}.

Over the last year CNAF has mainly been used for all-sky searches for unknown isolated neutron stars by the CW group. Other activities, which required significant amount of computing resources were:
\begin{itemize}
\item
Parameter estimation and General Relativity tests by the CBC group
\item
Science data preconditioning work by the Burst and Noise studies group
\item
Narrow-band searches for isolated neutron stars by the CW group
\item
Optimazion studies for the CBC low-latency pipeline
\end{itemize}
Most of these use the Grid.

\subsection{Storage}
Table \ref{tabSTCNAF} shows the storage at CNAF by the year 2009 up to the end of 2013.
\begin{table}[ht]
\caption{\label{tabSTCNAF}Storage at CNAF since 2009. (+) means that we don't know the exact number. In 2011 data 
from Castor have migrated to GEMSS, which uses gpfs\_virgo4 as cache disk.}
\begin{center}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{lllll}
\br
Year CNAF & gpfs4 [TB] & gpfs3 [TB] & Castor or & Castor disk [TB] \\
 & used / available Virgo & used / available Virgo &  GEMSS [TB]     & used / available all exp.\\
\mr
2009            & 190 / 256           & 9 / 16    & 145 (Castor)       	& (+)\\
2010 (Oct. 1)   & 261 / (256+186)=442 & 16 / 16   & 163 (Castor)       	& 17 / 36\\
2011            & 345 / 384           & 26 / 32   & 750            		& 0\\
2012 (Oct. 29)  & 325 / 368           & 33 / 48   & 826                	& 0\\
2013 (Nov. 18)  & 254/ 379            & 67 / 48   & 826               	& 0\\
\br
\end{tabular}
}
\end{center}
\end{table}

\subsection{Computing}
At CNAF in 2014, CPU resources have mainly been used for the all-sky CW search and for the CBC
Parameter Estimation and General Relativity work. We have also demonstrated successful execution of the MBTA pipeline at CNAF and on other Italian Grid sites.
During the year 2014, an important work has been done to do the porting of CBC pipelines from an LSC
related submission method to an architecture complaint with our CCs and in particular with Grid. This has removed the limit to run CBC analyses only on LSC clusters. Tests on real data have begun in September 2013 and since then CNAF has granted to Virgo a number of cores $\mathcal{O}$(1000), which is the minimum needed to prepare the CBC analysis and to run some new CW analysis (on VSR2/VSR4 data) enlarging the parameter space covered so far.

CNAF accounting system\footnote{\url{http://tier1.cnaf.infn.it/monitor}} provides information on the total and average consumption of computing power in 2014, summarized in Table \ref{CNAF2014}: 
\begin{table}[ht]
\caption{\label{CNAF2014} Accounting at CNAF (date: January, 1st - December, 31 2014)}
\begin{center}
\begin{tabular}{lll}
\br
WCT avg [HS06.day] & CPT avg [HS06.day] & Efficiency avg [CPT/WCT]\\
\mr
4,685.89 & 5,755.12 & 1.23\\
\br
\end{tabular}
\end{center}
\end{table}

Table \ref{tabCC} shows the evolution since 2007 of the CPU consumptions. 
\begin{table}[ht]
\caption{\label{tabCC}Evolution since 2007 of the CPU used at the CNAF}
\begin{center}
\begin{tabular}{ll}
\br
Year                 & WCT [HS06.day] \\
\mr
2007                &  60\\
2008                & 240\\
2009                & 453\\
2010                & 162\\
2011                & 674\\
2012 		    	& 669\\
2013                & 850\\
2014                & 4686\\
\br
\end{tabular}
\end{center}
\end{table}

\section{Final remarks}
The CNAF support to the VIRGO experiment  is not limited to the technical access of the CC facilities. The CNAF expertise was crucial for driving the discussion in the collaboration to define our computing model and focus on right solutions.
This kind of support is even more important than the access to their hardware infrastructure. In addition, CNAF support will be fundamental for testing the porting on the GPUs located at CNAF some of most computing demanding searches of gravitational wave signals.

Finally, we note that in the near future of the Advanced detector era, our computing needs will increase: we expect by the year 2018 a need for a continuous power $\mathcal{O}$(100) kHS06. This implies that the request of computing resources of our experiment on the CNAF infrastructure in the forthcoming years, though still below that of the HEP experiments at LHC, will represent a non negligible fraction of the overall resources.

\section*{References}
\begin{thebibliography}{99}
\bibitem{AdVpaper2015} Acernese F et al 2015 {\it Advanced Virgo: a second-generation interferometric gravitational wave detector}, Class Quant Grav {\bf 32} 024001

\bibitem{aLIGOpaper2015} Aasi J et al 2015 {\it Advanced LIGO}, Class Quant Grav {\bf 32} 074001

\bibitem{GEO600paper2014} Affeldt C et al 2015 {\it Advanced techniques in GEO 600}, Class Quant Grav {\bf 31} 224002

\bibitem{Kagrapaper} Somiya K 2012 {\it Detector configuration of KAGRA–the Japanese cryogenic gravitational-wave detector}, Class Quant Grav {\bf 29} 124007

\bibitem{IndIGOpaper} Unnikishnan C S 2013 {\it IndIGO and LIGO-India: Scope and Plans for Gravitational Wave Research and Precision Metrology in India}, Int J Mod Phys D {\bf 22} 1 1341010

\bibitem{observing_scenario} Aasi J et al 2013 {\it Prospects for Localization of Gravitational Wave Transients by the Advanced LIGO and Advanced Virgo Observatories} \url{http://arxiv.org/abs/1304.0670}

\bibitem{CM2013} Virgo Collaboration 2013 {\it The AdV Computing Model} Virgo Note VIR-0129H-13 \url{https://tds.ego-gw.it/ql/?c=9474}

\bibitem{VIR-PLA-DIR-7000-122} Virgo Collaboration 2002 {\it Virgo Computing Plan} Virgo Note \url{https://tds.ego-gw.it/ql/?c=1338}

\bibitem{LV_MOU} Virgo Collaboration, LSC Collaboration 2013 {\it Memorandum of Understanding between Virgo and LIGO} Virgo Note VIR-0386A-13 \url{https://tds.ego-gw.it/ql/?c=9740}

\bibitem{paper2014_01} AAsi J et al 2014 {\it Improved Upper Limits on the Stochastic Gravitational-Wave Background from 2009-2010 LIGO and Virgo Data}, Phys Rev Lett {\bf 113} 23 231101 

\bibitem{paper2014_02} Aartsen M G et al 2014 {\it Multimessenger search for sources of gravitational waves and high-energy neutrinos: Initial results for LIGO-Virgo and IceCube}, Phys Rev D {\bf 90} 10 UNSP 102002 

\bibitem{paper2014_03} Aasi J et al 2014 {\it First all-sky search for continuous gravitational waves from unknown sources in binary systems}, Phys Rev D {\ bf 90} 6

\bibitem{paper2014_04} Aasi J et al 2014 {\it Implementation of an F-statistic all-sky search for continuous gravitational waves in Virgo VSR1 data}, Class Quant Grav {\bf 31} 16 165014

\bibitem{paper2014_05} Accadia T et al 2014 {\it Reconstruction of the gravitational wave signal $h(t)$ during the Virgo science runs and independent validation with a photon calibrator}, Class Quant Grav {\bf 31} 16 165013

\bibitem{paper2014_06} Aasi J et al 2014 {\it Search for Gravitational Waves Associated with gamma-ray Bursts Detected by the Interplanetary Network}, Phys Rev Lett {\bf 113} 1 011102

\bibitem{paper2014_07} Aasi J et al 2014 {\it Methods and results of a search for gravitational waves associated with gamma-ray bursts using the GEO--600, LIGO, and Virgo detectors}, Phys Rev D {\bf 89} 12 122004

\bibitem{paper2014_08} Aasi J et al 2014 {\it Search for gravitational radiation from intermediate mass black hole binaries in data from the second LIGO-Virgo joint science run}, Phys Rev D {\bf 89} 12

\bibitem{paper2014_09} Aasi J et al 2014 {\it The NINJA-2 project: detecting and characterizing gravitational waveforms modeled using numerical binary black hole simulations}, Class Quant Grav {\bf 31} 11 115004

\bibitem{paper2014_10} Aasi J et al 2014 {\it Search for gravitational wave ringdowns from perturbed intermediate mass black holes in LIGO-Virgo data from 2005-2010}, Phys Rev D {\bf 89} 10

\bibitem{paper2014_11} Aasi J et al 2014 {\it Application of a Hough search for continuous gravitational waves on data from the fifth LIGO science run}, Class Quant Grav {\bf 31} 8 085014

\bibitem{paper2014_12} Aasi J et al 2014 {\it Gravitational waves from known pulsars: results from the initial detector era}, Astrophys J {\bf 785} 2 119

\bibitem{paper2014_13} Aasi J et al 2014 {\it Constraints on Cosmic Strings from the LIGO-Virgo Gravitational-Wave Detectors}, Phys Rev Lett {\ bf 112} 13 131101

\bibitem{paper2014_14} Aasi J et al 2014 {\it First searches for optical counterparts to gravitational-wave candidate events}, Astrophys J Supplement Series {\bf 211} 1 7 

\bibitem{VCN2014} Virgo Collaboration 2013 {\it Virgo computing status and needs for 2014} Virgo Note VIR-0505C-13 \url{https://tds.ego-gw.it/ql/?c=9898}
\end{thebibliography}

\end{document}