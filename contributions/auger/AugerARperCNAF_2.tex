\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{Pierre Auger Observatory Data Simulation and Analysis at CNAF.}

\author{G Cataldi$^1$ and the Pierre Auger Collaboration$^2$}

\address{
$^1$ Istituto Nazionale Fisica Nucleare, sezione di Lecce, Italy.\\
$^2$ Observatorio Pierre Auger, Av. San Mart\`\i n Norte 304, 5613 Malarg$\ddot{u}$e, Argentina\\
(Full author list : http://www.auger.org/archive/authors$\_$2014$\_$12.html)
}

\ead{Gabriella.Cataldi@le.infn.it}

\begin{abstract}
The Pierre Auger Observatory is described. 
The adopted computing model is summarized and the computing
organization of the Italian part of the collaboration is 
explained.
The flux of cosmic rays above 3$\times$10$^{17}$eV has
been measured with unprecedented precision at
the Pierre Auger Observatory based on data in
the period between January 1st 2004 and December 31st 2012. 
For realizing such a precision a reliable measurement 
of the exposure must be calculated. 
\end{abstract}

\section{Introduction}
The Pierre Auger Observatory, built near the town of Malarg$\ddot{u}$e in Argentina, 
has been gathering data since January 2004 \cite{1}. 
 It reached its baseline design covering 3000 km$^2$ with 1600
water Cherenkov surface detectors (SD) overlooked by 24 fluorescence telescopes (FD) by mid 2008 and by the 
end of 2009 had accumulated a total exposure of about twenty thousand km$^2$ sr yr, 
much larger than that of all previous air shower experiments combined. 
The surface detector has a duty cycle of almost
100$\%$ collecting then the vast majority of the data which are used for spectrum measurements and 
anysotropies search. 
The simultaneous observations with both the fluorescence
and surface detectors (hybrid observations) are possible for $\sim 13 \%$ of the events 
(those observed during moonless and clear nights), for which the longitudinal 
development in the atmosphere as well as the lateral
profile on the ground can be measured. This allows the cross 
calibration between the two detection techniques
% since the UV fluorescent light emitted by the 
% nitrogen molecules excited by the electromagnetic
% component of the air shower provide an almost calorimetric measurement of the energy
% of the primaries. 
and to determine the depth of maximum development of the shower,
which encodes precious information on the composition of the primaries and the properties of the
first hadronic interactions. The studies of the cosmic rays at the highest energies with the Auger
Observatory has already allowed to start addressing many of the old questions that motivated its
construction by measuring the features present in the spectrum, searching for anisotropies in the
cosmic ray arrival directions distribution or constraining the composition of the primary cosmic
rays.

\section{Organization of the Auger analysis.}
The date acquired at the Auger observatory are daily mirrored in sites, 
located in Lyon, Fermilab and Buenos Aires. Starting from these mirroring sites, the 
data are collected by the collaboration groups and they are 
used for reconstruction and analysis. 
At CNAF the data are daily transferred from Lyon allowing an easy access for the 
italian groups.
The most challanging task in term of CPU and SE allocation is the 
simulation process. 
This process can be divided in two steps: the simulation of the shower 
development in the atmosphere and the simulation of the shower interaction with 
the experimental apparatus. The two steps show completely different problematics and 
are fully separated, making use of different codes. 
For the shower development in the atmosphere, the code is based on the Corsika library\cite{2}.
This software is not a property of the Auger collaboration and it does not require
external libraries (apart from FLUKA).
For the detector simulation, the collaboration run a property code, 
based on Geant4 and needing several libraries as external.  
The shower simulation in the atmosphere requires the use of interaction hadronic models for 
simulating the interaction processes. 
These models are built starting from 
beam measurements taken at energies much lower then the ones of interest for Auger, 
and therefore can exhibits strong differences that must be evaluated in the systematics. 
%The study of the systematics introduced by the 
%teoretical assumptions need to refer to several different interaction models and this 
%imply that the complete simulation of a single air shower is heavy in term of calculation 
%resources. 
The collaboration plans and defines through the simulation committee a massive 
production of the two simulation steps, that are executed under GRID environment. 
Concerning the second step, i.e. the simulation of the shower interaction with the experimental 
apparatus, the only GRID running environment is the so called {\it ideal detector} that does not 
consider during the simulation phase the uncertainties introduced by the data taking conditions. 

\section{Organization of the Italian Auger Computing}
The national Auger cluster is located and active at CNAF since the end of 2010. 
The choice has allowed to use all the 
competences for the management and the GRID middleware of computing 
resources that are actually present among the CNAF staff. 
The cluster serves as Computing Element (CE) and Storage Element (SE) for all the 
Italian INFN groups. 
On the CE the standard version of reconstruction, simulation and 
analysis of Auger collaboration libraries  are installed and updated, a copy of the data is 
kept, and the Databases, accounting for the different data taking conditions are up to date. 
The CE and part of the SE are included in the Auger production GRID for the 
simulation campaign. On the CE of CNAF the simulation and reconstruction 
mass productions are mainly driven from the 
specific requirements of the italian groups. 
On the remaining part of the SE, the simulated libraries, specific to the analysis of 
INFN group are kept. At CNAF there are two main running environments, corresponding to two different queues:
$auger$ and $auger\_db$. The first is mainly used for mass production of Corsika simulation, 
and for the simulation of shower interaction with the atmosphere in condition independent from 
the environmental data. The second environment ($auger\_db$) is an ad hoc configuration that allows the 
running of the offline in dependence with the running condition databases.
CNAF is at present the only GRID infrastructure where this kind of environment 
can be run. 
The particular setup uses the WNodes environment with the Database accessed from the 
instantiated virtual machines. A specific configuration allows a suitable load to the DB servers.  

\section{The flux measurement of the Ultra High Energy Cosmic Rays}
Given the very specific configuration for the Auger CNAF we restrict this section to the 
measurement that is performed at CNAF using $auger\_db$, i.e. the flux 
measurement of the hybrid detector. 
The hybrid approach is based on the detection
of showers observed by the FD in coincidence with
at least one station of the SD array. Although a
signal in a single station does not allow an independent trigger and reconstruction in SD, it is a
sufficient condition for a very accurate determination of the shower geometry using the hybrid
reconstruction. 
In order to determine the cosmic ray spectrum, a reliable estimate of the exposure is needed,
and hence a strict event selection is performed \cite{3}. 
A detailed simulation of the detector response has
shown that for zenith angles below 60$^{\circ}$, every
FD event above 10$^{18}$ eV passing all the selection
criteria is triggered by at least one SD station,
independent of the mass or direction of the incoming primary particle.
The measurement of the flux of cosmic rays using hybrid events relies on the precise 
determination of the detector exposure that is influenced by
several factors. The response of the hybrid detector strongly depends on energy and distance from
the relevant fluorescence telescopes, as well as atmospheric and 
data taking conditions. To properly take into account 
all of these configurations and their time variability, the exposure has been
calculated using a sample of simulated events
that reproduce the exact conditions of the experiment. 
%$The total systematic uncertainty on
%the calculation of the exposure ranges from 14%
%at 1018 eV to below 6% above 1019 eV [14]. 
The current hybrid exposure as a function of energy
is shown in Figure 1 compared with the exposures
of the surface detectors.

\begin{figure}[]
%\begin{minipage}{16pc}
\centering
\includegraphics[width=16pc]{Exposure.eps}
\caption{\label{label}The integrated exposure of the different
detectors at the Pierre Auger Observatory as a function of energy. The SD 
exposure in the three cases is at above the energy corresponding to 
full trigger efficiency for the surface arrays.}
%\end{minipage}
\end{figure}

%The energy spectrum reconstructed of Ultra High Energy Cosmic Rays 
%from the hybrid events is included in a paper in preparation. 
%The main systematic uncertainty is
%due to the energy assignment which relies on the
%knowledge of the fluorescence yield (3.6$\%$), atmospheric conditions (3$\%$-6$\%$), 
%absolute detector calibration (9$\%$) and shower reconstruction
%(6$\%$) [24]. The invisible energy is calculated with
%a new, simulation-driven but model-independent
%method with an uncertainty of 1.5$\%$-3$\%$ \cite{4}. 
Unfortunatly the updated flux of cosmic rays above 3$\times 10^{17}$eV that has
been measured combining data from surface and fluorescence detectors can not be included in this report since it is foreseen his publication in the next ICRC conference (http://icrc$2015.$nl).

\section{The upgrade program of the experiment}
The upgrade program  of the Auger experiment will be presented at the april meeting of  the CSN$2$, and subsequently at the Finance Board at the end of May. At CNAF several 
mass production have been runned in order to finalize and evaluate the impact of the 
new hardware on the future detector. Among the 
experimental improvement there is the possibility to use a new small sized photomultiplier\cite{6}. The motivation for this study is to extend the dynamic
range of the surface detector, reducing the impact
of saturation for high energy events with shower
axis close to a station. In order to do so an implementation of the small area PMT
in the Simulation-Reconstruction framework has been performed and a mass CORSIKA proton shower production ad hoc has been realized\cite{7}.
 


\section*{References}
\begin{thebibliography}{9}
\bibitem{1} The Pierre Auger Collaboration 2010 {\it Nucl. Instr. and Methods in Physics Research} A {\bf 613} 29
\bibitem{2} J. Knapp and D. Heck 1993 {\it Extensive Air Shower Simulation with CORSIKA, KFZ Karlsruhe} {\bf KfK 5195B}
\bibitem{3} The Pierre Auger Collaboration 2011 {\it Astropart. Phys.} {\bf 34} 368
\bibitem{4} M. Tueros for the Pierre Auger Collaboration 2013 {\it Proc. 33rd ICRC, Rio de Janeiro, Brazil}
{\bf arXiv:1307.5059}
\bibitem{5} V. S. Berezinsky and S. I. Grigorieva 1988 {\it Astron. and Astrophys.} {\bf 199}  1
\bibitem{6}
M. Aglietta et al.,  {\bf GAP$2013-021$}, {\it Small PMT – A proposal to extend the dynamic range of the Auger Surface Detector for operations beyond 2015}
\bibitem{7}
V. Scherini et al.,  {\bf GAP$2014-089$}, {\it 
Implementation of a small PMT in the Offline simulation of the Surface Detector}

\end{thebibliography}

\end{document}


