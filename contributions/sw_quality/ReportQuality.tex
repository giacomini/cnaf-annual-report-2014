\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{url}
\begin{document}

\title{A novel software quality model}

\author{M Canaparo, E Ronchieri and D Salomoni}

\address{INFN CNAF, Viale Berti Pichat 6/2, 40126, Bologna, Italy}

\ead{marco.canaparo@cnaf.infn.it, elisabetta.ronchieri@cnaf.infn.it, davide.salomoni@cnaf.infn.it}

\begin{abstract}
Existing software quality models identify a set of software characteristics that are defined by standards, such as ISO-9126, and measured by metric tools. The characteristics require a set of metrics, typically shared among them, which are measured periodically during the software development life cycle. Software quality models and metrics determine an upside-down approach to quantify high reliability
balancing effort and results. However, quality models often provide a partial view of the analyzed software, resulting unhelpful for developers during their daily activities.   

In this report, we are going to describe our solution to fulfil the aforementioned issue. We designed mathematically and tested our own
quality model prototype where we connected software best practices with code metrics. Furthermore, 
we supplied the model with statistical techniques,
such as discriminant analysis and linear regression, to predict the quality at any stage of development. 
 For the validation process, we used some EMI software products
whose defects were already known. Our solution has proved to reasonably reproduce reality.
\end{abstract}

\section{Current State}
In the context of the software development in several European projects, such as DataGrid~\footnote{The DataGrid project, \url{http://eu-datagrid.web.cern.ch/eu-datagrid/}}, EGEE~\footnote{EGEE, Enabling Grid for e-Science egee, \url{http://eu-egee-org.web.cern.ch/eu-egee-org/index.html}}, EGI~\footnote{European Grid Infrastructure (EGEE), \url{http://www.egi.eu/}} and EMI~\footnote{European Middleware Initiative (EMI), \url{http://www.eu-emi.eu/}}, researchers including those from INFN CNAF have analysed a set of quality models (such as McCall \cite{mccall}, Bohem \cite{bohem}, Dromey \cite{dromey} and ISO \cite{iso}) according to the projects' requirements. These models have revealed to be specific for the domain they were designed (e.g., telecommunication and aerospace). Furthermore, they address a subset of software characteristics \cite{iso} resulting too complex to extrapolate the needed information.
The proposed software quality model connects software best practices~\cite{CMMI} with a set of metrics~\cite{Kan} to improve the prediction of the quality of software at any stage of development.  
While for the best practices we considered those to improve the success of software development process, 
for the metrics we derived them from best practices, static and dynamic analysis. 
The best practices~\cite{Perks} we selected refer to software structure, the construction of the code,  deployment, testing and configuration management. The metrics we decided to include are specific of both best practices (such as those related to file and code conventions and software portability) and analysis~\cite{Chidamber} (such as Lines Of Code and
Number of Defects). However, we planned the validation of our model with a progressive increase in the data set in terms of product metrics and software packages to properly speculate on the variables included in the model. 

We fully described the model~\cite{RonchieriII} by using the mathematical description formalism to express various levels of abstraction from the fundamental concepts of software engineering up to metrics. It leverages predictive techniques, called risk-threshold Discriminant Analysis
(DA)~\cite{Guo} and linear regression~\cite{Fenton}, whose starting point is the measurement of the foregoing metrics, while its outcomes determine
risky software products that may contain defects. The calculated risk-threshold~\cite{Salomoni} contributes to detecting fault-prone and non fault-prone components, whilst the predictive technique determines the metrics that influence the behaviour of the component the most.
As result, DA confirmed the correctness given in~\cite{Ronchieri},
while the regression method determined an inaccurate number of defects. However, the outcomes can be improved
by increasing the data set size and better contextualizing the predictive methods.

We used some EMI products under INFN responsibilities in the EMI distributions~\cite{Aiftimiei}.
We selected source code mainly written in \texttt{Python}, \texttt{sh}, \texttt{Java}, \texttt{C} and \texttt{C++}. The analysis exploited up to 5,489 files in 52 software components amounting to a 1,570,323 total lines of code by using a Matlab-based prototype tool that codes the presented solution. The prototype classified all the components in faulty and non-faulty groups with a correctness of about $88\%$.
We validated our model enlarging the data set by increasing the number of metrics and
software products~\cite{RonchieriII}. For the former we also considered complexity metrics, while for the latter we used
CREAM (Computing Resource Execution And Management)~\cite{Andreetto},
VOMS (Virtual Organization Management System)~\cite{Ceccanti},
WMS (Workload Management System)~\cite{Cecchi} and
YAIM (Yet Another Installation Manager)~\cite{Jayalal}, WNoDeS (Worker Nodes on Demand Services)~\cite{Salomoni} and StoRM (STOrage Resource Manager)~\cite{Zappi} products released in the EMI 3 Monte Bianco distribution, to highlight similarities and differences among development scenarios. 

\section{Future Work}
Starting from the current state, there are several improvements that we can conduct in the following periods.
In the short-term, we are going to study the correlation among metrics and to express defects as function of
various metrics: the former provides us with details about 
how they influence one another; the latter determines which metric has a greater weight than others. The statistical 
computing tool, called R, represents the best tool to fulfil these achievements.
Our aim is to
identify and improve the predictive technique that reproduces reality as much as possible strengthened by our knowledge of the problem.
In the medium-term, we would like to increase the data set by adding new software products and metrics both static
and dynamic ones. Our purpose is to improve the $\%$ of correctness in the prediction of our model.
In the long-term, we will consider and adopt further predictive techniques, in addition to DA and regression, such
as k-fold cross-validation~\cite{Rodriguez} and support vector method~\cite{Lo}, to strengthen the validation of 
our model. Furthermore, we will dedicate effort to evaluate the applicability of existing software metrics thresholds to be included in our model.

\section*{References}
\bibliographystyle{iopart-num}
\bibliography{report}

\end{document}
