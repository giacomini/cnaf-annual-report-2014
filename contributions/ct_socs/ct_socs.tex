\documentclass[a4paper]{jpconf}
\usepackage{graphicx}

\begin{document}

\title{Porting the Filtered Back-projection algorithm on low-power Systems-On-Chip}

\author{ Elena Corni$^{1,2}$, Lucia Morganti$^3$, Maria Pia Morigi$^{1,2}$, Rosa Brancaccio$^{1,2}$, Eva Peccenini$^{2,4}$, Matteo Bettuzzi$^{1,2}$, Daniele Cesini$^3$, Giuseppe Levi$^{1,2}$ and Andrea Ferraro$^3$}

\address{$^1$Department of Physics and Astronomy, University of Bologna, Italy}
\address{$^2$INFN (National Institute of Nuclear Physics), Section of Bologna, Italy}
\address{$^3$INFN-CNAF, Bologna, Italy}
\address{$^4$Enrico Fermi Center for Study and Research, Rome, Italy}

\ead{lucia.morganti@cnaf.infn.it}

\begin{abstract}

Among its various applications, X-ray Computed Tomography can be
profitably applied in the field of Cultural Heritage to reconstruct
the internal structure of art objects in a non-invasive way.  In a
collaboration between the X-ray Imaging Group of the Physics and
Astronomy Department at the University of Bologna and INFN-CNAF, we
investigated the possibility of porting the X-ray Computed Tomography
reconstruction algorithm to new low power architectures typical of the
embedded and mobile market, the so-called Systems-on-Chip (SoCs).

In particular, we exploited the Graphics Processing Unit (GPU) of the
NVIDIA Tegra K1 SoC, and maximized the simultaneous use of CPU and GPU
by combining a multi-threaded OpenMP version and a CUDA version of the
reconstruction algorithm.  Our main finding is that only three Tegra
K1 boards, equipped with Giga ethernet interconnections, allow to
reconstruct as many 2D slices (of a 3D volume) per unit time as a
traditional high-performance computing node, using one order of
magnitude less energy.  These results seem very promising in view of
the construction of an energy-efficient computing system of a mobile
tomographic apparatus.

\end{abstract}

\section{Introduction}

We are embarking upon a new era in which scientific workloads that
were traditionally confined to High Performance Computing (HPC)
systems are starting to be ported to low power embedded architectures
in order to improve energy-efficiency and power consumption.  The
interest of the scientific community for these low power, low cost
Systems on Chip (hereafter SoCs) is mainly triggered by their
ever-increasing computing performances.

In this paper, we focus on the porting of a real scientific
application, and specifically the Filtered Back-projection algorithm
from X-ray Computed Tomography, to low power SoCs.

The X-ray Computed Tomography analysis of large art objects of
Cultural Heritage, carried on for both scientific investigations and
restoration purposes, is typically time-consuming and power-consuming.
Moreover, in most situations it is simply not possible to execute the
reconstruction software directly where and when the X-ray measurements
are acquired.  Hence, a natural interest arises in exploring the
possibility of running the reconstruction algorithm on a mobile,
possibly battery-powered, device.

The chosen application for the present study is the C and MPI Filtered
Back-projection algorithm for Computed Tomography reconstruction
developed by the X-ray Imaging Group of the Physics and Astronomy
Department at the University of Bologna.

The chosen SoC-based platform for the development, porting and testing
of the application is the NVIDIA Tegra K1 SoC, which is available in a
cluster of development boards located at INFN-CNAF.

All the results obtained with the low power architecture are compared
with those obtained on a typical x86 HPC node accelerated with a
recent NVIDIA GPU, belonging to a high-performance cluster which is
also located at INFN-CNAF.

The paper is organized as follows.

In Section~2 we provide details on the experimental setup, i.e. the
computing architectures, the apparatus used to measure the power
consumption, and the Dataset adopted in the tests.  In Section~3,
after a brief explanation of the Filtered Back-projection algorithm,
we describe the porting to OpenMP and CUDA that were performed for the
present work.  In the following Section~4 we evaluate the algorithm
from the points of view of performances and energy consumption for
both low power and traditional architectures, and propose a low power,
portable solution for X-ray Tomography applied to art objects.
Finally, we draw our conclusions in Section~5.

\section{Experimental setup}

\subsection{Target architectures}

For comparative purposes, all the tests presented in this work are
performed using two different architectures.

The first one (hereafter Xeon) represents a typical node in a HPC
cluster.  It is equipped with two Intel Xeon E5-2620 CPUs, 6 physical
cores each, HyperThread enabled (i.e. a total of 24HT cores in the
single node), and with a Tesla K20 GPU accelerator from NVIDIA.

The second one represents a low power alternative: a cluster of NVIDIA
Jetson Tegra K1 development boards, each one (hereafter TK1) equipped
with a quad-core, 32-bit, Arm Cortex A15 processor, and a K1 GPU from
NVIDIA.

The following Table summarizes the main features of the two
architectures.

\begin{table}[tbh!]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|l | |c|c|c|c||c|c|c|c||c|}
\hline
& CPU & Cores & RAM & Frequency & GPU & Cores & RAM & Frequency\\
\hline
Xeon & Intel Xeon E5-2620 &24&48&2500&K20&2496&5&706\\
\hline
TK1 & ARM Cortex-A15     &4&2&2300& K1&192&2&852\\
\hline
\hline
\end{tabular}
\label{tab:specs}
\caption{CPU (left) and GPU (right) specifications of the target
  architectures. RAM is given in GB; for the TK1 SoC, it is shared
  between CPU and GPU. Frequency is given in MHz.}
\end{table}

\subsection{Measuring power consumption}

The experimental apparatus used to determine the electric power
consumption of the considered architectures consists of a Tektronix
DMM4050 digital multimeter for DC current measurements of TK1, and a
Voltech PM300 Power Analyzer for AC power measurement of Xeon.

We stress that for the Xeon node we measured the power consumption
upstream of the main server power supply (measuring on the AC cable),
whereas for the TK1 board we measured the DC current absorbed.
However, this difference in the measurements should not impact
significantly on the obtained results, given the close to one cos phi
factor of the server power supply.

\subsection{Dataset}

For the tests presented in this work, we used a real Dataset from the
high-resolution tomographic analysis of the Kongo Rikishi, a Japanese
wooden statue dating back to the XIII
century~\cite{brancaccio11,brancaccioFI,kongo}.

We obtained three representative sets of images of increasing
computational complexity, named Kongo\_2048, Kongo\_1024, and
Kongo\_256 hereafter, whose sides have a size of 2048, 1024 and 256
pixels, respectively.  The number of angles at which radiographs have
been acquired for each set is 720; only for the 256-side slice set, it
is 360.


\section{The Filtered Back-projection algorithm for Tomographic reconstruction}

The process of Tomographic reconstruction (see e.g.~\cite{kak}) starts
with acquired radiographs (projections) of the art object at many
different angles and for many different heights of the detector, and
ends with a series of slices, i.e. 2D ``reconstructed'' images which
correspond to internal sections of the investigated art object at the
different heights.  The full 3D volume of the object can then be
reconstructed by superimposing a set of reconstructed slices.

For a given height on the detector, all the information needed to
reconstruct a slice of the investigated object at fixed height is
contained in the so-called \textit{sinogram}, a 2D image in which each
row contains the 1D attenuated projection of the object on the
detector at fixed height and at many different angles.

Roughly speaking, the main steps of the Filtered Back-projection
reconstruction algorithm are~\cite{kak}:

\begin{enumerate}
\item perform the 1D Fast Fourier Transform of each row of the
  sinogram in the frequency domain;
\item filter each row;
\item perform the 2D Fourier anti-transform;
\item "geometrically" back-project the anti-transformed space into the
  normal space.
\end{enumerate}

Over the recent years, the X-ray Imaging Group of the Physics and
Astronomy Department at the University of Bologna developed a C
application performing all the above steps in order to reconstruct
slices of large art objects from X-ray
radiographs~\cite{brancaccio11}.  Since the amount of acquired data is
typically very big, and the relative processing time consuming, a MPI
version of the code has been used in HPC clusters~\cite{brancaccio11,
  brancaccioFI}.

In order to run the Filtered Back-projection reconstruction algorithm
in the target architectures described above, we developed both a
multi-threaded OpenMP version and a CUDA (Compute Unified Device
Architecture, from NVIDIA) version of the algorithm, as detailed
below.

\subsection{A multi-threaded OpenMP version of the Filtered Back-projection algorithm}

For the porting of the Filtered Back-projection algorithm to OpenMP,
we marked with specific preprocessor directives the following sections
of the code to be executed in parallel by different \textit{threads}:
\begin{itemize}
\item the zero padding of the slice
\item the convolution
\item the geometric reconstruction of the slice.
\end{itemize}
The latest section, which is the most time-consuming (around 99\% of
the total execution time for the original code), is the one in which
back-projected values are assigned to each pixel of the slice.  In the
original C code, this step resulted in two nested loops on the x and y
coordinates of only those pixels which lay inside the so-called
\textit{reconstruction circle}, i.e. a circle (inscribed in the slice,
assuming the center of rotation placed at the center of the slice)
where the reconstruction process is defined.  In order to parallelize
the two original nested loops on the x and y directions of the slice,
we used the OpenMP \textit{collapse} clause.

Figure~\ref{fig:OpenMP} shows the runtime, speedup and efficiency of
the algorithm as a function of the number of threads for the Xeon
architecture (hyperthreading enabled) and for a slice in the
Kongo\_256 Dataset.

\begin{figure}[h]
\centering
\includegraphics[width=30pc]{fig1.png}\hspace{2pc}%
\begin{minipage}[b]{36pc}\caption{\label{fig:OpenMP} From top to
    bottom: runtime, speedup and efficiency of the OpenMP version of
    the Filtered Back-projection algorithm for increasing number of
    threads in the Xeon.}
\end{minipage}
\end{figure}

\subsection{A CUDA version of the Filtered Back-projection algorithm}
For the porting of the Filtered Back-projection algorithm to CUDA, we
defined the following three \textit{kernels}, i.e. functions called by
the CPU (\textit{host}) and executed on the GPU (\textit{device}) by
many \textit{threads} in parallel:
\begin{itemize}
\item the zero padding of the slice
\item the Fast Fourier Transform (FFT) and convolution
\item the geometric reconstruction of the slice.
\end{itemize}

For the typical sizes of the images in our Dataset, offloading the
zero-padding to the GPU is always timesaving.

With respect to the kernel devoted to FFT and convolution, instead, we
compared complex-complex single precision FFT speeds for NR-f from
Numerical Recipes~\cite{NR} (the one adopted in the original code),
FFTW and the CUDA CUFFT library for arrays of increasing size, as
shown in Figure~\ref{fig:fft}, and found that the CUDA CUFFT library
becomes more efficient only when the size of the slice exceeds 4096
pixel, i.e. arrays are bigger than 8192 ($2^{13}$).  Such big sizes
are not present in our Dataset, and so the FFT-kernel is actually
never called by the CPU.

\begin{figure}[tbh!]
  \centering
  \includegraphics[width=36pc]{fig2.png}
  \caption{Complex-complex single precision FFT execution time for
    NR-f, FFTW and CUFFT library for increasing array size (see
    similar findings by, e.g., \cite{benchFFT1}, \cite{benchFFT2}).}
\label{fig:fft}
\end{figure}

Finally, we wrote the geometric reconstruction-kernel so that every
thread processes one pixel of the slice, and we restricted the
computation to those pixels belonging to the square in which the
reconstruction circle is inscribed.  As naturally expected for such
image-processing problem, this part of the code is remarkably
accelerated by the CUDA implementation.  Of course, the speed of the
reconstruction process depends on the adopted number of threads and
blocks\footnote{The batch of threads that executes a kernel is
  organized in a \textit{grid} of thread \textit{blocks}.}.  After
some experiments with increasing values of \textit{dimBlock}, defined
as the size of each block of threads in the grid, we found that a
minimum value in the execution time was reached when \textit{dimBlock}
equals 16 for both Xeon and TK1 architectures.  Hence, this value of
\textit{dimBlock} is adopted as optimal in what follows.

For reference, the execution time is 0.5s for the reconstruction of
one image of the Dataset Kongo\_256 (smallest) and 7s for one image of
the Dataset Kongo\_2048 (largest) on the Xeon, while the corresponding
values on the TK1 SoC are 0.9s and 24s.

\section{Results}

A natural way to evaluate the speed of the process of Computed
Tomography reconstruction is the number of 2D slices reconstructed per
time unit.

Nonetheless, in this study we do not restrict ourselves to
time-to-solution, and we also wish to consider energy-to-solution
metrics.

Hence, in Figure~\ref{fig:results} we show the number of slices per
time unit (left plot) and per energy unit (right plot) reconstructed
using the CPU (OpenMP version) and the GPU (CUDA version) for the two
different architectures and for three characteristic images from our
Datasets.
\begin{figure}[tbh!]
\centering
\includegraphics[width=18pc]{fig3.png}
\includegraphics[width=18pc]{fig4.png}
\caption{Slices per second (left) and slices per Joule (right) of
  OpenMP and CUDA versions of the Filtered Back-projection algorithm
  executed on Xeon (orange) and TK1 boards (yellow).  The
  reconstructed images belong to the Kongo\_2048 (top), Kongo\_1024
  (middle) and Kongo\_256 (bottom) Datasets.  The rightmost bar shows
  the combined GPU+CPU solution on TK1, i.e. only 3 CPU threads are
  used for the OpenMP version (see Section~4.1).  Otherwise, the
  OpenMP version uses all the available threads, i.e. 24 threads on
  Xeon and 4 threads on TK1.}
\label{fig:results}
\end{figure}

Of course, the HPC node guarantees a higher speed than the low power
SoC, and thus a bigger number of reconstructed slice per second.
However, when it comes to energy efficiency the bars in
Figure~\ref{fig:results} show the opposite behavior, and the SoC TK1
performs much better than the Xeon.

Incidentally, the left plot in Figure~\ref{fig:results} also shows
that the GPU-version of the algorithm is generally faster than the
multi-threaded version, due to the higher number of available threads.
Still, when processing images of small sizes (see the bottom row),
performance bottlenecks in the data transfer to and from CUDA device
arise, and the OpenMP version allows to reconstruct more slices per
second than the CUDA version.

\subsection{The proposed solution: CPU and GPU combined, portable and low power}

Based on our tests, we speculated that the best performances could be
achieved using both the CPU and GPU of a TK1 board, i.e. executing the
OpenMP version and the CUDA version of the algorithm in parallel on
the SoC.

In practice, this can be done running the multi-threaded OpenMP
version of the algorithm on three of the four available CPU cores in
the TK1, while the fourth and last available core executes the GPU
version of the algorithm.  The slices reconstructed per time unit and
per energy unit with such configuration are shown by the rightmost
bars in both the left and right plots of Figure~\ref{fig:results}.

With such approach, indeed, only three TK1 boards allow to reconstruct
as many images per time unit as a traditional server, but consuming
one order of magnitude less electrical power.

\subsection{Correctness of the reconstructed images}

The images reconstructed with the different versions of the Filtered
Back-projection algorithm and for the different architectures were
compared in terms of pixel-by-pixel standard deviations with the image
reconstructed using the original, serial code.

In particular, we assumed that for values of the standard deviations
smaller than the minimum detectable value in a grayscale, i.e. the
ratio between the maximum intensity in an image and the total number
of distinguishable levels in the grayscale, two slices can safely be
considered equivalent (see Dynamic Range, e.g.~\cite{DR}).  In this
way, we checked that the slices reconstructed with all the
implementations of the algorithm used in this work, and for both Xeon
and TK1, were consistent with the slices reconstructed using the
original code.

\section{Conclusions and future work}

In this paper, we presented our experience of porting the Filtered
Back-projection algorithm used for 3D Computed Tomography
reconstruction to a low power, low cost system-on-chip, the NVIDIA
Tegra K1 (TK1).  The porting, which was done in two programming
languages, OpenMP and CUDA, so to exploit both the CPU and GPU
available on the system-on-chip, was straightforward.  The correctness
of the output of the application was always checked.

Performances were measured in terms of number of 2D slices (of a 3D
volume) reconstructed per time unit and per energy unit, and compared
with those obtained on a traditional x86 HPC node (Xeon) accelerated
with a NVIDIA K20 GPU.

If on one hand the HPC node provides better absolute performance,
i.e. number of reconstructed slices per second, than the SoC
architecture, on the other hand, the SoC results up to 30 times more
energy-efficient, i.e. number of reconstructed slices per Joule.

Finally, we proposed a combined OpenMP/CUDA approach to run the
application, that allows to obtain the same performance (slices/s) of
the HPC node using only three TK1 boards in a portable, cheap, fast,
reliable and power saving device.

We are well-aware of the slightly unfair comparison described here,
between a development board and a server built for harsh production
environments, with multiple power supplies, redundant fans, multiple
hard disks, etc.  However, we believe that this work provides good
indication of the potential of modern SoCs for specific kinds of
applications, in particular those that manage to exploit the power of
GPU.

In the future, we will work on improving the CUDA version of the
algorithm using the so-called streams, in order to better exploit the
GPU, which in our tests was never fully loaded, with a usage
efficiency close to 60\%.  Moreover, we will try to improve the data
management for the array transfers between host and device, as the
current implementation is not optimized for small sizes of the images.

\section*{References}

\begin{thebibliography}{9}

\bibitem{brancaccio11} Brancaccio, R., Bettuzzi, M., Casali, F.,
  Morigi, M.P., Levi, G., Gallo, A., Marchetti, G., and Schneberket,
  D., IEEE TRANSACTIONS ON NUCLEAR SCIENCE, 58, 4, 2011
\bibitem{kak} Kak, A.C., and Slaney, M., Principles of Computerized
  Tomographic Imaging, IEEE Press, 1988
\bibitem{brancaccioFI} Brancaccio, R., Bettuzzi, M., Casali, F.,
  Morigi, M.P., Levi, G., Gallo, A., Marchetti, G., and Schneberket,
  D., ART'11 10th International conference on non destructive
  investigations and microanalysis for the diagnostics and
  conservation of cultural and environmental heritage., FLORENCE,
  AIPnD, 1-8, 2011
\bibitem{kongo} Casali, F., Morigi, M.P., Bettuzzi, M., Berdondini,
  A., Brancaccio, R., and D'Errico, V., Restaurare L'Oriente -
  Sculture lignee giapponesi per il MAO di Torino, Nardini Editore,
  Collana Cronache 1, 38- 43, 2008
\bibitem{NR} NUMERICAL RECIPES IN C: THE ART OF SCIENTIFIC COMPUTING
  (ISBN 0-521-43108-5)
\bibitem{benchFFT1} Fast Fourier Transform (FFTs) and Graphical
  Processing Units (GPUs), slide 19, Kate Despain, CMSC828e.
\bibitem{benchFFT2} \verb|http://www.sharcnet.ca/~merz/CUDA\_benchFFT/|
\bibitem{DR} Bettuzzi, M., Brancaccio, R., Morigi, M.P., and Casali,
  F., Effective dynamic range measurement for a CCD in full-field
  industrial X-ray imaging applications, Proc. SPIE 6616, Optical
  Measurement Systems for Industrial Inspection V, 2007

\end{thebibliography}

\end{document}
