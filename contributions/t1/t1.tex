\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{makecell}

\begin{document}

\title{The INFN Tier-1: a general overview}

\author{
  L~dell'Agnello, A~Cavalli, L~Chiarelli, A~Chierici, S~Dal Pra, D~De Girolamo,
  M~Donatelli, D~Gregori, A~Mazza, G~Misurelli, M~Onofri, M~Pezzi, A~Prosperini,
  P~Ricci, F~Rosso, V~Sapunenko, A~Simonetto, S~Virgilio and S~Zani
}

\begin{abstract}
  This contribution presents a general overview of the situation at
  the INFN Tier-1, followed by some salient facts that happened during
  2014. More detailed reports on specific activities are covered in
  other contributions to this Annual Report.
\end{abstract}

\section{Current availability of resources}

The number of scientific collaborations using the INFN Tier-1 hosted
at CNAF has increased during 2014: NA62, Panda, CTA, Darkside and
Cuore have joined the existing collaborations using the computing and
storage resources available at our data center.

Table~\ref{pledged} summurizes the pledged resources at the INFN
Tier-1 by the end of 2014.

\begin{table}[h]
  \begin{center}
    \begin{tabular}{|l||r|r|r|}
      \hline
      \textbf{Experiment}
      & \makecell{\textbf{CPU} \\ (HS06)}
      & \makecell{\textbf{Disk} \\ (TB-N)}
      & \makecell{\textbf{Tape} \\ (TB)}                   \\
      \hline \hline
      LHC experiments             &  98950 & 11320 & 16190 \\
      HEP experiments             &  19700 &   610 &  4625 \\
      Astro-particle experiments  &  18093 &  2770 &  2175 \\
      Virgo                       &  10000 &   428 &   818 \\
      Nuclear physics experiments &      0 &     0 &   380 \\
      \hline \hline
      \textbf{Total} & \textbf{135546} & \textbf{15128} & \textbf{24188} \\
      \hline
    \end{tabular}
    \caption{\label{pledged}2014 pledged resources}
  \end{center}
\end{table}

\subsection{The computing farm}

At the beginning of 2014 the general-purpose farm had a total
computing power of about 190~kHS06, which was reduced during the
course of the year. The quite large ``over-pledge'' was due to old
hardware not yet dismissed.
 
\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{jobs.png}
  \caption{\label{farm}Farm usage in the period January--March 2014}
\end{figure}

Figure~\ref{farm} presents the usage of the computing farm in terms of
submitted jobs during the first quarter of 2014 and it shows how the
farm was (and is) almost always fully used, despite the large
over-pledge. The bumps visible in the graph are typically due to
maintenance interventions on the farm, for example to apply patches to
the system.

The computing resources of the general-purpose farm were then
diminished starting from March as a consequence of a serious incident:
in the night between March 8$^{th}$ and 9$^{th}$ a fire broke out at
the control panel of one of the chillers, which caused all of them to
go out of power due to the fault of the common power line. Indeed,
after the intervention of the fire squad, a quick raise of the
temperature in the computing center was registered and an emergency
shutdown of all the resources was performed to avoid major damages.

After the restoration of the power line to the chillers (on Sunday
March 9$^{th}$) the services were gradually reopened and on March
10$^{th}$ Tier-1 operations were restored, albeit with a reduced
computing capacity.

We then decided to definitively dismiss the oldest worker nodes, hence
reducing the computing power of the farm to about 175~kHS06, which was
still well above the pledge limit.

\subsection{The storage}

Storage resources, on the other hand, in 2014 have increased in size
of nearly 2~PB for the disk (bringing the total to about 15~PB and
more thatn 50~million files), while the real occupancy of the tape
storage (about 16~PB) has remained well below the pledge values (about
24~PB). At the end of 2014 a complete repack of the data on tape
has started in order to move all the data to the new TK10D drives.

Both disk space and tape space are structured as a Hierarchical Mass
Storage system and are managed by GEMSS, the Grid Enable Mass Storage
System, a home-made integration of the IBM General Parallel File
System (GPFS) with the IBM Tivoli Storage Manager (TSM). An Oracle
clustered database infrastructure is deployed for relational data
storing and retrieving.

In 2014 also the new activity of the CDF Long Term Data Preservation
has been set up and the bulk copy of data from FNAL
started~\cite{ltdp}.

Disk and tape storage services, together with the data transfer
services, are operated by the Data storage group within the Tier-1
data center unit.

\subsection{The infrastructure}

Two improvements in the basic infratructre are worth mentioning:

\begin{itemize}
\item Anti-flooding doors were installed for all Tier-1 halls.
\item The efficiency of the chillers increased with the installation
  of a by-pass circuit between the manifold outlet and the collector
  recovery.
\end{itemize}

\subsection{HPC resources}

Besides the main computing and storage resources, a small HPC farm
based on GPU cards was installed in January: in total 4 servers with
one NVIDIA K20 card and 4 servers with 2 NVIDIA K40 cards were
made available to the users.

\section{Highlights}

The most significant activities performed during 2014 in the context
of the data center are presented in other contributions to this Annual
Report~\cite{lowpower,accounting,dynamicfarm,monitoring,ltdp,network}.

This section lists some additional activities performed during the
period that are worth mentioning.

\subsection{Migration to oVirt}

To ease the deployment of virtual machines the virtualization
infrastructure has been migrated to the oVirt software
platform. Thanks to this solution the farming group is now able to
delegate machine management to single users and to interface the
infrastructure with other software tools (like Foreman, a solution
under investigation at CNAF in order to take over the old
configuration and installation system).

\subsection{Study of alternative batch systems}

We have also evaluated alternatives to LSF, like UGE (Univa Grid
Engine) and Slurm, both to address economic issues and to prevent
potential LSF scalability problems. While Slurm failed to satisfy some
basic requirements for our environment, resulting fragile and not
scalable enough when applied to our use cases (issues in term of
scalability have been reported also by other Tier-1s), the tests with
UGE offered us a real alternative. However we were able to negotiate
with IBM a reduction of the cost for the LSF license and hence the
agreement has been renewed until December 2018.

In the HEP community there is also a renewed and strong interest for
HTCondor, thanks to its top-of-the-class features; indeed, it is a
serious candidate for substituting LSF at this Tier-1 after 2018.

\subsection{Support for multi-core jobs}

At the beginning of 2014 we joined the WLCG Multicore Task Force and
in August we could enable the multi-core jobs on our farm as required
by ATLAS and CMS experiments. This was done through an innovative
configuration of the farm allowing its dynamic partition between
standard and multi-core jobs, minimizing the waiting time and
optimizing the resource usage. We were able to reach an efficiency of
over 90\%.

\subsection{Emergency shut-down procedure}

As a follow-up of the recovery from the fire incident mentioned above,
we reviewed the emergency shut-down procedures for the computing
room. If a cooling problem occurs it is vital to identify the nodes
that can be easily switched off in order to keep the room temperature
at an acceptable level: the largest fraction of the machines in the
computing room is constituted by worker nodes, which can be easily
switched off in case of emergencies, leaving time to other, more
complicated systems (for example the storage), to be taken care of
(and eventually be switched off too). The farming group implemented a
procedure to smoothly switch off all the computing nodes, using both a
software approach (a ``poweroff'' command issued via ssh) and a
hardware one (IPMI command to cut the power of a node). In the future
this procedure, still in the testing phase, could be made available to
the on-call operator. allowing us to increase security and safety of
the computing room.

\section*{References}

\begin{thebibliography}{9}
\bibitem{lowpower} Low-power CPU investigation, this report.
\bibitem{accounting} Adapting a custom accounting system to APEL, this
  report.
\bibitem{dynamicfarm} Dynamic partitioning for multi-core and
  high-memory provisioning with LSF, this report.
\bibitem{monitoring} Towards a common monitoring dashboard for the
  Tier-1, this report.
\bibitem{ltdp} Projecting the CDF computing model to the long-term
  future, this report.
\bibitem{network} The INFN Tier-1: networking, this report.
\end{thebibliography}

\end{document}
