\documentclass[a4paper]{jpconf}

\usepackage{graphicx}
\usepackage{amsmath}   

%\usepackage{lineno}

%\linenumbers

\begin{document}

%\begin{frontmatter}

\title{The $40\ \mathrm{MHz}$ trigger-less DAQ for the LHCb upgrade}

\author{D~H~Campora Perez$^1$, A~Falabella$^2$, D~Galli$^{3,4}$, F~Giacomini$^2$, V~Gligorov$^1$, M~Manzali$^2$, U~Marconi$^4$, N~Neufeld$^1$, A~Otto$^1$, F~Pisani$^{1,6}$ and V~M~Vagnoni$^4$}
\ead{antonio.falabella@cnaf.infn.it}

\address{$^1$ CERN, Geneva, Switzerland}
\address{$^2$ INFN CNAF, Bologna, Italy}
\address{$^3$Universit\`a Bologna, Bologna, Italy}
\address{$^4$ INFN Sezione di Bologna, Bologna, Italy}
\address{$^5$ Universit\`a Ferrara, Ferrara, Italy}
\address{$^6$ Universit\`a la Sapienza, Roma, Italy}

\begin{abstract}
The LHCb experiment will undergo a major upgrade during the second long shutdown (2018 - 2019), aiming to let LHCb collect an order of magnitude more data with respect to Run 1 and Run 2.
The maximum readout rate of $1\ \mathrm{MHz}$ is the main limitation of the present LHCb trigger. 
The upgraded detector will read-out at the LHC bunch crossing frequency of $40\ \mathrm{MHz}$, using an entirely software based trigger.
A new high-throughput PCIe Generation 3 based read-out board, named PCIe40,  has been designed on this purpose. 
The read-out board will allow an efficient and cost-effective implementation of the DAQ system by means of high-speed PC networks.
The network-based DAQ system reads data fragments, performs the event building, and transports events to the High-Level Trigger at an estimated aggregate rate of about $32\, \mathrm{Tbit/s}$. 
Possibile technologies candidates for the foreseen event-builder under study are InfiniBand and Gigabit Ethernet. 
In order to define the best implementation of the event-builder we are performing tests of the event-builder on different platforms with different technologies. 
For testing we are using an event-builder evaluator, which consists of a flexible software implementation,
to be used on small size test beds as well as on HPC scale facilities. 
The architecture of DAQ system and up to date performance results will be presented.
\end{abstract}

%\begin{keyword}
%Front End \sep Trigger \sep DAQ and Data Management
%
%\PACS 07.05.Hd    
%\end{keyword}

%\end{frontmatter}

\section{Introduction}

LHCb is one of the four main experiments in operation at the Large Hadron Collider at CERN \cite{LHCb:2008}. 
The LHCb main goals are to make precise measurements of CP violation parameters and studying rare decays of $b$ and $c$-quark hadrons.
It has already performed successful  physics measurements exploiting the Run 1 data and it has just started the Run 2 data taking.
The LHCb detector will be upgraded during the Long Shutdown 2, foreseen by 2018-2019, in order to record data at the maximum design LHC energy of $14\ \mathrm{TeV}$,
running at an instantaneous luminosity of $2 \times 10^{33}\mathrm{{cm}^{-2}}\mathrm{{s}^{-1}}$. 
The key features of the upgraded detector are the trigger-less readout system and the full software trigger \cite{TDR:2014}.

%\begin{figure}
%\centering
%\includegraphics[width=0.49\linewidth]{LHCb_View}
%\caption{Schematic side view of the LHCb detector.}
%\label{fig:LHCb}
%\end{figure}

\section{Trigger evolution}

The trigger system of the present LHCb detector consists of a two stage-trigger: the fixed latency, near-detector, Level-0 (L0) hardware trigger
and a software trigger, the so called High-Level Trigger (HLT). 
Aim of the L0 trigger is to reduce the bunch crossing rate to $\sim1$~MHz, at which the detector is read-out. 
The HLT applies then more advanced selections, in order to further reduce the rate to $\sim5$~KHz, for storage and offline data processing.

%\begin{figure}
%\centering
%\includegraphics[width=0.49\linewidth]{Trigger_Efficiency}
%\caption{L0 trigger efficiency as a function of the luminosity for some selected decay modes. 
%Trigger efficiency of the hadronic modes saturates at the current running luminosity of $4 \times 10^{32}\mathrm{{cm}^{-2}}\mathrm{{s}^{-1}}$.}
%\label{fig:Trigger_Efficiency}
%\end{figure}

The upgraded LHCb detector will operate at a luminosity of $2 \times 10^{33}\mathrm{{cm}^{-2}}\mathrm{{s}^{-1}}$,  five times higher with respect to Run 1\cite{Trigger:2014}. 
The $1$~MHz readout is a bottleneck because of the limited bandwidth and the limited hadronic trigger efficiency.

\section{Upgrade DAQ implementation}

\begin{figure}
\centering
\includegraphics[width=0.49\linewidth]{Event_Building_Niko}
\caption{The architecture of the upgraded LHCb readout-system.}
\label{fig:Event_Building_Network}
\end{figure}

The main feature of the LHCb upgraded detector is the trigger-less readout system, without any hardware trigger. 
The logic scheme of the readout system is shown in Figure \ref{fig:Event_Building_Network}, with the main components: 
The event builder, the Timing and Fast Control(TFC) system, The Experiment Control System (ECS) for monitoring and configuration, and the trigger Event Filter Farm (EFF).

\begin{table}[htbp]
  \centering
  \caption{Constraints for the online system.}
  \label{tab:constraints}
  \begin{tabular}{cc}
    \hline
    Event rate & 40~MHz \\
    Mean nominal event size & 100~KBytes \\
    Readout board bandwidth & up to 100~Gbit/s \\
    CPU nodes & up to 4000 \\
    Aggregated throughput & 32~Tbit/s \\
    \hline
\end{tabular}
\end{table}

The design requirements for the implementation of the trigger-less readout system are summarised in Table \ref{tab:constraints}. 
The aggregated bandwidth of the event builder network can be estimated given the foreseen nominal event size of $100\ \mathrm{KBytes}$, 
assuming the maximum event rate of $40\ \mathrm{MHz}$, to be of the order of $32\ \mathrm{Tbit/s}$. 
The size of the event-builder farm can be estimated of the of the order 500 PCs, this by assuming the input rate of a server, through the PCIe40, is about $100\ \mathrm{Gbit/s}$.

The event-builder network can be effectively implemented by using commercial local area network technologies such as Ethernet or InfiniBand. 
In the following we present the results of the first scalability tests of the event builder InfiniBand-based implementation.
The InfiniBand standard is widely used in HPC clusters, since it provides cost-effective high-speed performance.

\section{Event Building Performance Evaluator}

In order to test the InfiniBand event-builder implementation we developed a performance evaluator software. 
The software evaluator emulates the behaviour of the Readout Unit (RU) and of the Builder Unit (BU).
The RU receives event fragments from the detector and send them to the BU. 
The BU collects event fragments from all the RUs and assembles them into a complete event to be afterwards sent to the EFF. 
The orchestration of the event building is managed by an Event Manager (EM) unit. 
The EM sets a server as the BU, balancing the load among the servers.
The focus of our test is on the RDMA (\emph{Remote Direct Memory Access}) capabilities of the InfiniBand network. 
RDMA allows to write or read data by a user-space application without the intervention of the CPU and the OS, resulting in low latency communications.
Among the possible different protocols that can be implemented, we tested the \emph{push} mode. 
Running in push mode the data transfer is started by the RU (in pull mode it is started by the BU). 
The EM addresses events from the RUs to the current specific BU at a tuneable readout frequency 
(the expected visible event bunch-crossing rate at the foreseen luminosity being around $30\ \mathrm{MHz}$). 

\section{Event builder nodes tuning}
The InfiniBand adapters reach the best performance following the recommendations as explained at \cite{Mellanox:2014}. 
The PCIe motherboard configuration should be of generation 3.0, since it provides a bandwidth of $\sim126\ \mathrm{Gbit/s}$ on 16 lanes slot.
A second key aspect to be considered is the CPU power management. 
The CPU can be commanded by the Linux OS to enter into a low-power mode, called the "c-states", when in idle state. 
Another important aspect to be considered concerns the case of NUMA (\emph{Non Uniform Memory Access}) architecture of the multi-processors systems. 
In this case, the best performance can be achieved running the application on the CPU-cores directly connected to the PCIe adapter.

In order to estimate the role of these effects we measured the data transfer rate on a basic system, consisting of one BU and one RUs in a point-to-point connection. 
The CPU and HCA (Host Channel Adapter) used for these tests are reported in Table \ref{tab:pTOp_tuning}.
%y_err 0.120476653437
%y_mean 5.25296875 9.83702013109
%y2_err 0.708353890633
%y2_mean 11.816875 22.1289794007
%y3_err 0.0141481765057
%y3_mean 52.48328125 98.283298221

\begin{table}[htbp]
  \centering
  \caption{Test bed characteristic for the point-to-point rate measurements.}
  \label{tab:pTOp_tuning}
  \begin{tabular}{cc}
    \hline
    CPU & Intel Xeon CPU E5-2620 2.00GHz \\
    HCA & Mellanox MCB194A-FCAT $56\ \mathrm{Gbit/s}$ (FDR) \\
    \hline
\end{tabular}
\end{table}

To perform the tests we used the OpenMPI implementation of RDMA over InfiniBand \cite{OpenMPI}. 
The result of the tests can be seen in Figure \ref{fig:EB_Tuning}. 
The Mellanox FDR HCA allows for $54.3\ \mathrm{Gbit/s}$ maximum transfer rate (taking into account data encoding).
The measured transfer rate when the CPU power management is active, without binding the application to a specific CPU, are shown as blue dots.
The measured data transfer rate turns out to be $\sim 10\%$ of the maximum value.
Binding the application to the CPU directly connected to the PCIe adapter, we get a bandwidth average value of $\sim 11.8\ \mathrm{Gbit/s}$ (green dots).
Disabling the CPU power management we get an average value of $\sim 52.5\ \mathrm{Gbit/s}$ (red dots), corresponding to the $\sim 98\%$ of the maximum rate. 
The conclusion we reach is that the event builder software is able to saturate the network capability when the proper settings of the system are applied. 
We also observed that the transmission rate is stable with time.

\begin{figure}
\centering
\includegraphics[width=0.49\linewidth]{BW_Tuning}
\caption{Event building point-to-point transmission rate measurements. 
The blue dots represent the transmission rate seen by the BU without any optimisation applied. 
The green dots represent the results we get when binding the application to the CPU directly connected to the PCIe interface. 
The red dots represents the results obtained when disabling also the power management.}
\label{fig:EB_Tuning}
\end{figure}

\section{Scalability tests}

We performed scalability tests of the event-builder on a large scale system,
running the event-builder software evaluator at the 516 nodes Galileo cluster of the Cineca Consortium \cite{Galileo:2014}.
The size of the cluster is similar to the expected size of the event builder, however the
Galileo cluster uses InfiniBand HCAs of the QDR type, which provide a bandwidth of about $ 27\ \mathrm{Gbit/s}$. 
We first performed reference measurements of the transmission rate achievable between any two nodes of the cluster selected randomly,  
using standard performance test tools (e.g. ib\_write\_bw). 
The results of the reference measurements are reported in Figure \ref{fig:QDR_Galileo}. 
The average transfer rate we get is $ 25.7\ \mathrm{Gbit/s}$, corresponding to 94\% of the expected maximum bandwidth (single duplex).
\begin{figure}
\centering
%y mean 25.7223076923
\includegraphics[width=0.49\linewidth]{QDR_char}
\caption{Characterization of the Intel QDR HCA of the Galileo cluster. The transmission rate has been measured using standard test tools.}
\label{fig:QDR_Galileo}
\end{figure}

We run then the event builder software evaluator with different farm configurations, progressively increasing the number of nodes.
Figure \ref{fig:Scalability_Galileo} shows the average transmission rate recorded by the BU versus the number of RUs (nodes used for testing). 
The results show the good scalability of the event-builder based on the OpenMPI implementation of RDMA.
Figure \ref{fig:EB_Galileo} shows the results for a test performed using 128 nodes. 
As can be seen the data transfer is stable over time at $\sim 56\%$ of maximum full-duplex bandwidth allowed for these HCA. 
The event builder the software showed good performance in term of scalability and stability in a cluster of high complexity.

\begin{figure}
\centering
\includegraphics[width=0.42\linewidth]{c1}
\caption{Scalability test results. The measured transfer rate as a function of the number of RU nodes.}
\label{fig:Scalability_Galileo}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.49\linewidth]{128_nodes}
\caption{Data transfer rate for the BU for 128 nodes test of the event builder software.}
\label{fig:EB_Galileo}
\end{figure}


\section{Conclusion}
The DAQ system for LHCb Upgrade has been redesigned in order to cope with the higher luminosity foreseen for the Run 3 of LHCb. 
Its implementation  requires an high-throughput event builder network that must handle an aggregated traffic of the order of $32 \ \mathrm{Tbit/s}$. 
Such a network can be built using commercial hardware components such as InfiniBand.
We developed a prototype software for testing purpose.
The results of the measurements performed show that the event builder is stable and scalable up to 128 nodes.
In order to reach good performance a proper tuning of the system is mandatory.


\section*{Acknowledgments}

The authors thank the HPC User Support team at Cineca for their prompt support
during the tests.


%% bibliography
\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi

\bibitem{LHCb:2008}
LHCb Collaboration, {The LHCb detector at the LHC},
  JINST 3 (2008) S08005.  

\bibitem{TDR:2014}
LHCb Collaboration, {LHCb Trigger and Online Upgrade Technical Design Report},
  CERN-LHCC-2014-016. LHCB-TDR-016 (2014).
  
\bibitem{Trigger:2014}
LHCb Collaboration, {The Upgrade of the LHCb Trigger System},
  JINST 9 (2014) C10026.

\bibitem{Mellanox:2014}
Mellanox Technologies 2014, {Performance Tuning Guidelines
for Mellanox Network Adapters}

\bibitem{OpenMPI}
{http://www.open-mpi.org}

\bibitem{Galileo:2014}
{http://www.hpc.cineca.it/content/galileo}

\end{thebibliography}

\end{document}
