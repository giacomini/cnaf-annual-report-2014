\documentclass[a4paper]{jpconf}
\usepackage{tikz}

\begin{document}

\title{Dynamic partitioning for multi-core and high-memory provisioning with LSF}

\author{Andrea Chierici}
\ead{andrea.chierici@cnaf.infn.it}

\author{Stefano Dal Pra}
\ead{stefano.dalpra@cnaf.infn.it}

\author{Giuseppe Misurelli}
\ead{giuseppe.misurelli@cnaf.infn.it}

\author{Saverio Virgilio}
\ead{saverio.virgilio@cnaf.infn.it}

\section{Introduction}

During January 2014, The INFN--T1 was requested to enable the
execution of multi--core applications, using 8 cores simultaneously in
the same Worker Node. Even thought current batch systems are commonly
able to accept and manage a great variety of jobs, this kind of
activity requires special configurations in a typical WLCG farm: this
is because the common workload is given by single--core jobs. A
8--core job can only start when eight free slots are available at the
same time in the same node, which is an extremely unlikely event.

\section{Traditional solutions and their drawbacks}

\begin{description}
\item[Host partitioning:] this is the most simple and straightforward
  solution. A fixed subset of the Worker Nodes of the site are
  exclusively dedicated to multi--core only activity. This is
  unacceptable at the INFN--T1, because too many resources would
  remain unused when not enough multi--core processing is in progress
  or because subset may bee undersized during a boosted multi--core
  demand.
  
\item[Advanced reservation:] this is a standard feature provided by
  most batch systems, LSF included. Assuming that the batch system
  knows the maximum expected end--time for each running job, it can
  select the node where enough slots are early going to be free. The
  node can be exclusively reserved for a specific multi--core job
  during a suitable time window, when currently running jobs are
  expected to be finished.

  The duration of most of the running jobs is totally uncertain,
  varying from a few seconds (extremely frequent with {\em empty
    pilot} jobs) to a large upper run limit defined at queue level.
  This means that each time a node gets reserved, a {\em drain time}
  begins, during which no single--core jobs can be scheduled on it,
  causing free slots to be unusable until enough are available to host
  a multi--core job. Furthermore, the batch system would
  systematically trigger an advanced reservation for each pending
  multi--core job, adding more and more CPU power loss due to draining
  times.
\end{description}

The available known methods to run a mixture of single and multi core
jobs are not general enough to fulfil our case, so a different model
had to be designed.

\section{The dynamic partitioning model}
A desired feature would consist in having a varying number of nodes
dedicated to multi--core according on needs. This would be equivalent
to an auto resizing host partition, and can be defined according to
the following principles:

\begin{description}
\item[Drain one, run many:] once selected for multi--core and put on
  draining, a node should run multi--core jobs as long as possible, in
  order to minimize the impact on the drain time.
  
\item[Follow the demand:] depending on the number of pending
  multi--core jobs, more nodes should be dedicated, up to a defined
  threshold.
  
\item[Avoid emptiness:] if no multi--core jobs are being scheduled to
  a dedicated node, it should be reverted back to work with ordinary
  single core activity.

\end{description}

\section{Implementation with LSF}
The dynamic partitioning has been implemented by a few python scripts,
a couple of simple C programs and a configuration file. The main
elements are:
\begin{description}
\item[elim:] to obtain a dynamic partition, worker nodes are tagged
  with a {\tt mcore} flag. WNs whose flags equal one are dedicated to
  multi--core. This means that:
  \begin{itemize}
  \item single--core jobs cannot be dispatched to nodes having {\tt
    mcore==1}
  \item multi--core jobs must be dispatched to nodes having {\tt
    mcore==1}
  \end{itemize}
  The flag is, in LSF terminology, an {\em external load index} and
  its value is computed and reported by the WN itself, by running an
  {\em elim} script written by the administrator. This computes and
  prints to its {\tt stdout} the value of the flag every 60 seconds.

\item[esub:] for each job, at submission time, another custom script,
  the external submitter {\em esub} is executed in the submission host
  (the CREAM--CE, in our case). It distinguishes multi--core jobs from
  single--core ones by inspecting the submission parameters, then
  modifying one of them, the {\em resource request} to ask {\tt
    mcore==1} for multi--core jobs and {\tt mcore!=1} for single--core
  jobs.

\item[director:] a third script, the {\em director}, implements the
  logic of the partitioning model. It runs every six minutes and
  decides which nodes are to be added or removed from the mcore
  partition. The result is written in a Json file on a shared
  filesystem, accessible to any node in the LSF cluster. The {\em
    elim} running on each WN decides the value of its own mcore flag
  status by inspecting this file.

\item[status:] the director makes use of status information, which are
  provided by a couple of simple {\tt C} programs based on the {\tt
    lsf/lsbatch.h} api. These information are:
  \begin{itemize}
  \item the resource request of each pending or running job.
  \item the current set of unavailable or closed nodes.
  \end{itemize}

\item[configuration:] it is possible to tune the behaviour of the
  dynamic partitioning by configuring the host groups available for
  multi--core, specified as a list of racks in our case, the maximum
  number of nodes that can be in drain status, the maximum acceptable
  number of unused slots, the higher acceptable ratio of unused slots,
  and a number of other minor details.

\item[hosts:] we selected for mcore queue, WNs with 16 and with 24
  slots, thus being able to host two or three mcore jobs.
\end{description}

\begin{figure}
  \begin{center}
    \begin{tikzpicture}[node distance=15mm]
      \tikzstyle{every node}=
                [%
                  fill=green!50!black!20,%
                  draw=green!50!black,%
                  minimum size=7mm,%
                  circle,%
                  thick%
                ]

                \node (M) {$M:0$};
                \node (D) [right of=M] {$D:1$};
                \node (R) [below of=D] {$R:1$};
                \node (P) [below of=M] {$P:0$};

                %%\path [thick,shorten >=1pt,-stealth']
                \path [thick,shorten >=1pt]
                (D) edge [loop above] (D)
                (M) edge [loop above] (M)
                (P) edge [loop below] (P)
                (R) edge [loop below] (R)
                (M) [->] edge (D)
                (D) [<->] edge (P)
                (D) [<->] edge (R)
                (P) [->] edge (M)
                ;
    \end{tikzpicture}
  \end{center}
  \caption{The status transition map}
  \label{fig:statemachine}
\end{figure}

\subsection{Transitions}
The director considers each node as a member of one of four disjoint
sets:
\begin{itemize}
\item $M$: available for mcore. This is initially the set of hosts
  defined in the configuration file. Only single--core jobs can be
  dispatched to nodes in this set, and they have their mcore flag set
  to 0.
\item $D$: Assigned to mcore. A node in this set is in drain time. It
  may have single--core running jobs but no single--core can be
  dispatched there. It might have up to eight free slots. Nodes in
  this sets have their mcore flag set to 1.
\item $R$: Running only multi--core jobs, drain time finished, no free
  slots. Nodes in this sets have their mcore flag set to 1.
\item $P$: Purged from mcore. A node in this set comes from the $D$
  set, where it was found to have free room for multi--core jobs after
  many (default: three) consecutive checks. There are multi--core jobs
  still running, however single--core only can be dispatched to Nodes
  in this set. The mcore flag is set to 0.
\end{itemize}

\subsection{Dynamic partitioning}
The dynamic partitioning works like a finite state machine, as
represented in Fig.~\ref{fig:statemachine}
\begin{enumerate}
\item At $T=0$, all WNs are $w_i$ in the set $M= \{w_1,\dots,w_N\}$
\item When $Q_m>0$ multi--core jobs are queued, $k$ WN are moved from
  $M$ to $D=\{w_1,\dots,w_k\}$ by the director.
\item When a node is full of multi--core, it is moved from $P$ to $R$.
\item When a node $w_i \in D$ has free room for a multi--core and no
  jobs have started there after a timeout, it is moved from $D$ to
  $P$.
\item When more multi--core nodes are required, they are moved from $P
  \cup M$ to $D$, beginning with $P$.
\item The elim script on each node $w_i$ updates its mcore status:

  $$ mcore(w_i) = \left\{ \begin{array}{rl} 
  1 & if\; w_i \in D \cup R\\
  0 & if\; w_i \in M \cup P\\
\end{array}\right.
  $$

\end{enumerate}

\begin{figure}[!htb]
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{mcore_cumulative.png}
    \caption{MCORE: Done jobs}\label{fig:mcore_cumul}
  \end{center}
  \endminipage\hfill
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{atlas_himem_cumulative.png}
    \caption{ATLAS HIMEM: Done Jobs}\label{fig:himem_cumul}
  \end{center}
  \endminipage\hfill
\end{figure}

\section{Deployment and improvements: high memory jobs}
The dynamic partitioning system started working on the production
batch system on the 1\textsuperscript{st} of August 2014, and proved
to work successfully (Fig.~\ref{fig:mcore_no_himem}) without the need
for manual interventions.

During October, a new requirement came from the ATLAS LHC experiment
to provide resources for the so called {\em himem} jobs. These are
special single--core jobs requiring twice the RAM of an ordinary one.

To fulfil this, himem jobs are treated like multi--core ones, except
that they require two slots in the mcore partition. This way one core
remains idle, however the working one has the requested amount of
RAM. Furthermore, LSF is configured to run no more than four such jobs
per node. Doing so, resources are always granted for at least two
8--core jobs on nodes with 24 slots.

This solution has the benefit of reducing the number of unused slots,
at the cost of a little delay in the start of 8--core jobs on the
node.
\begin{figure}[!htb]
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{mcore_20days.png}
    \caption{Dynamic partition early days, August 2014,
      multi--core. The space between red and green line represents
      unused slots. Sudden submission dropdown have negative impact on
      average efficiency (see
      Fig.~\ref{fig:mcore_aug2014}). Different configurations have
      impact on the reactivity of the system, i.e. how quickly the
      partition grows or shrinks.}\label{fig:mcore_no_himem}
  \end{center}
  \endminipage\hfill
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{mc20days_03_2015.png}
    \caption{Dynamic partition, March 2015, multi--core and
      high--memory. The number of unused slots over time is much
      reduced respect to the case of
      Fig.~\ref{fig:mcore_no_himem}. By inspecting the {\em used
        slots} line it can be noted how the fill factor decreases
      when there is lack of himem jobs. The partition reaches
      greater size and dropdown are less frequent.}\label{fig:mc20days2015}
  \end{center}
  \endminipage\hfill
\end{figure}

\begin{figure}[!htb]
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{mcore_aug2014.png}
    \caption{Atlas, multi--core, 2014 Aug. The
      submission flow suddenly interrupts several times a month. As a
      consequence nodes in the mcore partition are put back at work
      with single--core jobs. Short after, submission flow restart,
      triggering the need for a new draining session.}\label{fig:mcore_aug2014}
  \end{center}
  \endminipage\hfill
  \minipage{0.49\textwidth}
  \begin{center}
    \includegraphics[width=1.0\textwidth]{mcore_mar2015b.png}
    \caption{Atlas, multi--core and high--memory, March 2015. The
      number of unused slots over time is greatly reduced respect
      to the case of Fig.~\ref{fig:mcore_aug2014}. By inspecting
      the {\em used slots} line it can be noted how the fill
      factor reduces when the himem job submission flow
      stops.}\label{fig:mc20days2015}
  \end{center}
  \endminipage\hfill
\end{figure}

%% SELECT
%% substr(date(from_unixtime(eventtimeepoch)) ::text,1,7) AS "Month",
%% numprocessors AS cores,
%% split_part(userfqan,'/',2) AS "VO",
%% COUNT(*) AS N,
%% (SUM(utime+stime+0.0)/86400)::NUMERIC(9,3) "CPT_days",
%% (SUM(runtime*numprocessors+0.0)/86400)::NUMERIC(9,3) "WCT_days",
%% (AVG((utime+stime+0.0)/(runtime*numprocessors))*100)::NUMERIC(9,3) AS "%Eff",
%% (AVG(utime+stime)/3600)::NUMERIC(9,3) AS "avg_CPT_h",
%% (AVG(runtime)*8/3600)::NUMERIC(9,3) AS "avg_WCT_h"
%% FROM gridjob
%% WHERE
%% eventtimeepoch >= to_unixtime('2015-03-01') -3600 AND
%% eventtimeepoch < to_unixtime('2015-04-01') -7200 AND
%% queue IN ('cms','cms_mcore','atlas','mcore','atlas_himem') AND
%% runtime > 180 AND
%%  split_part(userfqan,'/',2) IN ('atlas','cms') AND
%%  split_part(split_part(userfqan,'/',3),'=',2) IN ('pilot','production')
%% GROUP BY "Month","VO",cores
%% ORDER BY cores,"VO","Month";

\begin{center}
  \begin{table}[h]
    \begin{tabular}{l | r | l | r | r | r | r | r | r}
      \textit{Month} & \textit{cpu} & \textit{VO} & \textit{n} &
      \textit{CPT\_days} & \textit{WCT\_days} & \textit{\%Eff} &
      \textit{$E[CPT]\,[h]$} & \textit{$E[WCT]\,[h]$} \\ \hline 2015-03 & 1
      & atlas & 338737 & 57508.103 & 60272.685 & 67.217 & 4.075 & 34.163
      \\ 2015-03 & 1 & cms & 207049 & 57664.471 & 74034.616 & 39.111 & 6.684
      & 68.654 \\ 2015-03 & 2 & atlas & 52900 & 3529.259 & 7683.582 & 43.272
      & 1.601 & 13.944 \\ 2015-03 & 8 & atlas & 23848 & 13799.668 &
      18293.281 & 70.298 & 13.888 & 18.410 \\ 2015-03 & 8 & cms & 2798 &
      8511.604 & 11948.709 & 28.167 & 73.009 & 102.491 \\
    \end{tabular}
    \caption{Activity by core and VO, March 2015. Noticeably, the average
      efficiency of multi--core jobs ($cpu=8$) is higher than that of
      single--core. This partially compensates the cpu power loss due to draining.}
    \label{tab:acct_mar2015}
  \end{table}
\end{center}

\section{Results}
The dynamic partitioning started in August 2014, enabling access to
compute resources for 8--core jobs. Two months later it was adapted to
enable high--memory jobs too. Fig.~\ref{fig:mcore_cumul} and
\ref{fig:himem_cumul} report cumulative CPUTime and WallclockTime for
multi--core and high--memory activity. Tab.~\ref{tab:acct_mar2015}
reports {\em per cpu} accounting data for March 2015.

In order to evaluate performances, a {\em Fill Factor} was considered,
defined as $FF=\frac{used\;slots}{dedicated\;slots}$, with optimal
value $FF=1$. Fig.~\ref{fig:mcore_no_himem} shows an insufficient
behaviour, with an average fill factor $FF=0.84$ and a partition size
of 335 slots, 65 of which unused. On the other hand, the partition
grows and shrinks itself according to the multi--core jobs submission
flow (Fig.~\ref{fig:mcore_aug2014}). As expected, during time periods
of steady submission flow, the fill factor increases near to 1.

Fig.~\ref{fig:mc20days2015} shows much better performances, with
$FF=0.95$ and an average partition size of 1466 slots, 71 of which
unused. The improvement is due to a more regular submission rate and
to high--memory jobs, running in the mcore partition with 2 assigned
slots each.


\section{Conclusions}
A dynamic partitioning system for the LSF batch system has been
designed and implemented at INFN--T1. It works smoothly, provides
resources for multi--core and high--memory jobs. The system is more
efficient with smooth job submission flows and suffer with sudden
interruptions and restarts, because of the need for draining filled
resources.  (Fig.~\ref{fig:mcore_aug2014}). High--memory jobs are
helpful to reduce the number of unused slots on the draining
nodes. Having more independent multicore submitters also helps in
preventing partition collapsing.

\end{document}
