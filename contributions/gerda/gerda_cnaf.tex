\documentclass[a4paper]{jpconf}
\usepackage{graphicx}

%\bibliographystyle{iopart-num}
%\usepackage{citesort}

\begin{document}
\title{The GERDA experiment}
\author{E. Medinaceli on behalf of the GERDA collaboration}
\address{Universit\`{a} di Padova, via Marzolo 8, 35100 Padova, Italy}
\ead{medinaceli@pd.infn.it}

\begin{abstract}
GERDA (GERmanium Detector Array) is an experiment searching for the neutrino-less double beta 
decay ($0\nu\beta\beta$) of the isotope $^{76}$Ge. The first part of the experiment 
was carried on between 2011 and 2013. The second phase is presently under preparation at the 
Laboratori Nazionali del Gran Sasso of INFN. This contribution briefly reviews the basic 
facts and results achieved by GERDA, with a particular focus given on computing.
\end{abstract}

\section{The experiment, latest results and on-going upgrade}
The GERmanium Detector Array (GERDA), located at the INFN underground laboratory of 
Gran Sasso, is an experiment searching for the neutrino-less double beta 
decay ($0\nu\beta\beta$) of $^{76}$Ge. This type of decay, which is predicted 
by several extensions of the Standard Model of particle physics, is a process that 
violates the lepton number conservation by two units. The decay is possible only if neutrinos 
have a Majorana mass component. In the assumption that the decay is mediated by the Majorana 
neutrino mass, its half-life is directly connected to the neutrino absolute mass, which is 
presently unknown: neutrino oscillation experiments provide a measurement of the mass splitting 
of the neutrino eigenstates, but cannot pinpoint the absolute mass scale. The $0\nu\beta\beta$ 
decay, if exists at all, is a very rare process, with half-life exceeding 10$^{25}$~yr. 
Therefore, experiments aiming to search for it must feature a ultra-low background.  \\
The experiment uses an array of high-purity germanium (HPGe) detectors immersed in a new 
shielding concept of liquid argon and water~\cite{ref:tech,ref:phase1}. The HPGe detectors 
are made from germanium isotopically enriched to about 86\% in the isotope of interest, $^{76}$Ge. 
An array of 
HPGe detectors is deployed in a 64 m$^3$ cryostat filled with liquid argon (LAr). 
The LAr serves as cooling medium and shielding against external backgrounds. The shielding is 
complemented by 3~m of ultra-pure water instrumented with photo-multipliers to detect 
Cherenkov light emitted by cosmic ray muons \cite{ref:tech}. The LAr volume was used 
as a passive shielding 
during the first phase of GERDA, but will be operated as an active veto in the Phase~II, to 
further suppress the residual background. \\
The signature for $0\nu\beta\beta$ decay is a single peak at $Q_{\beta\beta}$-value of 
the decay (2039~keV for $^{76}$Ge). Pulse shape discrimination techniques of the charge 
signal from the HPGe detectors are employed to improve the experiment sensitivity. 
The so-called ``GERDA Phase I'' took data between November 2011 and May 2013 with eight 
enriched HPGe detectors from the predecessor experiments Heidelberg-Moscow~\cite{ref:hdm} 
and IGEX~\cite{ref:igex}, and 
five new-generation custom-made enriched detectors of BEGe type. A total  
total exposure of 21.6 kg$\cdot$yr was collected with a mean background index at 
$Q_{\beta\beta}$ of $10^{-2}$ counts/(keV kg yr). \\
A blind analysis approach was pursued: events 
in the region of interest around $Q_{\beta\beta}$ were initially not made available for analysis. 
The blinded region was 
opened only when all cuts and algorithms had been frozen. No signal
was observed and a lower limit was derived for the half-life of $0\nu\beta\beta$ decay of $^{76}$Ge, 
$T^{0\nu}_{1/2}>2.1\times10^{25}$~yr (at 90\% C.L.)~\cite{ref:phase1}.
These results show no indication of a peak at $Q_{\beta\beta}$, i.e. the 
claim for the observation of $0\nu\beta\beta$ decay in $^{76}$Ge based on the Heidelberg-Moscow 
data was not confirmed~\cite{ref:claim}. A background model has been developed in GERDA~\cite{ref:bckg} to 
describe the observed energy spectrum outside the blinded region at $Q_{\beta\beta}$. The 
model contains several contributions, that are expected on the basis of material screening or 
that are established by the observation of characteristic structures in the energy 
spectrum. The model was used to predict the intensity and the spectral shape of the background 
in the region of interest around the $Q_{\beta\beta}$, before the actual unblinding. \\
GERDA is currently in the transition to the so-called ``Phase II'' aiming to significantly 
increase the experimental sensitivity by collecting a larger exposure (about 100 kg$\cdot$yr) and 
by further suppressing the residual background. About 30 new-generation BEGe type enriched HPGe 
detectors~\cite{ref:beges} will be deployed, which will more than double the total available 
germanium mass. A major upgrade is the instrumentation of the 
LAr volume surrounding the detector array as an active veto system. The goal
of GERDA Phase~II is to achieve a background index of the order of $10^{-3}$ counts/(keV kg yr) at 
$Q_{\beta\beta}$. Given an exposure of 100 kg$\cdot$yr, this would yield a sensitivity on $T_{1/2}^{0\nu}$ 
of about $10^{26}$~yr. Currently the experiment is taking the first commissioning 
data for Phase~II.   

\section{GERDA computing}
The two key paradigms adopted for the offline data processing are: (1) blinding and (2) hierarchical 
structure. Due to the requirement of blinding events in the region of interest at $Q_{\beta\beta}$, 
the raw data files produced by the DAQ system cannot be immediately distributed to the Collaboration, 
but have to be pre-processed to remove ``sensitive'' events. The original data files must be stored 
on a protected disk area and then re-processed for the unblinding. \\
The analysis chain is performed in a layer of storage file structures, whose level corresponds to a 
different stage of the reconstruction of the event and to a different kind of information. The first 
level of the data (raw data), which contain pulse shapes and other digitizer information 
(time stamps, etc.), is produced directly by the DAQ and has its own specific binary format. Data 
at this level is referred as Tier0.  Raw data are hence converted into a standardized format, 
which is based on the MGDO libraries~\cite{ref:mgdo}, and stored as \textsc{Root} files \cite{ref:root}. This 
second level of the data structure is referred as Tier1. The Tier1 files basically contain the same 
information as the Tier0 (waveforms and digitizer data), except for the events falling into the 
region of interest: they are removed from the Tier1 file in the process and stored separately. Having 
a standardized exportable and stable format is functional to the decoupling of the higher-level 
analysis from the specific binary format of the raw data: this allows the same algorithms to 
be used plug-and-play, irrespectively of the binary format of the parent DAQ system.\\ 
Afterward, waveforms are analyzed individually by the application of a chain of digital signal 
processing algorithms. The algorithms are coded as nearly-independent modules within the 
software framework 
\textsc{Gelatio}~\cite{ref:gelatio}, thus to provide the flexibility to produce many user-customized 
chains of signal processing. The output of the \textsc{Gelatio} modules (e.g. rise time, pulse 
amplitude, etc.) are stored as a \textsc{Root} file (labeled Tier2) and used for higher-level analysis. 
Finally, quality cuts, energy calibration and pulse shape discrimination are performed, whose 
results are stored in new \textsc{Root} files denoted as Tier3 and Tier4. In the path from the Tier1 to 
Tier4, the physics information is extracted and digested for the final analysis and 
the size of the files shrinks by a factor of $\sim 1000$.  The final physics analysis is 
performed on the Tier4 files. \\

Being GERDA a low-background experiment, the data throughput is modest. In Phase~I, it was 
about 4~GB/day for the physics data taking, plus 20~GB/week for calibrations. The data rate, 
including the pre-processing and the basic reference offline analysis, could be handled by using 
the GERDA computing resources at Gran Sasso. \\
The Collaboration policy requires three backup copies of the raw data to be made, in addition to the 
master version which is stored at the Gran Sasso laboratory. The copies must be kept in Italy, in 
Germany and in Russia. The CNAF center in Bologna is acting as the Italian backup center for the 
GERDA data, which are kept on disk and on tape. All raw data of GERDA Phase I, plus different sets 
of data coming from the GERDA Commissioning and from detector characterization campaigns , 
are stored at CNAF. The 
virtual organization (VO) \texttt{gerda.mpg.de} has been made available for GERDA to access the GRID 
resources at CNAF and all files are registered in the corresponding catalog. Currently the 
amount of disk usage of these data is around 9~TB; Phase~I data account for about 4~TB. Higher-level 
TierX files are not stored at the moment because they are always reproducible by re-processing 
the initial raw files.\\
 
The complete suite of GERDA analysis software is also installed at CNAF. Several test were  
performed using data from GERDA Phase~I, and new tools are being developed for the analysis of 
Phase~II data. It is expected that data from GERDA Phase~II, including commissioning data and 
calibration results will also be store at CNAF. 
The anticipated amount of data for the entire GERDA Phase~II is about 20$\div$30~TB. The increase 
with respect to the Phase~I is mainly due to the read-out of the LAr instrumentation and to 
the higher number of active HPGe detectors. All files will be registered to the catalog of 
the GERDA VO. \\
In the next few months, CNAF resources other than storage will be employed, and specifically CPU. 
It is foreseen to run and store on the CNAF facilities dedicated GERDA Monte Carlo 
simulations based on the software MaGe~\cite{ref:mage}. Monte Carlo simulations will be required to 
build a background model for Phase~II and to benchmark the new pulse shape discrimination 
algorithms that are being developed.


\section*{References}
\begin{thebibliography}{9}
\bibitem{ref:tech}K.-H. Ackermann \emph{et al.} (GERDA Collaboration), Eur. Phys. J. C (2013) 73:2330
\bibitem{ref:phase1}M. Agostini {\it{et al.}} (GERDA Collaboration), Phys. Rev. Lett. 111 (2013)  122503
\bibitem{ref:hdm}H.V. Klapdor-Kleingrothaus \emph{et al.} (Heidelberg-Moscow Collaboration), 
Eur. Phys. J. A (2001) 12:147
\bibitem{ref:igex} C.E. Aalseth \emph{et al.} (IGEX Collaboration), Phys. Rev. D 65 (2002) 092007
\bibitem{ref:claim} H.V. Klapdor-Kleingrothaus \emph{et al.}, Phys. Lett. B 586 (2004) 198
\bibitem{ref:bckg}M. Agostini {\it{et al.}} (GERDA Collaboration), Eur. Phys. J. C (2014) 74:2764
\bibitem{ref:beges} M. Agostini {\it{et al.}} (GERDA Collaboration), Eur. Phys. J. C (2015) 75:39
\bibitem{ref:mgdo}M. Agostini, {\it{et al.}}, J. of Phys.: Conf. Ser. 375 (2012) 042027
\bibitem{ref:root}R. Brun and F. Rademakers, Nucl. Inst. \& Meth. in Phys. Res. A 389 (1997) 81
\bibitem{ref:gelatio}M. Agostini, {\it{et al.}}, J. of Phys.: Conf. Ser. 368 (2012) 012047.
\bibitem{ref:mage}M. Boswell {\it{et al.}}, IEEE Trans. Nucl. Sci. 58 (2011) 1212
\end{thebibliography}
\end{document}
