\documentclass[a4paper]{jpconf}
\usepackage{graphicx}

\bibliographystyle{iopart-num}
%\usepackage{citesort}

\begin{document}
\title{CUORE experiment}

\author{CUORE collaboration}

%\address{}

\ead{cuore-spokesperson@lngs.infn.it}

\begin{abstract}
  CUORE is a ton scale bolometric experiment for the search of neutrinoless double beta decay in $^{130}$Te.
The detector is in the advanced commissioning phase at the Laboratori Nazionali del Gran Sasso of INFN, in Italy.
It is composed by an array of 988 TeO$_2$ bolometers, for a total mass of 0.75$\,$ton.
Based on the performance of CUORE-0, a pilot experiment which is running since march 2013, the projected CUORE sensitivity for the neutrinoless double beta decay half life of $^{130}$Te is of 10$^{26}\,$y after five years of live time.
The migration of the CUORE data processing to the CNAF computing cluster has started 2014, and a more intense use of resources is expected in 2015.
\end{abstract}

\section{The experiment}
The main goal of the CUORE experiment~\cite{Artusa:2014lgv} is to search for neutrinoless double beta decay (0$\nu$DBD) of the isotope $^{130}$Te.
In this spontaneous decay a nucleus changes its atomic number by two units, and two electrons are emitted.
Its observation would imply the Majorana nature of the neutrino mass, and could give information on the neutrino mass hierarchy and absolute scale.
To date there is no experimental evidence for this decay, and the half life limits lie in the range 10$^{22}\,\div\,$10$^{25}\,$y, depending on the isotope that is being considered.
In a calorimetric detector, the sum energy of the two electrons emitted in 0$\nu$DBD produces a sharp peak in the spectrum, centered at the Q-value of the decay.
Typical 0$\nu$DBD Q-values are in the few MeV range, therefore the tiny signal is submerged by background from natural radioactive decays.
The CUORE detector is an array of 988 $^{nat}$TeO$_2$ bolometers, with a total mass of 741$\,$kg (201$\,$kg of $^{130}$Te).
The bolometers are arranged in 19 towers, each tower is composed by 13 floors of 4 bolometers each.
A single bolometer is a cubic TeO$_2$ crystal with 5$\,$cm side and a mass of 0.75$\,$kg, equipped with a neutron transmutation doped (NTD) germanium sensor for signal readout.
The TeO$_2$ crystals are coupled to a copper support structure by mean of small PTFE supports.
The bolometer array is enclosed in a dilution refrigerator whose mixing chamber is cooled to $\sim$10$\,$mK and thermally coupled to the copper support structure of the detectors.
The CUORE bolometers act at the same time as source and detectors for the sought signal.
When a particle interacts in a CUORE crystal, it produces a sizable temperature rise, $\Delta$T=E/C, that can be read by the NTD sensor.
The CUORE collaboration aims at reaching a background of 10$^{-2}\,$counts/(keV$\cdot$kg$\cdot$y) in the region of the energy spectrum where the 0$\nu$DBD signal is expected ($Q_{\beta\beta}\,\simeq\,$2528$\,$keV), and a FWHM energy resolution of 5$\,$keV.
With these parameters, the experiment will reach a $^{130}$Te half-life sensitivity of about 10$^{26}\,$y in five years of live time.

\section{Status of CUORE and CUORE-0}
The CUORE experiment is currently in the advanced commissioning phase at the Laboratori Nazionali del Gran Sasso of the INFN, Italy.
All the 19 bolometer towers were successfully built and are now stored underground in nitrogen overpressure.
The commissioning of the CUORE cryostat is ongoing.
In 2014 the cryostat reached a stable base temperature of 6$\,$mK.
After this test the detector wires running from the mixing chamber to room temperature were installed, and a mini-tower of 8 bolometers was operated for the first time in the CUORE cryostat at the end of 2014.
The commissioning of the cryostat will continue during the first half of 2015. In autumn the CUORE towers will be installed in the cryostat and the detector cool down will start by the end of the year.

To check the effectiveness of the CUORE detector assembly procedure, a first CUORE-like tower made of 52 bolometers, named CUORE-0~\cite{Aguirre:2014lua}, was built and is taking data in the former Cuoricino~\cite{Andreotti:2010vj} cryostat since April 2013.
From a study of the CUORE-0 background spectrum and with the aid of Monte Carlo simulations, it could be possible to evince that the CUORE background goal of 10$^{-2}\,$counts/(keV$\cdot$kg$\cdot$y) is within reach.
Moreover CUORE-0 measured an average energy resolution slightly better than 5$\,$keV FWHM on the 2615$\,$keV photoelectric peak from $^{208}$Tl, in perfect agreement with the energy resolution goal of CUORE.

\section{CUORE computing model and the role of CNAF}
The CUORE and CUORE-0 raw data consist in Root files containing events in correspondence with energy releases occurred in the bolometers.
Each event contains the waveform of the triggering bolometer and of those geometrically close to it, plus some ancillary information.
Root files also contain some non event-based information, such as the run start date and time and the run type (background or calibration).
All the non event-based information is also stored in a PostgreSQL database that is also accessed by the offline data analysis software.
The data taking is organized in runs, each run lasting about one day.
%A few days of detector calibration data taking are perfomed every about one month of background data taking.
Raw data are transferred from the DAQ computers to the permanent storage area at the end of each run.
In CUORE-0 about 500$\,$GB/y of raw data are produced, while for CUORE about 20$\,$TB/y of raw data are expected.
%In background runs CUORE-0 produces about 300$\,$MB/day of raw data.
%In calibration runs the triggering rate is higher and about 3$\,$GB/day of raw data are produced.
%For CUORE we expect about 20$\,$GB/day and 200$\,$GB/day of raw data for background and calibration runs, respectively.

The CUORE-0 and CUORE data analysis flow consists in two steps.
In the first level analysis the event-based quantities are evaluated, while in the second level analysis the energy spectra are produced and studied.
The analysis software is organized in sequences.
Each sequence consists in a collection of modules that scan the events in the Root files sequentially, evaluate some relevant quantities and store them back in the events.
The analysis flow consists in several fundamental steps that can be summarized in pulse amplitude estimation, detector gain correction, energy calibration and search for events in coincidence among multiple bolometers.

Most of the CUORE-0 data analysis and simulations were run on a dedicated computing cluster located at the Roma1 division of INFN.
Since 2014 a transition phase has started to move the CUORE-0 and CUORE analysis and simulation software to CNAF.
In 2014 the CUORE data analysis software was installed, configured and tested at CNAF, and some CUORE and CUORE-0 Monte Carlo simulations were run.
In 2015 a much more intense usage of the CNAF computing resources is expected, both in terms of data analysis (reprocessing of the CUORE-0 data) and simulations.
When the CUORE data taking will start, it is expected that the primary data storage and a computing cluster for basic analysis will be located at LNGS. 
The data processing and the simulations will be run at the CNAF computing cluster, which will also host a copy of the raw data and of the data analysis root files.
\section*{References}
\bibliography{cnaf_cuore}

\end{document}
