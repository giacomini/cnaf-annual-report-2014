\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{amssymb}

\begin{document}
\title{OPERA Experiment}

\author{S.~Dusini, on behalf of the OPERA Collaboration}

\address{INFN, Sez. di Padova, via Marzolo 8, 35131 Padova, Italy}

\ead{stefano.dusini@pd.infn.it}

\section{The OPERA experiment}

OPERA~\cite{proposal1,status_report} is a long-baseline neutrino experiment designed to observe for the first time the $\nu_\mu\rightarrow\nu_\tau$ oscillation in direct appearance mode through the detection of the production of the corresponding $\tau$ lepton in the CNGS $\nu_\mu$ beam \cite{CNGS} over a baseline of $730~\mbox{km}$. 
OPERA is an hybrid detector consisting of two instrumented targets, each followed by a muon spectrometer. Each target is a succession of walls of ''ECC bricks'' interleaved with planes of scintillator strips. The ECC bricks are made of 56 1-mm thick lead plates providing the mass interleaved with emulsion films with a micrometric resolution. The total number of ''ECC-bricks'' is about 150000 for a total target mass of $\sim 1.2~{\rm kton}$. The OPERA detector is located in the Gran Sasso underground laboratory (LNGS) in Italy. For a detailed description of the detector we refer to \cite{operadetector}.\\
The CNGS beam has run for five years, from 2008 till the end of 2012, delivering a total of $17.97 \times 10^{19}$ protons on target yielding 19505 neutrino interactions recorded in the OPERA targets. The analysis of these events is still in progress. 
\section{Recent OPERA results}
In this section we rrport the most important results obtained by the OPERA collaboration during the 2014.  
\subsection{$\nu_\tau$ appearance search}
\label{stat}
So far the OPERA Collaboration observed four $\nu_{\tau}$ candidates: two candidate events are in the $\tau\to h$ channel~\cite{Agafonova:2010dc,Agafonova:2014ptn}, one in the $\tau\to 3h$ channel~\cite{Agafonova:2013dtp} and one in the $\tau\to\mu$ channel~\cite{Agafonova:2014bcr}.
In the data sample analysed so far the expected $\nu_\tau$ signal in all decay channels is $2.11\pm 0.42$ events (for $\Delta m^2_{32} = 2.32\times 10^{-3}\ {\rm eV}^2$~\cite{Beringer:1900zz} and $sin^2(2\theta_{23}) = 1$), while the total expected background 
is $0.233\pm 0.041$ events. 
The significance of the observed four $\nu_\tau$ candidate events has been estimated with two methods. 
The first method combines the $p$-values of the single channels ($p_i$ for $i = h, 3h, \mu, e$) according to Fisher's rule into the estimator $p^{*}=p_h p_{3h} p_\mu p_e$. In order to take into account the systematic uncertainties of the backgrounds, 100 sets of randomised backgrounds are generated. A mean $p$-value of 1.24 $\times$ 10$^{-5}$ is obtained by a Monte Carlo calculation of the tail probability corresponding to the observed value of $p^{*}$. With this method the absence of the of signal can be excluded with a significance of 4.2$\sigma$.\\
The second method is based on the likelihood ratio test~\cite{Cowan:2010js} with the likelihood function defined as
\begin{equation}
\mathcal{L} (\mu) = \prod^{4}_{i=1}\frac{e^{-(\mu s_i + b_i)} (\mu s_i + b_i)^{n_i}}{n_{i}!},
\quad i = h, 3h, \mu, e 
\label{likelihood}
\end{equation}
where the parameter $\mu$ determines the strength of the signal process ($\mu = 0$ correspond to the background-only hypothesis), $s_i$ and $b_i$ are the numbers of expected signal and background events, $n_i$ the number of observed events. The systematic uncertainties of the backgrounds were taken into account in a similar way as above. The $p$-value of the observed statistic is $1.03\times 10^{-5}$ corresponding to a significance of 4.2$\sigma$ for the exclusion of the null hypothesis.\\
In OPERA the rate of $\nu_\tau$ CC interactions is given by 
\begin{equation}
N_\tau = A\int_{E_{th}\simeq 3.5\ {\rm GeV}} \phi_{\nu_\mu}(E)P_{\nu_\mu\to\nu_\tau}(E)\sigma^{CC}_\tau (E) \epsilon(E) dE	
\label{ntau}
\end{equation}
where A is a normalization constant proportional to the detector mass, $\phi_{\nu_\mu}(E)$ in the $\nu_\mu$ flux at the detector location, $P_{\nu_\mu\to\nu_\tau}(E)$ is the oscillation probability, $\sigma^{CC}_\tau (E)$ is the cross-section for $\nu_\tau^{CC}$ interactions and $\epsilon (E)$ is the $\nu_\tau$ detection efficiency. The lower limit of the integral is given by the energy threshold for $\tau$ lepton production. On the CNGS beam at the distance of 730 Km the effective two state oscillation probability can be approximated by expanding the oscillation term in power series, and Eq. (\ref{ntau}) can be approximated as  
 \begin{equation}
N_\tau \simeq  1.61\ A\  
\sin^2(2\theta_{23}) \left (\Delta m^2_{32}[{\rm eV^2}] \right )^2 L^2[{\rm km}]\int_{E_{th}\simeq 3.5\ {\rm GeV}} \phi_{\nu_\mu}(E)\sigma^{CC}_\tau (E) \epsilon(E) \frac{dE}{E^2}\ .
\label{ntau_opera}
\end{equation}
In this approximation $N_\tau$ varies as $\left (\Delta m^2_{32}\right )^2$ and the number of observed $\nu_\tau$ candidates can be used to measure $\Delta m^2_{32}$.  Given the 4 observed events and the expected background of (0.233 $\pm$ 0.041) events, the confidence interval of $\Delta m^2_{32}$ has been estimated with the Feldman-Cousins method \cite{Feldman:1997qc}, assuming maximal mixing. The systematic uncertainties of signal and background expectations are taken into account to marginalise the likelihood function used for the ordering principle. The 90\% confidence interval for $\Delta m^2_{32}$ is $[1.8,\ 5.0]\times 10^{-3}~{\rm eV}^2$. An alternative method using a Bayesian approach~\cite{Beringer:1900zz} with a flat prior on $\Delta m^2_{32}$ gives for the credible interval of $\Delta m^2_{32}$ the values $[1.9, 5.0]\times 10^{-3}~{\rm eV}^2$.
\subsection{Search for sterile neutrinos in $\nu_\mu\to\nu_\tau$ oscillations}
\label{exo}

The study of $\nu_\mu\to\nu_\tau$ oscillation over a long-base line can also be use to set limits on the existence of sterile neutrinos~\cite{Pontecorvo:1967fh}. In the simplest extension of the three-neutrino mixing model with the addition of one massive neutrino $\nu_4$ with mass $m_4$, the oscillation probability is a function of the $4\times 4$ mixing matrix $U$  and of the three squared mass differences.
Defining $C=2|U_{\mu 3}||U_{\tau 3}|$, $\Delta_{ij}=1.27~\Delta m^2_{ij}~L/E$ 
($i$,$j$ = 1,2,3,4), $\phi_{\mu \tau}=~Arg(U_{\mu 3}U^*_{\tau 3}U^*_{\mu 4}U_{\tau 4})$ 
and $\sin 2\theta_{\mu \tau}=2 |U_{\mu 4}| |U_{\tau 4}|$, 
the $\nu_{\mu} \rightarrow \nu_{\tau}$ oscillation probability \textit{P(E)} 
can be parametrised as:

\begin{eqnarray}
P(E)&=&C^2~\sin^2 \Delta_{31} + \sin^2 2\theta_{\mu \tau} \sin^2 \Delta_{41} 
+ \frac{1}{2}C\sin2\theta_{\mu \tau}\cos\phi_{\mu \tau}\sin 2 \Delta_{31} ~\sin 2 \Delta_{41}\nonumber\\
&-& C\sin 2\theta_{\mu \tau}\sin \phi_{\mu \tau}\sin^2 \Delta_{31} \sin 2 \Delta_{41} 
+ 2C\sin 2\theta_{\mu \tau} \cos \phi_{\mu \tau} \sin^2 \Delta_{31} \sin^2 \Delta_{41}\nonumber\\
&+& C\sin 2\theta_{\mu \tau} \sin \phi_{\mu \tau} \sin 2 \Delta_{31}  \sin^2 \Delta_{41} 
\label{eq1}
\end{eqnarray}
where $\Delta m^2_{31}$ and $\Delta m^2_{41}$ are expressed in
$\mathrm {eV^2}$, $L$ in km and $E$ in GeV. Given the long baseline and
the average CNGS neutrino energy, \textit{P(E)} is independent of
$\Delta m^2_{21}$. 
The terms proportional to $\sin \phi_{\mu \tau}$ are CP-violating, while those
proportional to $\sin 2 \Delta_{31}$ are sensitive to the
mass hierarchy of the three standard neutrinos, normal ($\Delta
m^2_{31}>0$) or inverted ($\Delta m^2_{31}<0$). 
For $\Delta m_{41}^2 \gtrsim 1~{\rm ev^2}$, which is the region indicated by the neutrino oscillation anomalies \cite{Kopp}, matter effects are negligible on the CNGS beam. In this domain, taking into account the finite energy resolution of OPERA detector, $\sin 2~\Delta_{41}$ and $\sin^2 \Delta_{41}$ average to 0 and $\frac{1}{2}$, respectively. The oscillation probability (\ref{eq1}) can thus be approximate to:
\begin{equation}
P(E) =  C^2 \sin^2\Delta_{31} + \frac{1}{2}\sin^2 2\theta_{\mu\tau} + C\sin 2\theta_{\mu\tau}\cos\phi_{\mu\tau} \sin^2\Delta_{31} + \frac{1}{2} C\sin 2\theta_{\mu\tau}\sin\phi_{\mu\tau}\sin 2\Delta_{31}.
\label{eq2}
\end{equation}
The number of observed $\nu_\tau$ events is compared to the expectation from (\ref{eq2}) using the asymptotic $\chi^2$ 
distribution of the log likelihood ratio test statistics:
$ q~=-2~\ln(\widetilde{\it{L}}(\phi_{\mu \tau} ,\sin^2 2\theta_{\mu \tau})/\it{L_0})$,
where $\it{L_0}=e^{-n}~n ^n/n!$ and
$\widetilde{\it{L}}(\phi_{\mu \tau} ,\sin^2 2\theta_{\mu \tau})$ is the profile
likelihood obtained by maximising $\it{L}$($\phi_{\mu \tau}$ ,$ \sin^2 2\theta_{\mu \tau}$, $ C^2$) over $C^2$.
In figure \ref{fig:plot1}(a) the $90\%$ CL exclusion
limits are presented for both normal and inverted mass hierarchies in
the parameter space of $\phi_{\mu \tau}$ vs $\sin^2 2\theta_{\mu \tau}$. 
\begin{figure}[h!]
\centering 
\includegraphics[width=0.98\linewidth]{Fig1_new2.pdf}
\caption{\label{fig:plot1} (a) $90\%$ CL exclusion limits in the
$\phi_{\mu \tau}$ vs $\sin^2 2\theta_{\mu \tau}$ parameter space 
for normal (NH, dashed red) and inverted (IH, solid blue) hierarchies
assuming $\Delta m^2_{41}>$ 1 $\mathrm{eV^2}$. 
Bands are drawn to indicate the excluded regions.
(b) OPERA 90\% CL exclusion limits
in the $\Delta m^2_{41}$ vs $\sin^2 2\theta_{\mu \tau}$ parameter space 
for the normal (NH, dashed red) and inverted (IH, solid blue) hierarchy of
the three standard neutrino masses. The exclusion plots by NOMAD \cite{Astier:2001yj} 
and CHORUS \cite{Eskut:2007rn} are also shown. Bands are drawn to indicate the 
excluded regions.
}
\end{figure}

To extend the search for a possible fourth sterile neutrino down to small $\Delta m^2_{41}$ values, the likelihood has been computed using GloBES software~\cite{bglobes}, which takes into account the non-zero $\Delta m^2_{21}$ value and also matter effects. The likelihood has been profiled also on the $\Delta m^2_{31}$ value. More details on the analysis are available in~\cite{noteglobes}. In Figure~\ref{fig:plot1}(b) the $90\%$~CL exclusion plot is reported in the $\Delta m_{41}^2$ vs $\sin^2\theta_{\mu\tau}$ parameter space. 

\section{The Computing Model of the OPERA Simulation Software} 

In this section we describe the computing model of the OPERA Monte Carlo (MC) simulation. 
The OPERA simulation framework, called OpRelease, is a set of $\mbox{C++}$ packages running mainly on Linux platforms. Figure (\ref{oprelease}) shows a schematic description of the OPERA simulation software. \\
The OpRelease packages are managed by the CMT \cite{cmt} system and are stored in the CERN SVN repository \cite{opera-svn}. We are planing to migrate the SVN server to CNAF to allow the access to SVN server by none CERN users. 
CMT is also used to manage external libraries (like ROOT~\cite{root}, CLHEP~\cite{clhep}, Pythia~\cite{pythia}, etc. ) through the CMT Interface package to hide the system specific properties of those libraries from the OPERA software. The ROOT library is used extensively for data storage, geometrical description of the detector and as an interface with the Monte Carlo simulation software through the Virtual Monte Carlo (VMC) package \cite{vmc}.\\
In the spirit of the GAUDI framework of LHCb and ATLAS, the chain of various algorithms used in the simulation and reconstruction programs are steered by a dedicated package called OpAlgo. This allows, together with the package OpIO which manages the I/O, to decouple the algorithms from the particular data storage model (ROOT Trees, ORACLE DB, ASCII files).\\
The geometrical description of the detector is managed by the OpGeom package using ROOT-TGeoManager classes. 
The result is a persistent and simulation-independent geometry model which can be accessed by any program sharing the same geometrical information among the various packages of OpRelease.\\
The detector simulation software, OpSim, is constructed around the VMC package which allows switching between GEANT3 and GEANT4 transportation code while keeping the same external geometry and event steering routines. The simulation of a neutrino interaction is performed externally to OpSim in order to easily switch between different interaction generators. Neutrino interactions are fed to OpSim through a so called beam file.\\
The reconstruction of events is performed in two steps: in the first step an event is reconstructed using only the information of the electronic detector (ED), in the second step bricks selected by the brick-finding algorithm are reconstructed using hits recorded in the emulsion. \\  
The reconstruction of the electronic detector part of an event is done by a sequence of algorithms managed by the OpRec package. First recorded hits are processed by a pattern recognition algorithm and sub-samples of hits are grouped into three dimensional (3D) tracks. A 3D-track is tagged as a muon if the product of its length and the density along its path is larger than $660~g/cm^2$. The momentum of 3D-tracks is calculated from their bending in the spectrometer magnetic field and/or from their range with a Kalman filter-based reconstruction algorithm. 
A classifier algorithm, called OpCarac \cite{opcarac}, is applied to select neutrino interactions inside the OPERA target. These events are further processed by a brick-finding algorithm \cite{bf}. The topology and the energy deposition in the OPERA Target Tracker scintillator strips, as well as the muon track information (when available) are used to define a three-dimensional probability density map for the vertex position. This probability is integrated over the volume of the bricks to select and reconstruct the bricks with the highest probability of containing the neutrino interaction.\\
\begin{figure}[htbp]
\begin{center}
\includegraphics[scale=0.5]{OperaSoftSchema_v3.pdf}
\caption{Schema of the {\tt OpRelease} software.}
\label{oprelease}
\end{center}
\end{figure}
The reconstruction of the simulated emulsion data is performed by the OpEmuRec package. This software has been designed to simulate the scanning and reconstruction procedure followed in the emulsion scanning laboratories. For this purpose, OpEmuRec implements an interface to SySal.NET \cite{sysal} and FEDRA \cite{fedra} {\it off-line} reconstruction and analysis software used in the European Scanning System (ESS)~\cite{ESS}. 
SySal.NET is written in $C\#$ and it runs natively on Windows machines and using Mono \cite{mono} on Linux machines. 
The MC data production is controlled by an ORACLE database through {\it tcsh} scripts. 
Configuration files and program settings used to run the various packages are stored in the DB. Each operation is recorded in the DB which allows subsequent tracing of all operations performed for the production of a particular dataset. The MC data files produced are recorded in the DB with their name, location and a link to the operations performed to produce those files.\\    
\section{The OPERA activity at CNAF in 2014}
The OPERA activity at CNAF started at the end of 2013 with the aim of setting up a second MC production site besides the one at the IN2P3 Computer Center in Lyon. The CNAF site went in production in late summer 2014. The managment of the jobs is done using custom {\it tcsh} scripts, while the bookkeeping of the produced events is managed with the OPERA Oracle data base.\\
Beside the standard OPERA MC production the CNAF production farm have been used for the statistical analysis of the OPERA results presented in section~\ref{stat}. In particular at the end  of July, thanks to a temporary share upgrade for OPERA ($\sim 100$ nodes), some toy Monte Carlo simulations (each lasting 15 minutes) have been run using ROOT/RooStats combined with Proof On Demand resource manager. The aim was the correct evaluation of the significance of the recent OPERA tau neutrino appearance analysis with the full treatment of nuisance parameters which requires simulations and cannot by performed by approximated formulas.\\ 
The CNAF farm was also extensively used for the analysis reported in section~\ref{exo} for the search of sterile neutrinos. The  Monte Carlo simulation with the General Long Baseline Experiment Simulator (GLoBES) software as well as the statistical analysis have been done entirely at the CNAF computing center.  


\section*{References}
\begin{thebibliography}{9}

\bibitem{proposal1}
OPERA collaboration, M. Guler et al., \emph{An appearance experiment to search for $\nu_\mu\rightarrow\nu_\tau$ oscillations in the CNGS beam: experimental proposal}, CERN-SPSC-2000-028,  LNGS P25/2000.

\bibitem{status_report}
OPERA collaboration, M. Guler et al., \emph{Status Report on the OPERA experiment}, CERN/SPSC 2001-025, LNGS-EXP 30/2001 add. 1/01.

\bibitem{CNGS}
Ed. K. Elsener,  \emph{The CERN Neutrino beam to Gran Sasso (Conceptual Technical Design)}, CERN 98-02, INFN/AE-98/05.\\ 
R. Bailey et al., \emph{The CERN Neutrino beam to Gran Sasso (NGS) (Addendum to report CERN 98-02, INFN/AE-98/05)}, CERN-SL/99-034(DI), INFN/AE-99/05.

\bibitem{operadetector}
OPERA collaboration, R. Acquafredda et al., \emph{JINST} {\bf 4} (2009) P04018.

%\cite{Agafonova:2010dc}
\bibitem{Agafonova:2010dc}
  N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``Observation of a first $\nu_\tau$ candidate in the OPERA experiment in the CNGS beam,''
  Phys.\ Lett.\ B {\bf 691} (2010) 138
  
  \bibitem{Agafonova:2014ptn}
  N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``Observation of tau neutrino appearance in the CNGS beam with the OPERA experiment,''
  PTEP {\bf 2014} (2014) 10,  101C01
  %[arXiv:1407.3513 [hep-ex]].
  %%CITATION = ARXIV:1407.3513;%%
  %5 citations counted in INSPIRE as of 24 Apr 2015
  
%\cite{Agafonova:2013dtp}
\bibitem{Agafonova:2013dtp}
  N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``New results on $\nu_\mu \to \nu_\tau$ appearance with the OPERA experiment in the CNGS beam,''
  JHEP {\bf 1311} (2013) 036

\bibitem{Agafonova:2014bcr}
  N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``Evidence for $\nu_\mu \to \nu_\tau$ appearance in the CNGS neutrino beam with the OPERA experiment,''
  Phys.\ Rev.\ D {\bf 89} (2014) 5,  051102.
  	
  \bibitem{Beringer:1900zz}
  J.~Beringer {\it et al.}  [Particle Data Group Collaboration],
  %``Review of Particle Physics (RPP),''
  Phys.\ Rev.\ D {\bf 86} (2012) 010001.
  %%CITATION = PHRVA,D86,010001;%%
  %5863 citations counted in INSPIRE as of 24 Apr 2015

\bibitem{Cowan:2010js}
  G.~Cowan, K.~Cranmer, E.~Gross and O.~Vitells,
  %``Asymptotic formulae for likelihood-based tests of new physics,''
  Eur.\ Phys.\ J.\ C {\bf 71} (2011) 1554
     [Eur.\ Phys.\ J.\ C {\bf 73} (2013) 2501]
 % [arXiv:1007.1727 [physics.data-an]].
  %%CITATION = ARXIV:1007.1727;%%
  %659 citations counted in INSPIRE as of 24 Apr 2015
  
  \bibitem{Feldman:1997qc}
  G.~J.~Feldman and R.~D.~Cousins,
  %``A Unified approach to the classical statistical analysis of small signals,''
  Phys.\ Rev.\ D {\bf 57} (1998) 3873
  [physics/9711021 [physics.data-an]].
  %%CITATION = PHYSICS/9711021;%%
  %1991 citations counted in INSPIRE as of 24 Apr 2015
%\cite{Agafonova:2013xsk}

\bibitem{Pontecorvo:1967fh}
  B.~Pontecorvo,
  %``Neutrino Experiments and the Problem of Conservation of Leptonic Charge,''
  Sov.\ Phys.\ JETP {\bf 26} (1968) 984
   [Zh.\ Eksp.\ Teor.\ Fiz.\  {\bf 53} (1967) 1717].
   
\bibitem{Kopp} J. Kopp, P.A.N. Machado, M. Maltoni and T. Schwetz, [arXiv:1303.3011v3].
 
 \bibitem{Astier:2001yj}
  P.~Astier {\it et al.}  [NOMAD Collaboration],
  %``Final NOMAD results on muon-neutrino ---> tau-neutrino and electron-neutrino ---> tau-neutrino oscillations including a new search for tau-neutrino appearance using hadronic tau decays,''
  Nucl.\ Phys.\ B {\bf 611} (2001) 3
  
  \bibitem{Eskut:2007rn}
  E.~Eskut {\it et al.}  [CHORUS Collaboration],
  %``Final results on nu(mu) ---> nu(tau) oscillation from the CHORUS experiment,''
  Nucl.\ Phys.\ B {\bf 793} (2008) 326

\bibitem{bglobes} P. Huber, M. Lindner and W. Winter, Comput. Phys. Commun. 167 (2005) 195.\\ P. Huber, J. Kopp, M. Lindner, M. Rolinec and W. Winter, Comput. Phys. Commun. 177 (2007) 432.

\bibitem{noteglobes} S. Dusini et al., "Search for sterile neutrino mixing in the $\nu_\mu \to \nu_\tau$ appearance channel with the OPERA detector", http://operaweb.lngs.infn.it/Opera/notes/notes/175/1/.
    
%\bibitem{Agafonova:2013xsk} N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``Search for $\nu_\mu \rightarrow \nu_e$  oscillations with the OPERA experiment in the CNGS beam,''
 %JHEP {\bf 1307} (2013) 004
  % [Addendum-ibid.\  {\bf 1307} (2013) 085]
  
%\cite{Agafonova:2014mzx}
%\bibitem{Agafonova:2014mzx}
%  N.~Agafonova {\it et al.}  [OPERA Collaboration],
  %``Measurement of TeV atmospheric muon charge ratio with the full OPERA data,''
  % arXiv:1403.0244 [hep-ex].

\bibitem{cmt} See http://www.cmtsite.org.

\bibitem{opera-svn}  https://svnweb.cern.ch/cern/wsvn/opera

\bibitem{root} R.~Brun and F.~Rademakers, 
% ``ROOT: an object oriented data analysis framework,`` 
Nucl. Instrum. Meth. {\bf A 389} (1997) 81;\\
see also http://root.cern.ch/

\bibitem{clhep} http://proj-clhep.web.cern.ch/proj-clhep/

\bibitem{pythia} T.~Sj\"ostrand, S.~Mrenna and P.~Skands, Comput. Phys. Comm. {\bf 178} (2008) 852
[arXiv:0710.3820]    

\bibitem{vmc}
  I.~Hrivnacova {\it et al.}  [ALICE Collaboration],
  %``The Virtual Monte Carlo,''
  eConf C {\bf 0303241} (2003) THJT006\\
  see also http://root.cern.ch/drupal/content/vmc

\bibitem{opcarac} A.~Bertolin et al., OPERA public note n.100 (2009), http://operaweb.lngs.infn.it/Opera/publicnotes/note100.pdf
%"{\sc OpCarac}: an algorithm for the classification of the neutrino interactions recorded by the OPERA experiment"

\bibitem{bf}  A.~Chukanov et al., OPERA public note n. 162 (2013), http://operaweb.lngs.infn.it/Opera/publicnotes/note162.pdf
% Neutrino interaction vertex location with the OPERA electronic detectors.

\bibitem{sysal} E.~Barbuto, C.~Bozza, and C.~Sirignano,
% Vertex Reconstruction in SySal.NET: the �global vertexing� algorithm. 
OPERA note 78 4-06-2006, (2006).

\bibitem{fedra} V.~Tioukov, I.~Kreslo, Y.~Petukhov, and G.~Sirri. 
The FEDRA�Framework for emulsion data reconstruction and analysis in the OPERA experiment. 
Nucl. Instrum. and Meth. {\bf A 559} (2006) 103. 
%Proceedings of the X International Workshop on Advanced Computing and Analysis Techniques in Physics Research - ACAT 05

\bibitem{ESS}
  L.~Arrabito, E.~Barbuto, C.~Bozza, S.~Buontempo, L.~Consiglio, D.~Coppola, M.~Cozzi and J.~Damet {\it et al.},
  %``Hardware performance of a scanning system for high speed analysis of nuclear emulsions,''
  Nucl.\ Instrum.\ Meth.\ A {\bf 568} (2006) 578
  
\bibitem{mono} http://www.mono-project.com/

%\bibitem{storm} http://italiangrid.github.io/storm/index.html

%\bibitem{xrootd} http://xrootd.org/

\end{thebibliography}

\end{document}

