\documentclass[a4paper]{jpconf}
\usepackage{multirow}
\usepackage{tabu}

\begin{document}

\title{An assessment of software metrics tools}

\author{E Ronchieri and F Giacomini}

\ead{elisabetta.ronchieri@cnaf.infn.it}

\begin{abstract}

Software metrics are a special kind of analysis, namely one in which
the correspondent measures express source code characteristics and
establish improvement priorities. Such metrics allow judging the
quality of software and highlighting the lower-accuracy portions of
code. Existing tools are able to collect meaningful measurements for
trend quality over time.  However, they interpret and implement the
definitions of software metrics differently, causing a considerable
unequal interpretation of the assessment of a software system from
tool to tool.

In this report, we provide details of a number of software metric
tools -- both commercial and free -- that we considered while
assessing the software quality of two Geant4 sub-packages, called
\verb|geometry| and \verb|processes|, over various
releases. Measurements showed that the results are tool-dependent.

\end{abstract}

\section{Introduction}

Geant4~\cite{Agostinelli, Allison} is a simulation system that is used
in a wide variety of scientific context, such as shielding and
radiation protection, and developed and maintained by an international
widespread collaboration. It is a mature system (20 years old) that
can be used as a playground to study metrics and metrics tools: in
particular, can metrics help addressing its maintenability in the next
20 years? To answer this question, we have started to perform the
quantitative assessment of a subset of Geant4 packages, which play a
key role in scientific applications and are representative of
different software development processes.

\begin{description}

\item[Geometry] makes it possible to describe a geometrical structure
  and propagate particles through it. In turn, it includes a set of
  sub-packages such as biasing, divisions, magnetic field, management,
  navigation, solids and volumes. Any simulation application involves
  some geometrical modelling of the experimental configuration.

\item[Processes] handles particle interactions: electromagnetic
  interactions of leptons, photons, hadrons and ions, and hadronic
  interactions and transportation. Like the geometry package, it
  comprehends a set of sub-packages such as biasing, cuts, decay,
  electromagnetic, hadronic, management, optical, parameterization,
  scoring and transportation. Electromagnetic physics represents the
  core of particle transport, as almost any simulation scenario
  involves electromagnetic interactions either of primary or secondary
  particles.

\end{description}

In this report, we present some software metrics tools -- both
commercial and free -- that mainly support the C++ programming
language, the one in which Geant4 is written. Furthermore, they
calculate software product metrics, that provide the characteristics
of software over time in terms of various categories, such as size,
complexity, object-orientation and quality.

\section{Initial Assessment}

An initial assessment of these tools is reported in~\cite{Ronchieri},
where we included the following tools CCCC (C and C++ Code
Counter)~\cite{CCCC}, CLOC (Count Lines of Code)~\cite{CLOC},
Metrics~\cite{Metrics}, Pmccabe~\cite{Pmccabe}, SLOCCount (Source
Lines of Code Count)~\cite{Sloccount} and
Understand~\cite{Understand}.

CCCC (C and C++ Code Counter) v. 3.1.4 analyses and reports
measurements of source code primarily in C++.  It processes the files
listed on its command line as described below, generating a report in
HTML format on various measurement of the code processed:

\begin{verbatim}
$ cccc --outdir=dname --lang=c++ --lang=c file1 ...
\end{verbatim}

The main file is \verb|cccc.htm| with detailed reports on each
module. The report contains a number of tables that cover: a rejected
extents presenting a list of code regions which the analyser was
unable to parse; a structural summary including relationships to each
module identified; a procedural summary presenting values of
procedural measures summed for each module identified in the code
submitted; a project summary report.

Cloc (Count Lines Of Code) v. 1.60 counts blank lines, comment lines
and physical lines of source code.  It processes the files listed on
its command line as described below, generating a report on size
measurement of the code processed:

\begin{verbatim}
$ cloc file1 ... --by-file-by-lang –report-file=fname.txt
$ cloc file1 ... --skip-uniqueness --by-file-by-lang –report-file=fname.txt
\end{verbatim}

The metrics software package was born in 2010. It counts lines of code
and McCabe metrics~\cite{Mccabe}. Metrics v. 0.2.6 processes the files
listed on its command line as described below, generating a report in
a csv file:

\begin{verbatim}
$ metrics –format=csv file1
\end{verbatim}

Pmccabe v. 2.6 counts non-commented lines and McCabe complexity for C
and C++.  It processes the files listed on its command line as
described below, showing data on the stdandard output:

\begin{verbatim}
$ pmccabe -t -b file1 ...
$ pmccabe -t -F -C file1 ...
$ pmccabe -t -f -C file1 ...
\end{verbatim}

SLOCCount v. 2.26 identifies and measures several languages.  It
counts the comment lines and counts lines automatically.  SLOCCount
processes the files listed on its command line as described below,
showing data in the stdoutput:

\begin{verbatim}
$ sloccount –datadir dname file1 ...
\end{verbatim}

Understand v. 3.1.728 is a static analysis tool for maintaining,
measuring and analysing code, supporting the main programming
language, which is under an evaluation license. It processes the files
listed on its command line \verb|und|, generating a report in a csv
file.

We performed measures on a standard PC with the Ubuntu operating
system version 13.10. We considered various Geant4 release
distributions in the range [6.1, 10.0] downloaded in a designated
directory. Furthermore, we installed all the metrics tools, such as
CCCC, CLOC, Metrics, Pmccabe, SLOCCount and Understand, and used their
command-line tool. Referring to the measures, we also implemented a
set of scripts: to automatize measurements on the same source tree of
the geometry and processes packages in all the Geant4 distributions;
to generate intermediate files containing raw measured data; to remove
unnecessary information and filter them. We used the Microsoft Excel
2010 and RStudio version 0.98.1091 tools to perform some graphics.

The software metrics tools used in this study return the same value
for the Number of Files (NFiles), whereas they provide similar values
for Lines Of Code (LOC), Comment LOC (CLOC), Blank LOC (BLOC).  The
Cloc tool supports all the aforementioned metrics, while the SLOCCount
tool only provides information for the LOC metric. Furthermore, the
Cloc and Pmccabe tools provide the same Total LOC due to the use of
the same code to measure the LOC, CLOC and BLOC metrics. These tools
provide metric values that are often unequal, mainly due to the way
the lines with comments are counted, but also to the lines removed
from the tool parser, and the way curly brackets are counted.  CCCC
and Understand are able to provide measurements about
object-orientation category in addition to the others.

\section{Future Work}

The next steps of this activity consist of: doing statistical analysis
for inference; evaluating the applicability of existing quality
thresholds; adding further object-oriented metrics that contribute to
evaluate maintainability software factor; determining which metrics
are most effective at identifying risks; correlating interactions
between software quality and functional quality.  At the end of this
study, we intend to create a baseline for evaluating correlations
between software quality embedded in the Geant4 development process
and simulation observables produced by Geant4-based applications.

\section*{References}
\bibliographystyle{iopart-num}
\bibliography{report}

\end{document}
