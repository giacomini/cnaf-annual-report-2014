\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\begin{document}
\title{Provisioning IaaS for the Open City Platform project}

\author{C~Aiftimiei$^{1,2}$, M~Antonacci$^3$, G~Donvito$^3$,
  E~Fattibene$^1$, A~Italiano$^3$, D~Michelotto$^1$, D~Salomoni$^1$,
  S~Traldi$^4$, C~Vistoli$^1$ and G~Zizzi$^1$}

\address{$^1$INFN CNAF, Viale Berti Pichat 6/2, 40126 Bologna, Italy}
\address{$^2$IFIN - "Horia Hulubei", Bucharest - Magurele, Romania}
\address{$^3$INFN Bari, Via E. Orabona n. 4, 70125 Bari, Italy}
\address{$^4$INFN Padova, Via Marzolo 8, 35131 Padova, Italy}

\ead{cristina.aiftimiei@cnaf.infn.it}

\begin{abstract}

The Open City Platform (OCP) project~\cite{ref1} aims to research,
develop and test new technological solutions open, interoperable and
that can be used on-demand in the context of cloud computing, along
with new organizational models, sustainable in time, for the public
administration, to innovate, with scientific results, with new
standards and technological solutions, the provision of services by
local public administrations (PAL) and regional citizens, companies
and other government departments. OCP inherits the experience of other
projects of Cloud Computing applied to the Public Administration and
Research like PRISMA\cite{ref2}, Marche Cloud\cite{ref3}, INFN
Cloud. The IaaS layer of OCP is based on OpenStack~\cite{ref4}, the
open source cloud solution most widespread in the world to manage
physical resources, virtual machines and containers. In this paper we
will present the reaserch activity done at CNAF in order to indentify
the best tools for an automatic provisioning as the IaaS layer of the
OCP platform.

\end{abstract}

\section{Introduction}

As is well-known, Cloud computing is a model for procurement and use
of distributed storage and computing resources with a strong
development and adoption by the industry, academia and government. For
these players, however, the adoption of the models offered by cloud,
although it represents a strong opportunity, is often hampered by
different problems: technical and scientific issues, rules and
regulations that impose specific behavior and protection for
themselves and for the recipient of public services (citizen or
company).

Therefore the main areas of research conducted by the OCP can be
traced to:

\begin{itemize}
\item scientific and technological challenges like:
  \begin{itemize}
  \item Federated management of heterogeneous cloud platforms
  \item Integrated monitoring and support to billing systems
  \item Design and reengineering Cloud applications
  \item Disaster recovery as a Service
  \item Integration of PaaS components in particular Paas for eGov
  \item Open data and the Open Service and integration into business models
  \item Federated identity management  and its trust relationship
  \end{itemize} 
\item legal, organizational, functional challenges and new business
  models that are necessary to ensure a concrete feasibility within
  government scope of the results achieved by the project, like:
  \begin{itemize}
  \item Define new organizational models and public governance where regions have the role of infrastructure intermediaries
  \item Decouple the exercise of administrative functions (public
    role) from the ICT instrument (private role) on which the
    function is performed
  \item Adherence to new models regulations
  \item Accountability and attribution of precise responsibilities
    ensuring mutual protection among those involved in the chain of
    service
  \end{itemize}
\end{itemize}

The OCP architecture has been designed as a scalable, multilayer and 
interoperable platform that inherits also components from previous projects, like PRISMA. 
The main components are shown in Figure~\ref{fig1}:

\begin{itemize}
\item orchestrator module
\item monitoring/billing module
\item PaaS layer/services
\item IaaS layer/services
\item Open Data e Open Service Engine
\item Service/citizen market place
\end{itemize} 
\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=150mm]{iaas.png}
  \end{center}
  \caption{\label{fig1}OCP platform architecture}
\end{figure}

The IaaS layer of OCP is based on OpenStack, the most widely used open-source
cloud solution that allows to manage physical resources,
virtual machines and containers.

\section{IaaS and installation scenarios}

Open City Platform makes available a Cloud Computing platform open
source, flexible, scalable and compliant to international standards
that consists of components organized in different layers fully
integrated with each other that can already be installed and used for
activation of on demand services and access data in the Data Centers
of the PA. The platform will be continuously updated with additional
OCP components developed by the project in order to progressively meet
the various specific needs of PA. The layers that constitute the cloud
platform OCP consists of:

\begin{itemize}
\item LAYER 1: A Infrastructure as a Service (IaaS) platform based
  on OpenStack, suitably configured by capitalizing on the
  pioneering experiences made in the INFN Cloud-infrastructure,
  Marche Cloud project and especially in the PRISMA project.
\item LAYER 2: A Platform as a Service (PaaS) platform that allows
  to use heterogeneous IaaS sites and easily manage the activation and
  execution of new applications
\item LAYER 3: A Software as a Service (SaaS) platform that consists
  of an Application Store and a set of new services that allow PA to
  choose the application of interest, to configure it to suit their
  needs and activate it digitally native on cloud-infrastructure,
  also different from that of OCP.
\end{itemize}

In the first phase OCP will make available the components related to
the LAYER 1 and LAYER 2.  OCP is evaluating three different
installation methods for the IaaS layer. The selection of the
installation method that will be adopted by the regions involved in
the pilot study will depend on factors such as:

\begin{itemize}
\item degree of familiarity with OpenStack
\item interest in deepening configuration knowledge
\item requirement of special configurations, etc.
\end{itemize}

The proposed methods are:
\begin{itemize}
\item manual installation and configuration ('hardcore').
\item installation with data center automation tools like
  Puppet~\cite{ref5} and/or Foreman~\cite{ref6}.
\item semi-automatic installation (GUI) through the Fuel~\cite{ref7}
  tool.
\end{itemize}

\subsection{Manual Installation and Configuration}

This method consists of: 

\begin{itemize}
\item The installation of the Openstack middleware based on the use
  of the package manager (apt-get, yum, etc.) and the repositories
  available for the chosen distribution of the Operating System
\item Changing the configuration of the individual services by
  editing the corresponding configuration files.
\item Verifying the proper installation and configuration by checking
  the state of the processes involved, the status of the connections
  between related services, the messages collected in the logs, etc.
\item In the case of deploying multi-node HA, services are first
  installed in standalone mode and then clustered.
\end{itemize}

Some of the advantages of this method are:

\begin{itemize}
\item better understanding of OpenStack dependencies between
  components, of the possible choices in the deployment of services
\item more control over configurations, the possibility to enable
  advanced functionality, support multi-region, SSL support of the
  identity service, etc.
\end{itemize}

Yet, the following disadvantages have been identified:

\begin{itemize}
\item this method requires requires basic knowledge of Linux OS -
  bash, network configuration
\item error-prone - for ex. typo in the configuration change
\item time-consuming - many of the operations are repetitive
\item it requires additional effort to keep aligned configurations
  on more servers (eg in the case of multi-node deployment HA).
\end{itemize}

This method is widely used in INFN cloud projects both for production
and for test and pre-production environments. Based on our experience
we can say that the manual installation method is to be considered
preferable as a first approach to the installation of OpenStack to get
familiar with all the parts, when there is a need to implement special
advanced configurations. The greater flexibility and control in the
choice of configuration are the main strengths of this method.

\subsection{Installation with automation tools}

Puppet is a tool that has the purpose to automate procedures within a
data center. It provides modules for installation and automatic
configuration of OpenStack.

After defining the configuration, Hiera, an add-on Puppet component
that allows hierarchical configurations, the agent automatically
boots the process of installation and configuration. At the end of the
process it will produce a report. Errors can only be due to incorrect
(or lack of) configuration.

The OpenStack Puppet modules~\cite{ref8} follow the normal development
of OpenStack with a major new release for each version of
OpenStack. Within each cycle version of the modules they are released
1/2 updates.

Pros and cons:

\begin{itemize}
\item Puppet is almost a programming language that needs to be
  learned; its "learning curve" depends on ones experience, which
  will also serve to handle errors.
\item This method architecturally has no limits on the definition of
  configurations.
\end{itemize}

Work progress:

\begin{itemize}
\item at this moment all services are covered by OpenStack Puppet
  modules available.
\item In the future we plan to add the ability to support more
  complex use cases such as CEPH on the hypervisors.
\end{itemize}

It is possible to manage the installation of OpenStack through Puppet
also via a GUI through the use of the Foreman tool.

Pros:

\begin{itemize}
\item Use of a unique web interface to provision Linux hosts
  (Redhat, CentOS, Ubuntu, Debian, Suse) and manage both puppet
  modules and OpenStack components;
\item Easy installation of components using OpenStack QuickStack
  modules - there is no need to have a deep knowledge of OpenStack
  to install and configure it;
\end{itemize}

Cons:

\begin{itemize}
\item The installation procedure is in continue evolution (as
  OpenStack) - it may therefore be necessary to apply minor changes
  and patches in order to have working modules.
\item If the OpenStack infrastructure is very complex it should be
  assessed whether it is possible to finely parameterize via Foreman
  OpenStack components (keystone, nova, glance, cinder, etc.);
\item Error messages are not easy to understand.
\end{itemize}

\subsection{Semi-automatic installation using Fuel}

Fuel is a tool for the installation and management of an
OpenStack-based infrastructure. It allows to perform the installation
and configuration of one or more OpenStack environments through a
web-GUI (see Fig.~\ref{fig2}), and it gives also the possibility of
using the CLI (command line client). When we performed our tests it was
available for Icehouse~\cite{ref9} on Centos 6.5 and Icehouse on
Ubuntu 12.04.4.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=150mm]{Picture1.png}
  \end{center}
  \caption{\label{fig2}Fuel GUI}
\end{figure}

Functionality offered:

\begin{itemize}
\item Fuel discovers automatically each host (physical or virtual)
  configured to boot from network and present in its
  VLAN. Afterwards different roles can be assigned to each node
  (controller, storage etc.).
\item There is a wizard for an easy installation
\item Logs can be consulted via GUI, providing also control of the
  verbosity (see Fig.~\ref{fig3}).
\item It offers the possibility to perform tests on the health of the
  services and the correctness of network configuration.
\end{itemize}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=150mm]{Picture2.png}
    \includegraphics[width=150mm]{Picture3.png}
  \end{center}
  \caption{\label{fig3}Fuel - Installation and Configuration Logs display}
\end{figure}

The environment/infrastructure we deployed for testing purposes consisted of:

\begin{itemize}
\item Icehouse on CentOS 6.5
\item Multi-node HA
\item Neutron with GRE
\item Nodes: 1 Fuel Master, 3 Cloud Controllers, 2 Compute nodes, 1
  Cinder server, 2 Ceph servers and 1 Zabbix server (for monitoring)
\end{itemize}

Some of the advantages and disadvantages that we identified are:

\begin{itemize}
\item Pros:
  \begin{itemize} 
  \item Easy installation through the GUI
  \item It enables subsequent changes and new deployments (to change
    the additional services and add or remove nodes)
  \item There is no need of custom adaptations.
  \end{itemize}
\item Cons:
  \begin{itemize} 
  \item The initial configuration cannot be changed (eg. move from a
    simple to a high-availability setup)
  \item The OpenStack regions are not yet supported.
  \item The various components of the Cloud Controller (eg. Keystone,
    Glance, Horizon, Neutron) are all installed on the same machine.
  \item No installation options for an "all in one" setup (with only
    one server).
  \end{itemize}
\end{itemize}

\section{Future Work}

One of the layers of the OCP platform is the IaaS, the base on which
all the other layers build up. The chosen middleware is OpenStack,
the Juno~\cite{ref10} version, and in order to ease its deployment in
the testbeds available in the experimenter regions an activity of
testing various deployment methods was carried out. The results were
presented and based on them it was decided that in the first instance a
manual instalation will be performed in some of the testbeds whereas
future work will be invested in the preparation of an automatic tool
based on Puppet and Foreman to be used in the remaining testbeds and
new ones that will agree in using the results of the OCP project. This
tool will also be used for performing the updates of the
infrastructures to the new versions of Openstack.

\section*{References}

\begin{thebibliography}{9}
\bibitem{ref1} OpenCityPlatform Project, {\it http://www.opencityplatform.eu/}
\bibitem{ref2} PRISMA,{\it http://www.ponsmartcities-prisma.it/}
\bibitem{ref3} MCloud,{\it http://www.ecommunity.marche.it/AgendaDigitale/MCLoud/Obiettivi/tabid/206/Default.aspx}
\bibitem{ref4} OpenStack, {\it http://www.OpenStack.org/}
\bibitem{ref5} Puppet, {\it https://puppetlabs.com/}
\bibitem{ref6} Foreman, {\it http://theforeman.org/}
\bibitem{ref7} Fuel, {\it https://www.mirantis.com/products/mirantis-openstack-software/}
\bibitem{ref8} Openstack Puppet, {\it https://wiki.openstack.org/wiki/Puppet}
\bibitem{ref9} OpenStack Icehouse, {\it https://www.OpenStack.org/software/icehouse/}
\bibitem{ref10} OpenStack Juno, {\it https://www.OpenStack.org/software/juno/}
\end{thebibliography}

\end{document}
