#!/bin/sh -x

dn=$(dirname $0)
if [ "${dn}" = "." ]; then
  echo "Run the build in a subdirectory or in an out-of-source directory"
  exit 1
fi

topdir=$(readlink -f ${dn})
builddir=$(readlink -f .)
papersdir=${builddir}/papers
latex_templates=${topdir}/instructions/LaTeXTemplates.zip

die() {
    echo >&2 "$1"
    exit 1
}

build_from_source() {
    local project project_dir main_tex_file other_files bib_file ok
    project_dir="$1"
    project=$(echo ${project_dir} | tr '/' '_')
    main_tex_file="$2"
    [ -f "${topdir}/contributions/${project_dir}/${main_tex_file}" ] || die "invalid call to build_from_source()"
    shift; shift;
    other_files="$@"

    if [ ! -d ${project_dir} ]; then
	mkdir -p ${project_dir}
	cd ${project_dir}

	cp ${topdir}/contributions/${project_dir}/${main_tex_file} ./${project}.tex \
	    && for f in ${other_files}; do \
	           cp -a ${topdir}/contributions/${project_dir}/${f} .; done \
	    && unzip ${latex_templates} \
	    && pdflatex ${project} \
	    && if grep bibdata ${project}.aux; then
	           unzip -j BibTeX/iopart-num.zip iopart-num/iopart-num.bst
		   bibtex ${project}
		   pdflatex ${project}
	       fi \
	    && pdflatex ${project} \
	    && ln -sf ${builddir}/${project_dir}/${project}.pdf ${papersdir}

	cd -
    fi
}

link_pdf() {
    local project project_dir pdf_file
    project_dir="$1"
    project=$(echo ${project_dir} | tr '/' '_')
    pdf_file="$2"
    [ -f "${topdir}/contributions/${project_dir}/${pdf_file}" ] || die "invalid call to link_pdf()"

    ln -s "${topdir}/contributions/${project_dir}/${pdf_file}" ${papersdir}/${project}.pdf
}

if [ ! -d ${papersdir} ]; then
    mkdir -p ${papersdir}
fi

cd ${builddir}

# prepare cover
convert -verbose ${topdir}/contributions/cover/AR2014_CNAF.jpg ${papersdir}/cover.pdf

# prepare Giulia's photo
ln -sf ${topdir}/contributions/giulia/giulia.jpg ${papersdir}

build_from_source accounting   accounting.tex
link_pdf          alice        "ALICE per Annual Report CNAF formatted 30.4.2015.pdf"
build_from_source ams          2014_AMS_CNAF.tex AMS_2014.pdf AMSprod.pdf AMSshort.pdf contributors.pdf data_flow_2.pdf dataitaly.pdf
link_pdf          atlas        ATLAS-report-CNAF-201503.pdf
build_from_source auger        AugerARperCNAF_2.tex Exposure.eps
link_pdf          belle2       CNAFBelle2-2014.pdf
build_from_source borexino     Borexino_CNAFreport2014.tex
build_from_source cdf          CDF_CNAF_annual_report.tex cdfbib.bib figures
link_pdf          chaos        chaosAnnualReportCNAF2014.pdf
build_from_source cloud_cnaf   cloud_cnaf.tex jenkins.png storm.png
link_pdf          cms          REPORT_CMS_CNAF_2014.pdf
build_from_source coka         report-coka-v6.tex fig
build_from_source ct_socs      ct_socs.tex fig1.png fig2.png fig3.png fig4.png
build_from_source cta          CTA_AnnualReport_2015.tex
build_from_source cuore        cnaf_cuore.tex cnaf_cuore.bib
build_from_source dynamic_farm dynamic_farm.tex mcore_20days.png mcore_aug2014.png mcore_cumulative.png mcore_mar2015b.png atlas_himem_cumulative.png mc20days_03_2015.png
build_from_source eee          consCNAFeee.tex EEEarch.eps EEEmonitor.eps EEEpilotrun.eps
build_from_source gerda        gerda_cnaf.tex
build_from_source glast        cnaf_2014_v3.tex jobs.png share.png time.png
build_from_source km3tridas    km3-tridas.tex schema.jpg test.jpg
link_pdf          lhcb         LHCbCNAFAnnualReport2015.pdf
build_from_source lhcb_eb      LHCb.EB.Antonio.Falabella.tex *.eps
build_from_source low_power    low_power.tex avoton_tco.png c2750.png hs06_per_tender.png hs06w.png
build_from_source metrics      metrics.tex report.bib
build_from_source mw_devel     mw_2014.tex
build_from_source monitoring   monitoring.tex homepage.png json-snippet.png
link_pdf          na62         na62@cnaf.pdf
build_from_source ocp          ocp_cnaf.tex iaas.png Picture1.png Picture2.png Picture3.png
build_from_source opera        CNAF_report_Opera2014.tex Fig1_new2.pdf OperaSoftSchema_v3.pdf
build_from_source servizi_nazionali servizi_nazionali.tex
build_from_source sw_quality   ReportQuality.tex report.bib
build_from_source t1           t1.tex jobs.png
build_from_source t1_network   t1_network.tex netboard.png netlab.png wan.png
build_from_source tt           tt.tex
build_from_source user_support ar.tex cpu.eps disk_lhc.eps disk_nonlhc.eps tape.eps
build_from_source virgo        VirgoatCNAF.tex BS.png SCHEMA.png
build_from_source wnodes       ReportWNoDeS2.tex
build_from_source xenon        report_CNAF.tex

pdflatex ${topdir}/ar.tex \
&& pdflatex ${topdir}/ar.tex 2> /dev/null \
&& pdflatex ${topdir}/ar.tex 2> /dev/null
